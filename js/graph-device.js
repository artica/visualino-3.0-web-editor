// All device implementations are either inputs or outputs
var DEVICE_CLASS_INPUT = 1;
var DEVICE_CLASS_OUTPUT = 2;


function Device(id, name, type) 
{
	this.init();
	this.id = id;
	this.name = name ? name : '';
	this.type = type ? type : '';
	//console.log('new device:'+name);
	return this.uid;
}

Device.prototype = 
{
	init: function()
	{
		this.uid = g.uid_function.get_device_uid();
		this.visible = false;
		return this;
	},

	setImage: function(img_id)
	{
		console.log('setImage:'+img_id);
		this.type.img_id = img_id;
		this.type.image = images.getImage(img_id);
	},

	getImage: function()
	{
		//console.log(this);
		return this.type.getImage();
	},

	toString: function() 
	{
		return this.name;
	},
	
	
	//
	// export / import
	//
	
	export_device: function() 
	{
		var output = 
		{
			'name': this.name,
			'type': this.type.export_nodetype(),
			'uid': this.uid
		};
		return output;
	},
	
	export_arduino: function(d) 
	{
		return "D"+ padByte(d) +this.type.export_arduino();
	},
	
}
