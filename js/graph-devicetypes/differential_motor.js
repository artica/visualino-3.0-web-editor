
//
// Digital Output
//

function DeviceTypeDifferentialMotor(pin_a1, pin_a2, pin_b1, pin_b2) 
{
	this.name = 'differential_motor';
	this.class = DEVICE_CLASS_OUTPUT;
	this.type_id = DEVICE_OUTPUT_DIFFERENTIAL_MOTOR;
	
	this.pin_a1 = pin_a1;
	this.pin_a2 = pin_a2;
	this.pin_b1 = pin_b1;
	this.pin_b2 = pin_b2;
	this.init();
	return this.name;
}

DeviceTypeDifferentialMotor.prototype = 
{
	init: function() 
	{
		this.image = images.getImage('differential_motor');
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() 
	{
		return this.name;
	},
	toStringType: function() 
	{
		return t[lang][this.name];
	},
	toStringConfiguration: function() 
	{
		return 'Left:'+this.pin_a1 + ','+this.pin_a2 + '<br>Right:'+this.pin_b1 + ','+this.pin_b2;
	},
	list_properties: function(n) 
	{
		return 	"<table>"+
			"<tr>"+
				"<td>"+t[lang]['Left Motor']+" "+t[lang]['Pin']+" 1</td>"+
				"<td>"+createHtmlSelectorPins('select_motor_a_1_'+n, h.PINS_DIGITAL_OUTPUT, this.pin_a1)+"</td>"+
			"</tr>"+
			"<tr>"+
				"<td>"+t[lang]['Left Motor']+" "+t[lang]['Pin']+" 2</td>"+
				"<td>"+createHtmlSelectorPins('select_motor_a_2_'+n, h.PINS_DIGITAL_OUTPUT, this.pin_a2)+"</td>"+
			"</tr>"+
			"<tr>"+
				"<td>"+t[lang]['Right Motor']+" "+t[lang]['Pin']+" 1</td>"+
				"<td>"+createHtmlSelectorPins('select_motor_b_1_'+n, h.PINS_DIGITAL_OUTPUT, this.pin_b1)+"</td>"+
			"</tr>"+
			"<tr>"+
				"<td>"+t[lang]['Right Motor']+" "+t[lang]['Pin']+" 2</td>"+
				"<td>"+createHtmlSelectorPins('select_motor_b_2_'+n, h.PINS_DIGITAL_OUTPUT, this.pin_b2)+"</td>"+
			"</tr>"+

			"</table>";
	},
	add_list_properties_listeners: function() 
	{
	},

	create_default_device: function()
	{
		var device =  new Device( createId(), t[lang]['new_device'], new DeviceTypeDifferentialMotor( 3, 5, 6, 11) );
		return  device;
	},

	create_custom_device_data: function(n)
	{
		console.log("create_custom_device_data: digital output");
		var pin_a1 = $('#select_motor_a_1_'+n).val();
		var pin_a2 = $('#select_motor_a_2_'+n).val();
		var pin_b1 = $('#select_motor_b_1_'+n).val();
		var pin_b2 = $('#select_motor_b_2_'+n).val();
		var initial_state = $('#select_output_initial_state_'+n).val();

		return new DeviceTypeDifferentialMotor( pin_a1, pin_a2, pin_b1, pin_b2);
	},

	show_service_configuration: function(data)
	{
		return 	"<table><tr>"+
				"<td>"+t[lang]['Left Motor']+"</td>"+
				"<td><input type='number'id='text_left_speed' min='-255' max='255' value="+data.left_speed+"></input></td>"+
			"</tr><tr>"+
				"<td>"+t[lang]['Right Motor']+"</td>"+
				"<td><input id='text_right_speed' type='number' min='-255' max='255' value="+data.right_speed+"></input></td>"+
			"</tr></table>";
	},

	add_service_configuration_listeners: function(thisnode)
	{
		$('#text_left_speed').on('change', function() 
			{
				console.log("BAM! text_left_speed");
				thisnode.data.left_speed = parseInt($('#text_left_speed').val());
			});
		$('#text_right_speed').on('change', function() 
			{
				console.log("BAM! text_right_speed");
				thisnode.data.right_speed = parseInt($('#text_right_speed').val());
			});
	},

	init_service_data: function()
	{
		return {left_speed: 0, right_speed:0};
	},

	export_nodetype: function() 
	{
		var output = 
		{
			'name': this.name,
			'pin_a1': this.pin_a1,
			'pin_a2': this.pin_a2,
			'pin_b1': this.pin_b1,
			'pin_b2': this.pin_b2
		};
		return output;
	},
	export_arduino: function(device) 
	{
		return padByte(DEVICE_OUTPUT_DIFFERENTIAL_MOTOR) +
			padByte(this.pin_a1) + padByte(this.pin_a2)+
			padByte(this.pin_b1) + padByte(this.pin_b2)+ ';';
	},

	export_arduino_service: function(node)
	{
		console.log("DeviceTypeDifferentialMotor ---------------:"+node.device.uid+"  "+node.data.left_speed+"  "+node.data.right_speed);
		console.log(node);
		return 	padByte(SERVICE_OUTPUT_DIFFERENTIAL_MOTOR)+
			padByte(node.device.uid)+padInt(node.data.left_speed+255)+padInt(node.data.right_speed+255)+';';
	}

}
