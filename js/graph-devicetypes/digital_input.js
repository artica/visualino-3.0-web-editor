
//
// Digital Input
//

function DeviceTypeDigitalInput(pin_number, active_type)
{
	this.name = 'digital_input';
	this.class = DEVICE_CLASS_INPUT;
	this.type_id = DEVICE_INPUT_DIGITAL_PIN;
	this.pin_number = pin_number !== undefined ? pin_number : 2;
	this.active_type = active_type !== undefined ? active_type: 1;

	this.init();
	return this.name;
}

DeviceTypeDigitalInput.prototype = 
{
	init: function() 
	{
		this.image = images.getImage('digital_input');
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() 
	{
		return this.name;
	},
	toStringType: function() 
	{
		return t[lang][this.name];
	},
	toStringConfiguration: function() 
	{
		return this.pin_number + ', '+this.active_type;
	},
	list_properties: function(n) 
	{
		html = "<table>"+
			"<tr>"+
				"<td>"+t[lang]['Pin']+"</td>"+
				"<td>"+createHtmlSelectorPins('select_input_pin_number_'+n, h.PINS_DIGITAL_INPUT, this.pin_number)+"</td>"+
			"</tr>"+
			"<tr>"+
				"<td>"+t[lang]['Active']+"</td>"+
				"<td>"+createHtmlSelectorHighLow('select_input_active_state_'+n, this.active_type)+"</td>"+
			'</tr>'+
		'</table>';
		return html;
	},

	add_list_properties_listeners: function() 
	{

	},

	create_default_device: function()
	{
		var device =  new Device( createId(), t[lang]['new_device'], new DeviceTypeDigitalInput( 6, 0) );
		return  device;
	},

	create_custom_device_data: function(n)
	{
		console.log("create_custom_device_data: digital input");
		var pin_number = $('#select_input_pin_number_'+n).val();
		var active_state = $('#select_input_active_state_'+n).val();
		console.log("pimba! "+ n + "   "+ pin_number + "  " + active_state);

		return new DeviceTypeDigitalInput( pin_number, active_state);
	},

	show_service_configuration: function(data)
	{
		return "";
	},

	add_service_configuration_listeners: function(thisnode)
	{
	},

	init_service_data: function()
	{
		return {};
	},


	export_nodetype: function() 
	{
		var output = 
		{
			'name': this.name,
			'pin_number': this.pin_number,
			'active_type': this.active_type
		};
		return output;
	},

	export_arduino: function(device) 
	{
		return padByte(DEVICE_INPUT_DIGITAL_PIN) +
			padByte(this.pin_number) + padByte(this.active_type)+';';
	},

	export_arduino_service: function(node)
	{
		console.log("DeviceTypeDigitalInput ---------------:"+node.device);
		console.log(node.device);

		return padByte(SERVICE_INPUT_DIGITAL_PIN) + padByte(node.device.uid)+';';
	}

}
