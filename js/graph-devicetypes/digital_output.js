
//
// Digital Output
//

function DeviceTypeDigitalOutput(pin_number, initial_state) 
{
	this.name = 'digital_output';
	this.class = DEVICE_CLASS_OUTPUT;
	this.type_id = DEVICE_OUTPUT_DIGITAL_PIN;
	this.pin_number = pin_number !== undefined ? pin_number: 2;
	this.initial_state = initial_state !== undefined ? initial_state: 0;
	this.init();
	return this.name;
}

DeviceTypeDigitalOutput.prototype = 
{
	init: function() 
	{
		this.image = images.getImage('digital_output');
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() 
	{
		return this.name;
	},
	toStringType: function() 
	{
		return t[lang][this.name];
	},
	toStringConfiguration: function() 
	{
		return this.pin_number + ', '+this.initial_state;
	},
	list_properties: function(n) 
	{
		html = "<table>"+
			"<tr>"+
				"<td>"+t[lang]['Pin']+"</td>"+
				"<td>"+createHtmlSelectorPins('select_output_pin_number_'+n, h.PINS_DIGITAL_OUTPUT, this.pin_number)+"</td>"+
			"</tr>"+
			"<tr>"+
				"<td>"+t[lang]['Initial State']+"</td>"+
				"<td>"+createHtmlSelectorHighLow('select_output_initial_state_'+n, this.initial_state)+"</td>"+
			'</tr>'+
		'</table>';
		return html;
	},
	add_list_properties_listeners: function(n) 
	{

	},

	create_default_device: function()
	{
		var device =  new Device( createId(), t[lang]['new_device'], new DeviceTypeDigitalOutput( 5, 0) );
		return  device;
	},

	create_custom_device_data: function(n)
	{
		console.log("create_custom_device_data: digital output");
		var pin_number = $('#select_output_pin_number_'+n).val();
		var initial_state = $('#select_output_initial_state_'+n).val();

		return new DeviceTypeDigitalOutput( pin_number, initial_state);
	},

	show_service_configuration: function(data)
	{
		return "<table><tr><td>"+t[lang]['State']+"</td>"+			
			"<td>"+createHtmlSelectorHighLow('select_output_state', data.new_value)+"</td>"+
			"</tr></table>";
	},

	add_service_configuration_listeners: function(thisnode)
	{
		$('#select_output_state').on('change', function() 
			{
				thisnode.data.new_value = parseInt($('#select_output_state').val());
				console.log(thisnode.data.new_value);
			});
	},

	init_service_data: function()
	{
		return {new_value: 1};
	},

	export_nodetype: function() 
	{
		var output = 
		{
			'name': this.name,
			'pin_number': this.pin_number,
			'initial_state': this.initial_state
		};
		return output;
	},
	export_arduino: function(device) 
	{
		return padByte(DEVICE_OUTPUT_DIGITAL_PIN) +
			padByte(this.pin_number) + padByte(this.initial_state)+';';
	},

	export_arduino_service: function(node)
	{
		console.log("DeviceTypeDigitalOutput ---------------:"+node.device);
		console.log(node.device);

		return 	padByte(SERVICE_OUTPUT_DIGITAL_PIN)+
			padByte(node.device.uid)+padByte(node.data.new_value)+';';
	}
}
