

function testme() {
//	testImportFromFile('importjson/test.json');
}

function testExport() {
	var jsonstring = g.export_json();
	g.import_json(jsonstring);
}

function testImport() {
	var jsonstring = '{"nodes":[{"name":"","type":{"name":"Root"},"uid":"1","x":350,"y":150},{"name":"","type":{"name":"Parallel","completion_policy_selected":"requireall","failure_policy_selected":"requireone"},"uid":"2","x":350,"y":200},{"name":"","type":{"name":"Motor","pintype":"Digital","pin1value":5,"pin2value":6,"velocity":0},"uid":"3","x":314,"y":253}],"connections":["1:2","2:3"]}';
	g.import_json(jsonstring);
}

/* we should avoid requiring localhost webserver for now

function testImportFromFile(jsonfile) {
	g.import_jsonfile(jsonfile);
}


var getJSON = function(url, successHandler, errorHandler) {
  var xhr = new XMLHttpRequest();
  xhr.open('get', url, true);
  xhr.responseType = 'json';
  xhr.onload = function() {
    var status = xhr.status;
    if (status == 200) {
      successHandler && successHandler(xhr.response);
    } else {
      errorHandler && errorHandler(status);
    }
  };
  xhr.send();
};

getJSON('http://mathiasbynens.be/demo/ip', function(data) {
  alert('Your public IP address is: ' + data.ip);
});
*/
