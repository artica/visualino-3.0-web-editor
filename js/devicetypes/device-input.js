
//
// Input Device Class
//

// Constructor for the abstract Input Device Class
function DeviceInput(id, name) 
{
	Device.call(this, id, name);

	// Categorize this device
	this.device_class = DEVICE_CLASS_INPUT;

	return this;
}

// Inherit from the Device class
DeviceInput.inheritsFrom(Device);

// All the prototype definitions MUST be added after this point



/*
DeviceInput.prototype.getInputs = function()
{
	return 	
};

*/
