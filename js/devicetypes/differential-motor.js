
//
// Differential Motor Device
//

function DeviceOutputDifferentialMotor(id, name, data) 
{
	Device.call(this, id, name);
	// Create a new data field with the elements from this device type
	this.data.pin_a1 = data.pin_a1;
	this.data.pin_a2 = data.pin_a2;
	this.data.pin_b1 = data.pin_b1;
	this.data.pin_b2 = data.pin_b2;
	this.data.initial_speed_a = data.initial_speed_a;
	this.data.initial_speed_b = data.initial_speed_b;

	// Categorize this device
	this.type = 'device_differential_motor';
	this.device_class = DEVICE_CLASS_OUTPUT;
	
	// Initialize the node (depending on the categorization)
	Device.prototype.init.call(this);

	return this;
}

// Inherit from the Device class
DeviceOutputDifferentialMotor.inheritsFrom(Device);

// All the prototype definitions MUST be added after this point


// Get a string with the configuration of the Device
DeviceOutputDifferentialMotor.prototype.toStringConfiguration = function()
{
	return 'Left:'+this.data.pin_a1 + ','+this.data.pin_a2 + '<br>Right:'+this.data.pin_b1 + ','+this.data.pin_b2;
};


// Get a string with an html block to configure this device type
DeviceOutputDifferentialMotor.prototype.listProperties = function()
{
	return 	"<table>"+
		"<tr>"+
			"<td>"+t[lang]['Left Motor']+" "+t[lang]['Pin']+" 1</td>"+
			"<td>"+createHtmlSelectorPins('select_motor_a_1_'+this.number, _hardware_manager.PINS_DIGITAL_OUTPUT, this.data.pin_a1)+"</td>"+
		"</tr>"+
		"<tr>"+
			"<td>"+t[lang]['Left Motor']+" "+t[lang]['Pin']+" 2</td>"+
			"<td>"+createHtmlSelectorPins('select_motor_a_2_'+this.number, _hardware_manager.PINS_DIGITAL_OUTPUT, this.data.pin_a2)+"</td>"+
		"</tr>"+
		"<tr>"+
			"<td>"+t[lang]['Right Motor']+" "+t[lang]['Pin']+" 1</td>"+
			"<td>"+createHtmlSelectorPins('select_motor_b_1_'+this.number, _hardware_manager.PINS_DIGITAL_OUTPUT, this.data.pin_b1)+"</td>"+
		"</tr>"+
		"<tr>"+
			"<td>"+t[lang]['Right Motor']+" "+t[lang]['Pin']+" 2</td>"+
			"<td>"+createHtmlSelectorPins('select_motor_b_2_'+this.number, _hardware_manager.PINS_DIGITAL_OUTPUT, this.data.pin_b2)+"</td>"+
		"</tr>"+

		"<tr>"+
			"<td>"+t[lang]['initial_speed']+" "+t[lang]['Right Motor']+"</td>"+
			"<td><input type='number'id='text_initial_speed_a' min='-255' max='255' value="+this.data.initial_speed_a+"></input></td>"+
		"</tr>"+
		"<tr>"+
			"<td>"+t[lang]['initial_speed']+" "+t[lang]['Left Motor']+"</td>"+
			"<td><input type='number'id='text_initial_speed_b' min='-255' max='255' value="+this.data.initial_speed_b+"></input></td>"+
		"</tr>"+

		"</table>";
};


// Add listeners to react the the changes in the device configuration
DeviceOutputDifferentialMotor.prototype.addListPropertiesListeners = function()
{
	// @todo validate pins here
};


// Updates an existing device with data read from the UI
// Parameter "n" indicates the number reference of the html elements to be read
DeviceOutputDifferentialMotor.prototype.updateDevice = function(n)
{
	console.log("updateDevice: DeviceOutputDifferentialMotor");

	this.data = 
		{
			pin_a1:parseInt($('#select_motor_a_2_'+n).val(), 10),
			pin_a2:parseInt($('#select_motor_b_1_'+n).val(), 10),
			pin_b1:parseInt($('#select_motor_b_2_'+n).val(), 10),
			pin_b2:parseInt($('#select_motor_a_1_'+n).val(), 10),
			initial_speed_a:parseInt($('#text_initial_speed_a'+n).val(), 10),
			initial_speed_b:parseInt($('#text_initial_speed_b'+n).val(), 10)
		};
};


// Create a new default device of this device type
DeviceOutputDifferentialMotor.prototype.createDefaultDevice = function()
{
	return new DeviceOutputDifferentialMotor( _device_manager.nextDeviceID(), t[lang]['new_device'], 
		{
			// @todo change this to a dynamic pin
			pin_a1:3,
			pin_a2:5,
			pin_b1:6,
			pin_b2:11,
			initial_speed_a:0,
			initial_speed_b:0
		}
	);
};


// Get a string with an html block to configure a node that uses this device type
// Parameter "data" is stored in the node that references the device
DeviceOutputDifferentialMotor.prototype.showServiceConfiguration = function(data)
{
	return 	"<table><tr>"+
			"<td>"+t[lang]['Left Motor']+"</td>"+
			"<td><input type='number'id='text_left_speed' min='-255' max='255' value="+data.left_speed+"></input></td>"+
		"</tr><tr>"+
			"<td>"+t[lang]['Right Motor']+"</td>"+
			"<td><input id='text_right_speed' type='number' min='-255' max='255' value="+data.right_speed+"></input></td>"+
		"</tr></table>";
};


// Add listeners to react the the changes in the service configutation of a node
// Whenever some data changes, the node needs to be updated
DeviceOutputDifferentialMotor.prototype.addServiceConfigurationListeners = function(node)
{
	$('#text_left_speed').on('change', function() 
		{
			console.log("BAM! text_left_speed");
			node.data.left_speed = parseInt($('#text_left_speed').val(), 10);
		});
	$('#text_right_speed').on('change', function() 
		{
			console.log("BAM! text_right_speed");
			node.data.right_speed = parseInt($('#text_right_speed').val(), 10);
		});
};


// Create an object with default initial data for a service of this device
DeviceOutputDifferentialMotor.prototype.defaultServiceData = function()
{
	return {left_speed: 0, right_speed:0};
};


// Return a string with hex encoded data representing this device
DeviceOutputDifferentialMotor.prototype.exportToArduino = function()
{
	return Device.prototype.exportToArduino.call(this)+ 
		padByte(DEVICE_OUTPUT_DIFFERENTIAL_MOTOR) +
		padByte(this.data.pin_a1) + padByte(this.data.pin_a2)+
		padByte(this.data.pin_b1) + padByte(this.data.pin_b2)+ ';';
};


// Return a string with hex encoded data representing a service that uses this device
DeviceOutputDifferentialMotor.prototype.exportToArduinoService = function(node)
{
	console.log("DeviceOutputDifferentialMotor ---------------:"+device_number+"  "+node.data.left_speed+"  "+node.data.right_speed);
	console.log(node);
	return 	padByte(SERVICE_OUTPUT_DIFFERENTIAL_MOTOR)+
		padByte(this.number)+padInt(node.data.left_speed+255)+padInt(node.data.right_speed+255)+';';
};

