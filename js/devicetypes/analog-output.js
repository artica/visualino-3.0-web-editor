
//
// Analog Output Device
//

// Constructor for the Analog Output Device
function DeviceOutputAnalog(id, name, data) 
{
	Device.call(this, id, name);

	// Create a new data field with the elements from this device type
	this.data.pin = data.pin;
	this.data.initial_value = data.initial_value;

	// Categorize this device
	this.type = 'device_output_analog';
	this.device_class = DEVICE_CLASS_OUTPUT;

	// Initialize the node (depending on the categorization)
	Device.prototype.init.call(this);

	return this;
}

// Inherit from the Device class
DeviceOutputAnalog.inheritsFrom(Device);

// All the prototype definitions MUST be added after this point



// Get a string with the configuration of the Device
DeviceOutputAnalog.prototype.toStringConfiguration = function()
{
	return this.data.pin + ', '+this.data.initial_value;
};


// Get a string with an HTML block to configure this device type
DeviceOutputAnalog.prototype.listProperties = function()
{
	html = "<table>"+
		"<tr>"+
			"<td>"+t[lang]['Pin']+"</td>"+
			"<td>"+createHtmlSelectorPins('select_output_pin_number_'+this.number, _hardware_manager.PINS_ANALOG_OUTPUT, this.data.pin)+"</td>"+
		"</tr>"+
		"<tr>"+
			"<td>"+t[lang]['Initial State']+"</td>"+
			"<td><input type='number' id='select_output_initial_value_"+this.number+"' value='0' min='0' max='255'></input></td>"+
		'</tr>'+
	'</table>';
	return html;
};


// Add listeners to react the the changes in the device configuration
DeviceOutputAnalog.prototype.addListPropertiesListeners = function()
{
	// @todo validate pins here
};


// Updates an existing device with data read from the UI
// Parameter "n" indicates the number reference of the html elements to be read
DeviceOutputAnalog.prototype.updateDevice = function(n)
{
	console.log("updateDevice: DeviceOutputAnalog:" + n);
	console.log('##################');
	console.log(this.data);
	console.log( $('#select_output_initial_value_'+n).val() );

	this.data = 
		{
			pin:parseInt($('#select_output_pin_number_'+n).val(), 10),
			initial_value:parseInt($('#select_output_initial_value_'+n).val(), 10)
		};
	console.log(this.data);
};


// Create a new default device of this device type
DeviceOutputAnalog.prototype.createDefaultDevice = function()
{
	return new DeviceOutputAnalog( _device_manager.nextDeviceID(), t[lang]['new_device'], 
		{
			pin:3,	// @todo change this to a dynamic pin
			initial_value:0
		}
	);
};


// Get a string with an html block to configure a node that uses this device type
// Parameter "data" is stored in the node that references the device
DeviceOutputAnalog.prototype.showServiceConfiguration = function(data)
{
	return "<table><tr><td>"+t[lang]['State']+"</td>"+			
		"<td><input type='number' id='text_output_pin_value' value='0' min='0' max='255'></input></td>"+
		"</tr></table>";
};


// Add listeners to react the the changes in the service configutation of a node
// Whenever some data changes, the node needs to be updated
DeviceOutputAnalog.prototype.addServiceConfigurationListeners = function(node)
{
	$('#select_output_state').on('change', function() 
		{
			node.data.new_value = parseInt($('#text_output_pin_value').val(), 10);
			console.log(node.data.new_value);
		});
};


// Create an object with default initial data for a service of this device
DeviceOutputAnalog.prototype.defaultServiceData = function()
{
	return {new_value: 0};
};


// Return a string with hex encoded data representing this device
DeviceOutputAnalog.prototype.exportToArduino = function()
{
	return Device.prototype.exportToArduino.call(this) +
			padByte(DEVICE_OUTPUT_ANALOG_PIN) +
			padByte(this.data.pin) + padByte(this.data.initial_value)+';';
};


// Return a string with hex encoded data representing a service that uses this device
DeviceOutputAnalog.prototype.exportToArduinoService = function(node)
{
	console.log("DeviceOutputAnalog ---------------:"+node.device);
	console.log(node.device);

	return 	padByte(SERVICE_OUTPUT_ANALOG_PIN)+
		padByte(this.number)+padByte(node.data.new_value)+';';
};
