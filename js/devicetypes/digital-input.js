
//
// Digital Input Device
//

// Constructor for the Digital Input Device
function DeviceInputDigital(id, name, data)
{
	Device.call(this, id, name);
	// Create a new data field with the elements from this device type
	this.data.pin = data.pin;
	this.data.active_type = data.active_type;

	// Categorize this device
	this.type = 'device_input_digital';
	this.device_class = DEVICE_CLASS_INPUT;

	// Initialize the node (depending on the categorization)
	Device.prototype.init.call(this);

	return this;
}

// Inherit from the Device class
DeviceInputDigital.inheritsFrom(DeviceInput);

// All the prototype definitions MUST be added after this point


// List of variables that can be read from this device
DeviceInputDigital.prototype.values = [{value:0, name:'value'}];


// Get a string with the configuration of the Device
DeviceInputDigital.prototype.toStringConfiguration = function()
{
	return this.data.pin + ', '+this.data.active_type;
};


// Get a string with an html block to configure this device type
DeviceInputDigital.prototype.listProperties = function()
{
	html = "<table>"+
		"<tr>"+
			"<td>"+t[lang]['Pin']+"</td>"+
			"<td>"+createHtmlSelectorPins('select_input_pin_number_'+this.number, _hardware_manager.PINS_DIGITAL_INPUT, this.data.pin)+"</td>"+
		"</tr>"+
		"<tr>"+
			"<td>"+t[lang]['Active']+"</td>"+
			"<td>"+createHtmlSelectorHighLow('select_input_active_state_'+this.number, this.data.active_type)+"</td>"+
		'</tr>'+
	'</table>';
	return html;
};


// Add listeners to react the the changes in the device configuration
DeviceInputDigital.prototype.addListPropertiesListeners = function()
{
	// @todo validate pins here
	console.log("-----------------------Badabim!!!------------------------");
};


// Updates an existing device with data read from the UI
// Parameter "n" indicates the number reference of the html elements to be read
DeviceInputDigital.prototype.updateDevice = function(n)
{
	console.log("updateDevice: DeviceInputDigital");

	this.data = 
		{
			pin:parseInt($('#select_input_pin_number_'+n).val(), 10),
			active_type:parseInt($('#select_input_active_state_'+n).val(), 10)
		};
};


// Create a new default device of this device type
DeviceInputDigital.prototype.createDefaultDevice = function()
{
	return new DeviceInputDigital( _device_manager.nextDeviceID(), t[lang]['new_device'], 
		{
			pin:3, // @todo change this to a dynamic pin
			active_type:0
		}
	);
};


// Get a string with an html block to configure a node that uses this device type
// Parameter "data" is stored in the node that references the device
DeviceInputDigital.prototype.showServiceConfiguration = function(data)
{
	return "";
};


// Add listeners to react the the changes in the service configutation of a node
// Whenever some data changes, the node needs to be updated
DeviceInputDigital.prototype.addServiceConfigurationListeners = function(node)
{
	// This node has no service configuration
};


// Create an object with default initial data for a service of this device
DeviceInputDigital.prototype.defaultServiceData = function()
{
	return {};
};


// Return a string with hex encoded data representing this device
DeviceInputDigital.prototype.exportToArduino = function()
{
	return Device.prototype.exportToArduino.call(this) +
			padByte(DEVICE_INPUT_DIGITAL_PIN) +
			padByte(this.data.pin) + padByte(this.data.active_type)+';';
};


// Return a string with hex encoded data representing a service that uses this device
DeviceInputDigital.prototype.exportToArduinoService = function(node)
{
	console.log("DeviceInputDigital ---------------:"+node.device);
	console.log(node.device);

	return padByte(SERVICE_INPUT_DIGITAL_PIN) + padByte(this.number)+';';
};
