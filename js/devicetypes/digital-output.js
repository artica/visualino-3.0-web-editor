
//
// Digital Output Device
//

// Constructor for the Digital Output Device
function DeviceOutputDigital(id, name, data) 
{
	Device.call(this, id, name);

	// Create a new data field with the elements from this device type
	this.data.pin = data.pin;
	this.data.initial_state = data.initial_state;

	// Categorize this device
	this.type = 'device_output_digital';
	this.device_class = DEVICE_CLASS_OUTPUT;

	// Initialize the node (depending on the categorization)
	Device.prototype.init.call(this);

	return this;
}

// Inherit from the Device class
DeviceOutputDigital.inheritsFrom(Device);

// All the prototype definitions MUST be added after this point



// Get a string with the configuration of the Device
DeviceOutputDigital.prototype.toStringConfiguration = function()
{
	return this.data.pin + ', '+this.data.initial_state;
};


// Get a string with an HTML block to configure this device type
DeviceOutputDigital.prototype.listProperties = function()
{
	//console.log('*****************************');
	//console.log(this);
	html = "<table>"+
		"<tr>"+
			"<td>"+t[lang]['Pin']+"</td>"+
			"<td>"+createHtmlSelectorPins('select_output_pin_number_'+this.number, _hardware_manager.PINS_DIGITAL_OUTPUT, this.data.pin)+"</td>"+
		"</tr>"+
		"<tr>"+
			"<td>"+t[lang]['Initial State']+"</td>"+
			"<td>"+createHtmlSelectorHighLow('select_output_initial_state_'+this.number, this.data.initial_state)+"</td>"+
		'</tr>'+
	'</table>';
	return html;
};


// Add listeners to react the the changes in the device configuration
DeviceOutputDigital.prototype.addListPropertiesListeners = function()
{
	// @todo validate pins here
};


// Updates an existing device with data read from the UI
// Parameter "n" indicates the number reference of the html elements to be read
DeviceOutputDigital.prototype.updateDevice = function(n)
{
	console.log("updateDevice: DeviceOutputDigital:" + n);
	console.log('##################');
	console.log(this.data);

	this.data = 
		{
			pin:parseInt($('#select_output_pin_number_'+n).val(), 10),
			initial_state:parseInt($('#select_output_initial_state_'+n).val(), 10)
		};
	console.log(this.data);
};


// Create a new default device of this device type
DeviceOutputDigital.prototype.createDefaultDevice = function()
{
	return new DeviceOutputDigital( _device_manager.nextDeviceID(), t[lang]['new_device'], 
		{
			pin:3,	// @todo change this to a dynamic pin
			initial_state:0
		}
	);
};


// Get a string with an html block to configure a node that uses this device type
// Parameter "data" is stored in the node that references the device
DeviceOutputDigital.prototype.showServiceConfiguration = function(data)
{
	return "<table><tr><td>"+t[lang]['State']+"</td>"+			
		"<td>"+createHtmlSelectorHighLow('select_output_state', data.new_value)+"</td>"+
		"</tr></table>";
};


// Add listeners to react the the changes in the service configutation of a node
// Whenever some data changes, the node needs to be updated
DeviceOutputDigital.prototype.addServiceConfigurationListeners = function(node)
{
	$('#select_output_state').on('change', function() 
		{
			node.data.new_value = parseInt($('#select_output_state').val(), 10);
			console.log(node.data.new_value);
		});
};


// Create an object with default initial data for a service of this device
DeviceOutputDigital.prototype.defaultServiceData = function()
{
	return {new_value: 1};
};


// Return a string with hex encoded data representing this device
DeviceOutputDigital.prototype.exportToArduino = function()
{
	return Device.prototype.exportToArduino.call(this) +
			padByte(DEVICE_OUTPUT_DIGITAL_PIN) +
			padByte(this.data.pin) + padByte(this.data.initial_state)+';';
};


// Return a string with hex encoded data representing a service that uses this device
DeviceOutputDigital.prototype.exportToArduinoService = function(node)
{
	console.log("DeviceOutputDigitaServicel ---------------:"+node.device);
	console.log(node);

	return 	padByte(SERVICE_OUTPUT_DIGITAL_PIN)+
		padByte(this.number)+padByte(node.data.new_value)+';';
};
