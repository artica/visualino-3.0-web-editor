// Base device type identifiers (for arduino)
var DEVICE_OUTPUT_DIGITAL_PIN 			= 0x10;
var DEVICE_OUTPUT_ANALOG_PIN 			= 0x11;
var DEVICE_OUTPUT_DIFFERENTIAL_MOTOR 	= 0x12;
var DEVICE_INPUT_DIGITAL_PIN 			= 0x20;
var DEVICE_INPUT_ANALOG_PIN 			= 0x21;


function DeviceTypes() 
{
	this.devices = 
		[
			new DeviceTypeDigitalInput(),
			new DeviceTypeDigitalOutput(),
			new DeviceTypeDifferentialMotor()
		]
		
	this.device_selected = 0;
}

DeviceTypes.prototype = 
{
	unselect_device: function(index) 
	{
		this.device_selected = -1;
	}
}


