
var NODE_RADIUS = 50;

self.onmessage = function(e) 
{
	//console.log("work work work!");
	//console.log("recv this");
	//console.log(e);
	var nodes = e.data.nodes;
	var updated = false;
	if (nodes)
	{	
		//go through all combos of nodes, if two are too close, spread them slightly further away		
		for (var i = 0; i < nodes.length; i++) 
		{
			for (var j = 0; j < nodes.length; j++) 
			{
				if (i != j) 
				{
					//@todo: distance should be based on radius of nodes, not just 50
					if (distance(nodes[i], nodes[j]) < NODE_RADIUS) 
					{
						updated = true;

						// determine which one is lower x
						var lowerx = i;
						var higherx = j;
						if (nodes[j]['x'] < nodes[i]['x']) 
						{
							lowerx = j;
							higherx = i;
						}
						// node with lower x is decreased proportional to distance
						nodes[lowerx]['x'] = nodes[lowerx]['x'] - 1;
						nodes[higherx]['x'] = nodes[higherx]['x'] + 1;
						
						// determine which one is lower y
						var lowery = i;
						var highery = j;
						if (nodes[j]['y'] < nodes[i]['y']) 
						{
							lowery = j;
							highery = i;
						}
						
						
						// same for y
						nodes[lowery]['y'] = nodes[lowery]['y'] - 1;
						nodes[highery]['y'] = nodes[highery]['y'] + 1;
					
					}
				}
			}
		}
		
		postMessage({nodes:nodes, updated:updated});
    } else {
    	postMessage('hello world');
    }
};


self.distance = function(node1, node2) 
{
    var distX = node1.x - node2.x;
	var distY = node1.y - node2.y;
	return Math.sqrt(distX * distX + distY * distY);
}
