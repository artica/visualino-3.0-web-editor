
var lang = 'en';
var t = [];

//
// English
//

t['en'] = [];

// index.html
t['en']['bad_canvas'] = 'Couldn\'t access main_canvas';
t['en']['no_canvas'] = 'Your browser has no canvas support! That\'s bad.';


t['en']['constant'] = 'Constant';
t['en']['variable'] = 'Variable';
t['en']['device'] = 'Device';

t['en']['initial_value'] = 'Initial value';

t['en']['variable_bool'] = 'Bool';
t['en']['variable_byte'] = 'Byte';
t['en']['variable_int'] = 'Int';
t['en']['variable_long'] = 'Long';


t['en']['new_variable'] = 'New Variable';

t['en']['initial_value'] = 'Initial value';


t['en']['function_copy'] = 'Copy';
t['en']['function_map'] = 'Map';
t['en']['function_fx'] = 'f(x)';
t['en']['function_range'] = 'Range';


t['en']['devices'] = 'Devices';
t['en']['variables'] = 'Variables';



t['en']['image'] = 'Image';
t['en']['name'] = 'Name';
t['en']['type'] = 'Type';
t['en']['configuration'] = 'Configuration';


t['en']['title'] = 'Title';
t['en']['author'] = 'Author';
t['en']['email'] = 'E-mail';
t['en']['tag'] = 'Tag';
t['en']['search'] = 'Search';
t['en']['search_by'] = 'Search by';


t['en']['share_with'] = 'Share with';
t['en']['share_list'] = 'Share list';
t['en']['user'] = 'User';
t['en']['share'] = 'Share';
t['en']['group'] = 'Group';

t['en']['share_error_user_not_found'] = 'The desired user was not found in the Visualino database!<br>Make sure the Fiware name/email is correct, and the user has logged in into Visualino at least once.';
t['en']['share_successeful'] = 'The tree was successefully shared.';

t['en']['save_tree_created'] = 'A new tree was created.';
t['en']['save_tree_updated'] = 'The existing tree was updated.';

t['en']['delete_tree_deleted'] = 'The tree was deleted.';
t['en']['delete_tree_shared'] = 'The tree cannot be deleted since it\'s being shared. Please delete all the shares before deleting a tree.';

t['en']['shared_with'] = 'Shared with';
t['en']['remove_all_shares'] = 'Remove all shares';
t['en']['share_remove_successeful'] = 'The share was successefully removed.';
t['en']['remove'] = 'Remove';



t['en']['device_input_digital'] = 'Digital Input';
t['en']['device_input_analog'] = 'Analog Input';
t['en']['device_output_digital'] = 'Digital Output';
t['en']['device_output_analog'] = 'Analog Output';
t['en']['device_analog_input'] = 'Analog Input';
t['en']['device_motor'] = 'Motor';
t['en']['new_device'] = 'New Device';

t['en']['type'] = 'Type';

// node-function
t['en']['function'] = 'Function';

// node-test
t['en']['test'] = 'Test';
t['en']['test_equal'] = 'Equal';
t['en']['test_different'] = 'Different';
t['en']['test_bigger'] = 'Bigger';
t['en']['test_smaller'] = 'Smaller';
t['en']['test_inside'] = 'Inside Range';
t['en']['test_outside'] = 'Outside Range';


t['en']['warning'] = 'WARNING';
t['en']['error'] = 'ERROR';

// graph-nodetypes.js
t['en']['Root'] = 'Root';
t['en']['Root Info'] = 'This is the Root node! Where all the magic starts';
t['en']['Target'] = 'Target Platform';
t['en']['JSON Import'] = 'JSON Import';
t['en']['JSON Import Info'] = 'This is the JSON Import node! It allows you to import composite behavior trees from an encoded json string.';
t['en']['JSON String'] = 'JSON String';
t['en']['JSON Child'] = 'Can\'t import JSON if the node already has children.';
t['en']['Parallel'] = 'Parallel';
t['en']['Parallel Info'] = 'This is the Parallel node! It allows you to start a sub-tree whose children all run in parallel.';
t['en']['parallel_require_one'] = 'Require One';
t['en']['parallel_require_all'] = 'Require All';
t['en']['Completion Policy'] = 'Completion Policy';
t['en']['Failure Policy'] = 'Failure Policy';
t['en']['Sequence'] = 'Sequence';
t['en']['Sequence Info'] = 'This is the Sequence node! It allows you to start a sub-tree whose children all run in sequence.';
t['en']['Priority Selector'] = 'Priority Selector';
t['en']['Status Override'] = 'Status Override';
t['en']['status_fail'] = 'Fail';
t['en']['status_run'] = 'Run';
t['en']['status_success'] = 'Success';
t['en']['override_fail'] = 'Failure Override';
t['en']['override_run'] = 'Running Override';
t['en']['override_success'] = 'Success Override';
t['en']['Motor'] = 'Motor';
t['en']['Motor Info'] = 'This is the Motor node! It allows you to control a motor.';
t['en']['Right Motor'] = 'Right Motor';
t['en']['Left Motor'] = 'Left Motor';
t['en']['initial_speed'] = 'Initial Speed';
t['en']['Digital'] = 'Digital';
t['en']['Analogue'] = 'Analogue';
t['en']['PWM'] = 'PWM';
t['en']['Pin Type'] = 'Pin Type';
t['en']['Speed'] = 'Speed';
t['en']['Servo'] = 'Servo';
t['en']['Servo Info'] = 'This is the Servo node! It allows you to control a servo.';
t['en']['Servo Range'] = 'Servo Range';
t['en']['Servo Range Info'] = 'This is the Servo Range node! It allows you to control a servo.';
t['en']['Pin'] = 'Pin';
t['en']['Initial State'] = 'Initial State';
t['en']['Servo Incrementor'] = 'Servo Incrementor';
t['en']['Servo Incrementor Info'] = 'This is the Servo Incrementor node! It allows you to control a servo with incremental values.';
t['en']['Servo Threshold'] = 'Servo Threshold';
t['en']['Servo Threshold Info'] = 'This is the Servo Threshold node! It allows you to check values from the servo.';
t['en']['Threshold'] = 'Threshold';
t['en']['Pass Type'] = 'Type';
t['en']['Pass High'] = 'High Pass';
t['en']['Pass Low'] = 'Low Pass';
t['en']['Low'] = 'Low';
t['en']['High'] = 'High';
t['en']['LED'] = 'LED';
t['en']['LED Info'] = 'This is the LED node! It allows you to change the value of a LED.';
t['en']['Value'] = 'Value';
t['en']['Add'] = 'Add';
t['en']['State'] = 'State';
t['en']['Active'] = 'Active';
t['en']['Sensor'] = 'Sensor';
t['en']['Sensor Info'] = 'This is the Sensor node! It allows you to read value from a sensor.';
t['en']['Video'] = 'Video';
t['en']['Video Info'] = 'This is the Video node! It allows you to play a video.';
t['en']['Image'] = 'Image';
t['en']['Image Info'] = 'This is the Image node! It allows you to display an image.';
t['en']['Delay Time'] = 'Delay Time';
t['en']['Delay Time Info'] = 'This is the Delay Time node! It allows you to delay the following action for a certain number of miliseconds.';
t['en']['Assigned'] = 'Assigned';
t['en']['Sensor IR'] = 'Sensor IR';
t['en']['Left Bumper'] = 'Left Bumper';
t['en']['Right Bumper'] = 'Right Bumper';
t['en']['min'] = 'Minimum';
t['en']['max'] = 'Maximum';
t['en']['inc'] = 'Increment';

// graph-graph.js
t['en']['id'] = 'ID';
t['en']['Name'] = 'Name';
t['en']['tags'] = 'Tags';
t['en']['description'] = 'Description';
t['en']['Node Type'] = 'Node Type';
t['en']['unknown_type'] = 'Trying to import a node of type unknown to current version of visualino.';
t['en']['connections_error'] = 'Trying to import a connection to non existing nodes.';
t['en']['del'] = 'To delete a node just press the delete key when the node is selected.';
t['en']['delete_node'] = 'Delete node';
t['en']['Export .json'] = 'Export .json';
t['en']['0root'] = 'Warning: You should have at least 1 root node to export correctly.';
t['en']['manyroot'] = 'Warning: You should have only 1 root node to export correctly.';
t['en']['error_duplicate_unique_node'] = 'It is not allowed to add duplicates of unique nodes.';
t['en']['error_cannot_instantiate_node'] = 'It is not possible to create that type of node.';
t['en']['error_leaf'] = 'It is not allowed to add more nodes to leaf nodes.';
t['en']['error_loop'] = 'A node cannot be a child of one of its descendants.';
t['en']['error_too_many_children'] = 'This node cannot have more children.';
t['en']['error_missing_parent'] = 'This node needs to be connected to the tree.';
t['en']['error_missing_single_child'] = 'This node needs to have exactlly one child.';
t['en']['error_missing_children'] = 'This node needs to have more children.';

t['en']['warning_missing_children'] = 'This node should have mode children.';

t['en']['error_upload_invalid_tree'] = 'The tree is invalid. Please fix the problematic nodes before uploading again.';


t['en']['confirm_load'] = 'Are you sure you want to import a JSON file? Unsaved changes to the current tree will be lost.';
t['en']['COM Port'] = "COM Port:";
t['en']['Connect'] = "Connect";
t['en']['Disconnect'] = "Disconnect";
t['en']['No Connection'] = "The interface needs to be connected to a port before sending data!";
t['en']['Communication Problem'] = "Problems in data transmission to the Arduino!";
t['en']['Communication Fail'] = "Problem starting communication... Is the Arduino connected?";
t['en']['No Websocket'] = "The websocket is not connected to the COM gateway server!";
t['en']['error_tree_upload'] = "Error uploading the tree to the Arduino!";


//
// Portuguese
//

t['pt'] = [];

// index.html
t['pt']['bad_canvas'] = 'main_canvas não está acessível';
t['pt']['no_canvas'] = 'O seu browser não suporta o elemento canvas! É mau.';


// graph-devicetypes.js
t['pt']['device'] = '';

t['pt']['constant'] = 'Constante';
t['pt']['variable'] = 'Variável';
t['pt']['device'] = 'Dispositivo';

t['pt']['initial_value'] = 'Valor inicial';

t['pt']['variable_bool'] = 'Booleano';
t['pt']['variable_byte'] = 'Byte';
t['pt']['variable_int'] = 'Inteiro';
t['pt']['variable_long'] = 'Long';



t['pt']['new_variable'] = 'Nova Variável';

t['pt']['initial_value'] = 'Valor inicial';


t['pt']['function_copy'] = 'Cópia';
t['pt']['function_map'] = 'Mapeamento';
t['pt']['function_fx'] = 'f(x)';
t['pt']['function_range'] = 'Range';



t['pt']['devices'] = 'Dispositivos';
t['pt']['variables'] = 'Variáveis';


t['pt']['image'] = 'Imagem';
t['pt']['name'] = 'Nome';
t['pt']['type'] = 'Tipo';
t['pt']['configuration'] = 'Configuração';


t['pt']['title'] = 'Título';
t['pt']['author'] = 'Autor';
t['pt']['email'] = 'E-mail';
t['pt']['tag'] = 'Tag';
t['pt']['search'] = 'Pesquisar';
t['pt']['search_by'] = 'Pesquisar por';


t['pt']['share_with'] = 'Partilhar com';
t['pt']['user'] = 'Utilizador';
t['pt']['share'] = 'Partilhar';
t['pt']['group'] = 'Grupo';

t['pt']['shared_with'] = 'Partilhada com';



t['pt']['device_input_digital'] = 'Digital Input';
t['pt']['device_input_analog'] = 'Input Analógico';
t['pt']['device_output_digital'] = 'Output Digital';
t['pt']['device_output_analog'] = 'Output Analógico';
t['pt']['device_motor'] = 'Motor';
t['pt']['new_device'] = 'Novo Dispositivo';

t['pt']['type'] = 'Tipo';


// node-function
t['pt']['function'] = 'Função';

// node-test
t['pt']['test'] = 'Teste';

t['pt']['test_equal'] = 'Igual';
t['pt']['test_different'] = 'Diferente';
t['pt']['test_bigger'] = 'Maior';
t['pt']['test_smaller'] = 'Menos';
t['pt']['test_inside'] = 'Dentro do Intervalo';
t['pt']['test_outside'] = 'Fora do Intervalo';

t['pt']['warning'] = 'AVISO';
t['pt']['error'] = 'ERRO';


// graph-nodetypes.js
t['pt']['Root'] = 'Raíz';
t['pt']['Root Info'] = 'Este é o nó Raíz! Onde toda a magia começa.';
t['pt']['Target'] = 'Plataforma alvo';
t['pt']['JSON Import'] = 'JSON Import';
t['pt']['JSON Import Info'] = 'Este é o nó que permite importar JSON, ou seja, podes ter árvores dentro de nós e assim aumentar a complexidade dos teus programas!';
t['pt']['JSON String'] = 'JSON String';
t['pt']['JSON Child'] = 'Não é possivel importar JSON por o nó já ter filhos.';
t['pt']['Parallel'] = 'Paralelo';
t['pt']['Parallel Info'] = 'Este é o nó Paralelo! Permite começar um ramo onde todas as folhas correm em paralelo, ou seja, em simultâneo.';
t['pt']['parallel_require_one'] = 'Necessário Um';
t['pt']['parallel_require_all'] = 'Necessários Todos';
t['pt']['Completion Policy'] = 'Regra de Sucesso';
t['pt']['Failure Policy'] = 'Regra de Falha';
t['pt']['Sequence'] = 'Sequência';
t['pt']['Sequence Info'] = 'Este é o nó Sequência! Permite começar um ramo onde todas as folhas correm em sequência, ou seja, uma folha a seguir à outra.';
t['pt']['Priority Selector'] = 'Selector de Prioridade';
t['pt']['Status Override'] = 'Status Override';

t['pt']['status_fail'] = 'Falha';
t['pt']['status_run'] = 'Execução';
t['pt']['status_success'] = 'Successo';
t['pt']['override_fail'] = 'Falha devolve';
t['pt']['override_run'] = 'Execução devolve';
t['pt']['override_success'] = 'Successo devolve';
t['pt']['Motor'] = 'Motor';
t['pt']['Motor Info'] = 'Este é o nó Motor, vai permitir controlar um único motor.';
t['pt']['Right Motor'] = 'Motor Direito';
t['pt']['Left Motor'] = 'Motor Esquerdo';
t['pt']['initial_speed'] = 'Velocidade inicial';
t['pt']['Digital'] = 'Digital';
t['pt']['Analogue'] = 'Analógico';
t['pt']['PWM'] = 'PWM';
t['pt']['Pin Type'] = 'Tipo de pin';
t['pt']['Speed'] = 'Velocidade';
t['pt']['Servo'] = 'Servo';
t['pt']['Servo Info'] = 'Este é o nó Servo, vai permitir controlar um motor servo.';
t['pt']['Servo Range'] = 'Servo Range';
t['pt']['Servo Range Info'] = 'Este é o nó Servo Range, vai permitir controlar um motor servo.';
t['pt']['Pin'] = 'Pino';
t['pt']['Initial State'] = 'Estado Inicial';
t['pt']['Servo Incrementor'] = 'Incrementador de Servo';
t['pt']['Servo Incrementor Info'] = 'Este é o nó que incrementa o Servo, permite controlar o Servo com um valor que incrementa ou decrementa.';
t['pt']['Servo Threshold'] = 'Decisor do Servo';
t['pt']['Servo Threshold Info'] = 'Este é o nó Decisor do Servo, permite estabelecer valores para a utilização do Servo.';
t['pt']['Threshold'] = 'Limitador';
t['pt']['Pass Type'] = 'Tipo';
t['pt']['Pass High'] = 'Passa Alto';
t['pt']['Low'] = 'Baixo';
t['pt']['High'] = 'Alto';
t['pt']['LED'] = 'LED';
t['pt']['LED Info'] = 'Este é o nó do LED, permite mudar o estado do LED.';
t['pt']['Value'] = 'Valor';
t['pt']['Add'] = 'Adicionar';
t['pt']['State'] = 'Estado';
t['pt']['Active'] = 'Activo';
t['pt']['Sensor'] = 'Sensor';
t['pt']['Sensor Info'] = 'Este é o nó Sensor, permite fazer uma leitura ao Sensor.';
t['pt']['Video'] = 'Video';
t['pt']['Video Info'] = 'Este é o nó Video, permite ler e reproduzir um video.';
t['pt']['Image'] = 'Imagem';
t['pt']['Image Info'] = 'Este é o nó Imagem, permite ler e reproduzir uma imagem.';
t['pt']['Delay Time'] = 'Esperar';
t['pt']['Delay Time Info'] = 'Este é o nó Esperar, permite esperar um determinado periodo de tempo em milisegundos.';
t['pt']['Assigned'] = 'Assignado';
t['pt']['Sensor IR'] = 'Sensor IR';
t['pt']['Left Bumper'] = 'Bumper Esquerdo';
t['pt']['Right Bumper'] = 'Bumper Direito';
t['pt']['min'] = 'Minimo';
t['pt']['max'] = 'Máximo';
t['pt']['inc'] = 'Incremento';

// graph-graph.js
t['pt']['id'] = 'ID';
t['pt']['Name'] = 'Nome';
t['pt']['tags'] = 'Tags';
t['pt']['description'] = 'Descrição';

t['pt']['Node Type'] = 'Tipo de nó';
t['pt']['unknown_type'] = 'Nó de tipo desconhecido';
t['pt']['connections_error'] = 'Tentativa de importação de uma ligação de nós não existentes.';
t['pt']['del'] = 'Para apagar um nó basta premir a tecla delete quando este se encontra seleccionado.';
t['pt']['delete_node'] = 'Remover nó';
t['pt']['Export .json'] = 'Exportar .json';
t['pt']['0root'] = 'Aviso: Deve ter 1 nó raíz para exportar correctamente.';
t['pt']['manyroot'] = 'Aviso: Deve ter somente 1 nó raíz para exportar correctamente.';
t['pt']['error_duplicate_unique_node'] = 'Não é permitido adicionar duplicados de nós unicos.';
t['pt']['error_cannot_instantiate_node'] = 'Não é possível criar esse tipo de nó.';
t['pt']['error_leaf'] = 'Não é permitido adicionar mais nós a um nó terminal.';
t['pt']['error_loop'] = 'Um nó não pode ser filho de um dos seus descendentes.';
t['pt']['error_too_many_children'] = 'Este nó não pode ter mais filhos.';
t['pt']['error_missing_parent'] = 'Este nó precisa de estar ligado à árvore.';
t['pt']['error_missing_single_child'] = 'Este nó precisa de ter exactamente um filho.';
t['pt']['error_missing_children'] = 'Este nó precisa de ter mais filhos.';
t['pt']['warning_missing_children'] = 'Este nó deveria ter mais filhos.';

t['pt']['error_upload_invalid_tree'] = 'Esta árvore é inválida. É necessario arranjar os nós problemáticos antes de fazer um novo upload.';


t['pt']['confirm_load'] = 'Tem a certeza que deseja importar um ficheiro JSON? Alterações à àrvore não guardadas até ao momento serão perdidas.';
t['pt']['COM Port'] = "Porta COM:";
t['pt']['Connect'] = "Ligar:";
t['pt']['Disconnect'] = "Desligar:";
t['pt']['No Connection'] = "A interface precisa de estar ligada a uma porta para poder enviar dados!";
t['pt']['Communication Problem'] = "Problemas na comunicação com o Arduino!";
t['pt']['Communication Fail'] = "Problema ao iniciar a comunicação... O Arduino está ligado?";
t['pt']['No Websocket'] = "O websocket não está ligado ao COM gateway server!";
t['pt']['error_tree_upload'] = "Erro ao fazer upload da árvore para o Arduino!";


