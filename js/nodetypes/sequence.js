
//
// Sequence Node
//

function NodeSequence(id, name, data) 
{
	Node.call(this, id, name);

	// Categorize this node
	this.type = 'node_sequence';

	this.unique = false;
	this.min_children = 2;
	this.max_children = 999;

	// Initialize the node (depending on the categorization)
	Node.prototype.init.call(this);

	return this;
}

// Inherit from the Node class
NodeSequence.inheritsFrom(Node);

// All the prototype definitions MUST be added after this point

// Default data to create new nodes of this type
NodeSequence.prototype.DEFAULT_NODE_DATA  = {};


// Returns a block of HTML with a table with the necessary elements to configute this node type
NodeSequence.prototype.listProperties = function() 
{
	return '';
};


// Return a string with hex encoded data representing this node
NodeSequence.prototype.exportNodeToArduino = function() 
{
		return Node.prototype.exportNodeToArduino.call(this) +
			padByte(NODE_SEQUENCE) + padByte(this.children.length)+';';
};


