
//
// Root Node
//


function NodeRoot(id, name, data) 
{
	Node.call(this, id, name);

	// Categorize this node
	this.type = 'node_root';

	this.unique = true;
	this.min_children = 1;
	this.max_children = 1;


	// Initialize the node (depending on the categorization)
	Node.prototype.init.call(this);

	return this;
}

// Inherit from the Node class
NodeRoot.inheritsFrom(Node);

// All the prototype definitions MUST be added after this point


// Default data to create new nodes of this type
NodeRoot.prototype.DEFAULT_NODE_DATA  = {};


// Get a string with an HTML block to configure this node type
NodeRoot.prototype.listProperties = function() 
{
	//console.log("ROOT listProperties");
	//console.log(_device_manager.devices);
	var output = '<table><tr><th class="td_device_image">Image</th><th class="td_device_name">Name</th>'+
		'<th class="td_device_type">Type</th><th class="td_device_configuration">Configuration</th></tr></table>';

	// List the hardware device configuration
	for (var i=0; i<_device_manager.devices.length; i++) 
	{
		var device = _device_manager.devices[i];
		// Create the lines of the table with the basic device information (always visible)
		output += '<table><tr class="tr_device_select" device_number="'+i+'">'+
			'<td class="td_device_image"><image class="image_list" src="'+device.getImage().src+'"></image></td>'+
			'<td style="td_device_name">'+device.toString()+'</td>'+
			'<td style="td_device_type">'+device.toStringType()+'</td>'+
			'<td style="td_device_configuration">'+device.toStringConfiguration()+'</td></tr></table>';

		// Create the lines of the table with the buttons to update/delete/hide the devices
		output += "<div id='div_device_properties_"+i+"' style='display:"+
			( device.visible? 'block': 'none') + "'><table><tr>"+
			"<td><img class='button_update_device' device_number='"+i+"' src='images/header/update.svg'></img></td>"+
			"<td><img class='button_delete_device' device_number='"+i+"' src='images/header/delete.svg'></img></td>"+
			"<td><img class='button_collapse_device' device_number='"+i+"' src='images/header/collapse.svg'></img></td>"+
			"</tr></table><table>"+
			"<tr><td>"+t[lang]['type']+"</td><td>"+
			device.toStringType();

		// Get the image used for the device
		var image = device.getImage();

		// Create the expandable areas with device information
		output += "</td></tr>"+
			"<tr><td>"+t[lang]['Name']+"</td><td><input type='text' id='name_device_"+i+"'"+
			" value='"+device.name+"'></input></td></tr>"+
			"<tr><td>"+t[lang]['Image']+"</td><td><img id='image_device_"+i+"' class='image_device' "+
			"img_id='"+image.img_id+"' device_number='"+i+"' src='"+image.src+"'/></td></tr>"+
			"</table>"+
			"<div id='images_selector_"+i+"'></div>";

		// Now we add the configuration properties for the selected device type
		output += device.listProperties() +
			"</div>";
				
	}
	output += '<tr><td colspan="4"><hr></td></tr></table>';

	// Adds a line with an option to add a new device
	output += "<table><tr>"+
				"<td>"+t[lang]['Add']+"</td>"+		
				"<td><select id='select_new_device'>";

	// List all the existing device types, selecting the last selected type as the default
	for (d = 0; d < _device_manager.device_types.length; d++) 
	{
		output += "<option id='select_device'"+(_device_manager.new_device_type===d? " selected='true'": "")+
			" value='"+d+"'>" + _device_manager.device_types[d].toStringType() + "</option>";
		//console.log(_device_manager.device_types[d].toStringType());
	}

	output += "</select></td>"+
			"<td><img id='img_new_device' src='images/header/add.svg'></img></td>"+
			"</tr></table>";

	return output;
};


// Add the listeners for changes in the configuration
NodeRoot.prototype.addListPropertiesListeners = function() 
{
	var thisnode = this;
	// Selector to change the type of the new device to be created
	$('#select_new_device').on('change', function() 
		{
			_device_manager.new_device_type = parseInt($('#select_new_device').val(), 10);
		});

	// Button to add a new device to the device list with default values
	$('#img_new_device').on('click', function() 
		{
			// Create a new device based to the 'select_new_device' selector value
			var new_device = _device_manager.device_types[_device_manager.new_device_type].createDefaultDevice();
			_device_manager.devices.push(new_device);

			console.log("##########################################################################");
			console.log(new_device);
			// We also need to update the active nodetypes
			// If a new input our output is created, the respective nodec can be instantiated
			_node_manager.refreshDevices();

			console.log(new_device);

			// Now we refresh the properties window with the new data
			_ui.displayNodeProperties();

			// Save the new version of the tree with the new device
			_version_manager.addVersion('change_add_device');
		});

	// Button to update the settings of an existing device
	$('.button_update_device').on('click', function() 
		{
			// Try to update the device settings
			console.log("Updating...");

			// Get the device number and new device name from the forms
	        var device_number = parseInt(getHtmlAttr(event, 'device_number'), 10);
			var name = $('#name_device_'+device_number).val();

			// Get the a pointer to the old device to be updated
			var device = _device_manager.devices[device_number];

			// Update the device with the new settings from the UI
			device.updateDevice(device_number);

			console.log(device);

			console.log('-------> #image_device_'+device_number);

			// Now we set the (possibly) new name and image for the device
			device.name = name;
			device.setImage( $('#image_device_'+device_number).attr('img_id') );

			//console.log( 'zomg:' + $('#img_selected').attr('img_id') );

			// Now we refresh the properties window with the new data
			_ui.displayNodeProperties();

			// Save the new version of the tree with the updated device
			_version_manager.addVersion('change_update_device');
			
		});

	// Button to delete an existing device
	$('.button_delete_device').on('click', function() 
		{
			// Try to update the device settings
			console.log("Deleting...");
	        var device_number = parseInt(getHtmlAttr(event, 'device_number'), 10);

	        // Remove the desired position from the device list
			_device_manager.devices.splice(device_number, 1);

			// We also need to update the active nodetypes
			// If all the inputs our outputs are deleted, the respective nodes cannot be instantiated
			_node_manager.refreshDevices();

			// Now we refresh the properties window with the new device list
			_ui.displayNodeProperties();

			// Save the new version of the tree without the removed device
			_version_manager.addVersion('change_remove_device');

		});


	$('.button_collapse_device').on('click', function(event) 
		{
	        var device_number = parseInt(getHtmlAttr(event, 'device_number'), 10);
			collapseDiv('div_device_properties_'+device_number, 300);				
			_device_manager.devices[device_number].visible = false;
		});

	$('.tr_device_select').click( function(event) 
	{
        var device_number = parseInt(getHtmlAttr(event, 'device_number'), 10);

		_device_manager.devices[device_number].visible = !_device_manager.devices[device_number].visible;
		// Now we refresh the properties window with the new data

		//_ui.displayNodeProperties();

		if (_device_manager.devices[device_number].visible)
			expandDiv('div_device_properties_'+device_number, 300);
		else 
			collapseDiv('div_device_properties_'+device_number, 300);				

	});

	$('.image_device').click( function(event) 
		{ 
			console.log("MEH!");
	        var device_number = parseInt(getHtmlAttr(event, 'device_number'), 10);

	        // @todo 'output' num pode ser carago... mudar isto

			images.createImageSelector('images_selector_'+device_number, 'image_device_'+device_number, 'output'); 
		} );

};


// Return a string with hex encoded data representing this node
NodeRoot.prototype.exportNodeToArduino = function(node) 
{
	return Node.prototype.exportNodeToArduino.call(this) +
		padByte(NODE_SEQUENCE) + padByte(this.children.length)+';';
};


// Check if the node is valid
NodeRoot.prototype.validate = function()
{
	// The root needs to have exactlly 1 child
	if ( this.children.length !== 1)
	{
		this.error_state = NODE_STATE_ERROR;
		this.error_message = 'error_missing_single_child';
		return;
	}

	// If no errors were found, let's assume the node is fine
	this.error_state = NODE_STATE_NO_ERROR;
	this.error_message = '';		
};

