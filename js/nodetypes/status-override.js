
//
// Status Override Node
//

// These are the offsets for the bits of data representing the different policies when exporting
// to the Arduino. Only 2 bits are used for each policy, so the whole settings are saved in 1 byte

var NODE_OVERRIDE_POLICY_FAILURE_OFFSET = 0;
var NODE_OVERRIDE_POLICY_RUNNING_OFFSET = 2;
var NODE_OVERRIDE_POLICY_SUCCESS_OFFSET = 4;

function NodeStatusOverride(id, name, data) 
{
	Node.call(this, id, name);

	// Create a new data field with the elements from this node type
	this.data.failure_override = data.failure_override;
	this.data.running_override = data.running_override;
	this.data.success_override = data.success_override;

	// Categorize this node
	this.type = 'node_status_override';

	this.unique = false;
	this.min_children = 1;
	this.max_children = 1;


	// Initialize the node (depending on the categorization)
	Node.prototype.init.call(this);

	return this;
}

// Inherit from the Node class
NodeStatusOverride.inheritsFrom(Node);

// All the prototype definitions MUST be added after this point

// Default data to create new nodes of this type
NodeStatusOverride.prototype.DEFAULT_NODE_DATA  = { failure_override: 0, running_override: 1, success_override: 2 };


// Different completion policies for the parallel nodes
NodeStatusOverride.prototype.overrides = [{value:0, name:'status_fail'},
									    {value:1, name:'status_run'},
									    {value:2, name:'status_success'} ];


// Get a string with an HTML block to configure this node type
NodeStatusOverride.prototype.listProperties = function() 
{
	var output = '<table>' +

		// failure override		
		"<tr><td>"+t[lang]['override_fail']+"</td><td>"+
	   	createHtmlSelectorTranslated('select_failure_override', this.overrides, this.data.failure_override)+
	   	"</td></tr>"+

		// running override		
		"<tr><td>"+t[lang]['override_run']+"</td><td>"+
	   	createHtmlSelectorTranslated('select_running_override', this.overrides, this.data.running_override)+
	   	"</td></tr>"+

		// success override		
		"<tr><td>"+t[lang]['override_success']+"</td><td>"+
	   	createHtmlSelectorTranslated('select_success_override', this.overrides, this.data.success_override)+
	   	"</td></tr>"+

		"</td></tr></table>";
	
	return output;
};


// Save the properties of the current node based on data read from the configuration interface
NodeStatusOverride.prototype.saveProperties = function() 
{
	console.log("saveProperties NodeDelayTime");	
	this.data.failure_override = parseInt( $('#select_failure_override').val(), 10 );
	this.data.running_override = parseInt( $('#select_running_override').val(), 10 );
	this.data.success_override = parseInt( $('#select_success_override').val(), 10 );

	Node.prototype.saveProperties.call(this);
};


// Export the data for the node (so sent to the Arduino)
NodeStatusOverride.prototype.exportNodeToArduino = function() 
{
	//console.log('##### export_output:'+node);
	//console.log(node);

	//console.log('this.failure_override:'+this.failure_override);
	//console.log('this.running_override:'+this.running_override);
	//console.log('this.success_override:'+this.success_override);
	
	var policy = 
		(this.data.failure_override << NODE_OVERRIDE_POLICY_FAILURE_OFFSET) |
		(this.data.running_override << NODE_OVERRIDE_POLICY_RUNNING_OFFSET) |
		(this.data.success_override << NODE_OVERRIDE_POLICY_SUCCESS_OFFSET);

	console.log('policy:'+policy);
		
	return Node.prototype.exportNodeToArduino.call(this) +
		padByte(NODE_OVERRIDE) + padByte(policy)+';';
};


// Draw this node custom data over the default node data
NodeStatusOverride.prototype.draw = function(ctx) 
{
	if (this.display === false) return;
	Node.prototype.draw.call(this, ctx);

	var radius = this.radius * _ui.zoom;

	// Colors for the different override status
	var status_colors = ['#ee1111','#eeee11','#11ee11'];

	for (var i=0; i<=2; i++)
	{
		var mycolor = '#444444';
		if (i === this.data.failure_override) mycolor = status_colors[this.data.failure_override];
		drawCircle( ctx,
				this.display_x+radius*1.3+radius*(0.33*i),
				this.display_y-radius,
				radius*0.12,
				mycolor,
				"transparent" );
	}
	
	for (i=0; i<=2; i++) 
	{
		mycolor = '#444444';
		if (i === this.data.running_override) mycolor = status_colors[this.data.running_override];
		drawCircle( ctx,
				this.display_x+radius*1.3+radius*(0.33*i),
				this.display_y-radius*0.75,
				radius*0.12,
				mycolor,
				"transparent" );
	}
	
	for (i=0; i<=2; i++) 
	{
		mycolor = '#444444';
		if (i === this.data.success_override) mycolor = status_colors[this.data.success_override];
		drawCircle( ctx,
				this.display_x+radius*1.3+radius*(0.33*i),
				this.display_y-radius*0.5,
				radius*0.12,
				mycolor,
				"transparent" );
	}
};
