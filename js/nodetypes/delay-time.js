
//
// Delay Time Node
//

function NodeDelayTime(id, name, data) 
{
	Node.call(this, id, name);

	// Create a new data field with the elements from this node type
	this.data.delay_value = data.delay_value;

	// Categorize this node
	this.type = 'node_delay_time';

	this.unique = false;
	this.min_children = 0;
	this.max_children = 0;

	// Initialize the node (depending on the categorization)
	Node.prototype.init.call(this);

	return this;
}

// Inherit from the Node class
NodeDelayTime.inheritsFrom(Node);

// All the prototype definitions MUST be added after this point


// Default data to create new nodes of this type
NodeDelayTime.prototype.DEFAULT_NODE_DATA  = { delay_value:1000	};


// Returns a block of HTML with a table with the necessary elements to configute this node type
NodeDelayTime.prototype.listProperties = function() 
{
	return '<table>'+
			'<tr>'+
				'<td>'+t[lang]['Value']+'</td>'+
				'<td><input id="text_delay_time" type="number" value="'+this.data.delay_value+'" min="0" max="4294967295"></td>'+
			'</tr>'+
		'</table>';
};


// Add listeners to react the the changes in the node configuration
NodeDelayTime.prototype.addListPropertiesListeners = function() 
{
	// @todo Verificar se o valor está válido
};


// Save the properties of the current node based on data read from the configuration interface
NodeDelayTime.prototype.saveProperties = function() 
{
	console.log("saveProperties NodeDelayTime");
	this.data.delay_value = parseInt($('#text_delay_time').val());
	Node.prototype.saveProperties.call(this);
};


// Return a string with hex encoded data representing this node
NodeDelayTime.prototype.exportNodeToArduino = function() 
{
	return Node.prototype.exportNodeToArduino.call(this) +
		padByte(NODE_SERVICE) + padByte(SERVICE_DELAY)+
		padLong(this.data.delay_value)+';';
};


// Draw this node custom data over the default node data
NodeDelayTime.prototype.draw = function(ctx) 
{
	if (this.display === false) return;
	Node.prototype.draw.call(this, ctx);

	var offset = this.radius * _ui.zoom * 1.4;
	drawText ( ctx, this.data.delay_value, this.display_x, this.display_y+offset, "#eeeeee" );
};
