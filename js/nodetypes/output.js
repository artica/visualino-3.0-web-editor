
//
// Output data Node
//

function NodeOutput(id, name, data) 
{
	Node.call(this, id, name);

	// Create a new data field with the elements from this node type
	this.data.device_id = data.device_id;
	this.data.new_value = data.new_value;

	// Categorize this node
	this.type = 'node_output';

	this.unique = false;
	this.min_children = 0;
	this.max_children = 0;


	// Initialize the node (depending on the categorization)
	Node.prototype.init.call(this);

	// Create a pointer to the device this node references
	this.device = _device_manager.getDeviceById(this.data.device_id);

	return this;
}

// Inherit from the Node class
NodeOutput.inheritsFrom(Node);

// All the prototype definitions MUST be added after this point


// Returns a new node of the same type as this node, initialized with default settings
NodeOutput.prototype.createDefaultNode = function() 
{
	return new NodeOutput(_node_manager.nextNodeID(), '',
	{
		device_id:_device_manager.getDefaultDeviceID(DEVICE_CLASS_OUTPUT)
	});
};


// Get a string with an HTML block to configure this node type
NodeOutput.prototype.listProperties = function() 
{
	var output = '<table><tr><td>'+t[lang]['device']+'</td><td>';

	// Create a selector to choose which device to use
	output += _device_manager.getDeviceSelector(DEVICE_CLASS_OUTPUT, this.device);

	console.log("umpapaumpa");
	console.log(this.device);

	output += '</td></tr></table>';


	// Now we show the properties for the selected device type, assuming there is one
	if (this.device)
	{ 
		output += this.device.showServiceConfiguration(this.data);
	}
	return output;
};


// Save the properties of the current node based on data read from the configuration interface
NodeOutput.prototype.saveProperties = function() 
{
	console.log("saveProperties NodeOutput");	

	this.device = _device_manager.devices[ parseInt($('#select_device').val(), 10) ];
	// We need to initialize the default data for the new device
	this.data = this.device.defaultServiceData();
	// Now we refresh the properties window with the new data
	_ui.displayNodeProperties();

	Node.prototype.saveProperties.call(this);
};


// Export the data for the node (so sent to the Arduino)
NodeOutput.prototype.exportNodeToArduino = function() 
{
	console.log('##### export_output:'+this);
	console.log(this);
	return Node.prototype.exportNodeToArduino.call(this) +
		padByte(NODE_SERVICE) +	this.device.exportToArduinoService(this);
};


// Draw this node custom data over the default node data
NodeOutput.prototype.draw = function(ctx) 
{
	Node.prototype.draw.call(this, ctx);
	// @todo - here we will draw the new device state
};
