
//
// Parallel Node
//

function NodeParallel(id, name, data) 
{
	Node.call(this, id, name);

	// Create a new data field with the elements from this node type
	this.data.completion_policy = data.completion_policy;
	this.data.failure_policy = data.failure_policy;

	// Categorize this node
	this.type = 'node_parallel';

	this.unique = false;
	this.min_children = 2;
	this.max_children = 999;

	// Initialize the node (depending on the categorization)
	Node.prototype.init.call(this);

	return this;
}

// Inherit from the Node class
NodeParallel.inheritsFrom(Node);

// All the prototype definitions MUST be added after this point


// Default data to create new nodes of this type
NodeParallel.prototype.DEFAULT_NODE_DATA  = { completion_policy: 0, failure_policy: 1 };


// Different completion policies for the parallel nodes
NodeParallel.prototype.policies = [{value:0, name:'parallel_require_one'},
								   {value:1, name:'parallel_require_all'} ];



// Get a string with an HTML block to configure this node type
NodeParallel.prototype.listProperties = function() 
{
	var output = '<table>'+
	
		// Completion policy selector
		'<tr><td>'+t[lang]['Completion Policy']+'</td><td>'+
    	createHtmlSelectorTranslated('select_completion_policy', this.policies, this.data.completion_policy)+
		'</td></tr>'+

		// Failure policy selector
		'<tr><td>'+t[lang]['Failure Policy']+'</td><td>'+
    	createHtmlSelectorTranslated('select_failure_policy', this.policies, this.data.failure_policy)+
		'</td></tr></table>';

	return output;
};


// Save the properties of the current node based on data read from the configuration interface
NodeParallel.prototype.saveProperties = function() 
{
	console.log("saveProperties NodeParallel");	
	this.data.completion_policy = parseInt( $('#select_completion_policy').val(), 10 );
	this.data.failure_policy = parseInt( $('#select_failure_policy').val(), 10 );

	Node.prototype.saveProperties.call(this);
};


// Export the data for the node (so sent to the Arduino)
NodeParallel.prototype.exportNodeToArduino = function() 
{		
	var policy = 2*this.data.failure_policy + this.data.completion_policy;
	
	return Node.prototype.exportNodeToArduino.call(this) +
		padByte(NODE_PARALLEL) + padByte(this.children.length)+padByte(policy)+';';		
};


// Draw this node custom data over the default node data
NodeParallel.prototype.draw = function(ctx) 
{
	Node.prototype.draw.call(this, ctx);

	ctx.shadowOffsetX = 2;
	ctx.shadowOffsetY = 2;
	ctx.shadowBlur = 5;
	ctx.shadowColor = '#000000';
	
	var radius = this.radius * _ui.zoom;
	
	var thischar = '1';
	if (this.data.completion_policy === 1) thischar = '*';
	drawText ( ctx, thischar, this.display_x-radius, this.display_y-radius, "#11ff11" );
	
	thischar = '1';
	if (this.data.failure_policy === 1) thischar = '*';
	drawText ( ctx, thischar, this.display_x-radius, this.display_y+radius*0.5, "#ff1111" );
	
	ctx.shadowOffsetX = 0;
	ctx.shadowOffsetY = 0;
	ctx.shadowBlur = 0;
};


