
//
// Input data Node
//

function NodeInput(id, name, data) 
{
	Node.call(this, id, name);

	console.log('##### create NodeInput:');
	console.log(data);

	// Create a new data field with the elements from this node type
	this.data.device_id = data.device_id;

	// Categorize this node
	this.type = 'node_input';

	this.unique = false;
	this.min_children = 0;
	this.max_children = 0;


	// Initialize the node (depending on the categorization)
	Node.prototype.init.call(this);

	// Create a pointer to the device this node references
	this.device = _device_manager.getDeviceById(this.data.device_id);

	return this;
}

// Inherit from the Node class
NodeInput.inheritsFrom(Node);

// All the prototype definitions MUST be added after this point


// Returns a new node of the same type as this node, initialized with default settings
NodeInput.prototype.createDefaultNode = function() 
{
	return new NodeInput(_node_manager.nextNodeID(), '',
	{
		device_id:_device_manager.getDefaultDeviceID(DEVICE_CLASS_INPUT)
	});
};


// Get a string with an HTML block to configure this node type
NodeInput.prototype.listProperties = function() 
{
	var output = '<table><tr><td>'+t[lang]['device']+'</td><td>';

	// Create a selector to choose which device to use
	output += _device_manager.getDeviceSelector(DEVICE_CLASS_INPUT, this.device);

	output += '</td></tr></table>';

	console.log(this.device);

	// Now we show the properties for the selected device type, assuming there is one
	if (this.device)
	{ 
		output += this.device.showServiceConfiguration(this.data);
	}
	return output;
};


// Save the properties of the current node based on data read from the configuration interface
NodeInput.prototype.saveProperties = function() 
{
	console.log("saveProperties NodeInput");	

	this.device = _device_manager.devices[ parseInt($('#select_device').val(), 10) ];
	// We need to initialize the default data for the new device
	this.data = this.device.defaultServiceData();
	// Now we refresh the properties window with the new data
	_ui.displayNodeProperties();

	Node.prototype.saveProperties.call(this);

};


// Return a string with hex encoded data representing this node
NodeInput.prototype.exportNodeToArduino = function() 
{
	console.log('##### export_input:'+this);
	console.log(this);
	return Node.prototype.exportNodeToArduino.call(this) +
		padByte(NODE_SERVICE) +	this.device.exportToArduinoService(this);
};


// Draw this node custom data over the default node data
NodeInput.prototype.draw = function(ctx) 
{
	Node.prototype.draw.call(this, ctx);
	// @todo - here we will draw the new sensor state ?
};


