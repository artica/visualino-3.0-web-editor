// Size proportion between the node and the collapse button
var COLLAPSE_BUTTON_SIZE = 10;


function Node(name, type) 
{
	this.name = name ? name : '';
	this.type = type ? type : '';
	this.init();
	return this.uid;
}

Node.prototype = 
{
	init: function() 
	{
		this.uid = g.uid_function.get_node_uid();
		this.parent = null;
		this.children = [];
		this.highlighted = false;
		this.connections_selected = [];
		this.selected = false;
		this.locked = false;
		this.expanded = true;
		this.virtual_x = -1;
		this.virtual_y = -1;
		this.display_x = -1;
		this.display_y = -1;

		this.anchor_x = -1;
		this.anchor_y = -1;

		this.radius = 10;
		this.strokeColor = "rgb(0,0,0)";
		this.lineWidth = 3;
		this.animation = 1.0;
		this.animation_direction = 1;
		this.state = NODE_STATE_INACTIVE;		
		this.display = false;

		// If this node has reference to any device, set the pointers now
		//if (this.type.initPointers) this.type.initPointers();

		return this;
	},
	// Sets the node parent
	add_parent: function (v) 
	{
		this.parent = v;
		return v;
	},
	// Removes the node parent
	remove_parent: function (v) 
	{
		this.parent = null;
		return v;
	},
	// Adds a new child to this node, making sure it's not repeated
	add_child: function (v) 
	{
		//console.log('add_child');
		// Check if the child already exists on this list
		//console.log(v);
		//console.log(this.children);


		//if (v in this.children) return null;

		//console.log('ta-daaaaa!')

		this.children.push(v);
		
		// Adds this node as the parent of the parameter node
		v.add_parent(this);
		
		return this;
	},
	// Remove a child from this node
	remove_child: function (v) 
	{
		console.log('parent:'+this+'   remove_child:'+v);
		console.log(v);
		// If the desired child does not exist, nothing to do here...	
		var index = this.children.indexOf(v);
		console.log('index:'+this.children.indexOf(v));		
		if (index === -1) return null;

		// Removes this node as the parent of the parameter node
		v.remove_parent(this);
		
		this.children.splice(index, 1);
		console.log('REMOVED!');
		return v;
	},
	set_position: function (x, y)
	{
		//console.log('setting position to ' + x + ' ' + y);
		this.virtual_x = x;
		this.virtual_y = y;
		this.anchor_x = x;
		this.anchor_y = y;
		return this;
	},

	set_state: function(state)
	{
		/*
		console.log('set_state:' + this.id + '   '+state);
		console.log('state:' + state);
		console.log('NODE_STATE_INACTIVE:' + NODE_STATE_INACTIVE);
		console.log('NODE_STATE_RUNNING:' + NODE_STATE_RUNNING);


		console.log('state==NODE_STATE_INACTIVE :' + (state==NODE_STATE_INACTIVE) );
		console.log('state==NODE_STATE_RUNNING :' + (state==NODE_STATE_RUNNING) );

		if (state == NODE_STATE_INACTIVE) console.log('setting node:' + this.id + '   NODE_STATE_INACTIVE');
		if (state == NODE_STATE_RUNNING) console.log('setting node:' + this.id + '   NODE_STATE_RUNNING');
		*/

		this.state = state;
		
		// When a node is set as inactive or starts to run, all children nodes become inactives
		
		if ( state == NODE_STATE_INACTIVE && this.children.length>0 )
		{
			for (var i=0; i<this.children.length; i++)
			{
				this.children[i].set_state(NODE_STATE_INACTIVE);
			}
		}
		
	},
	// Checks all the ancestors of a specific node up to the root
	// of the tree, looking for a specific node
	check_ancestors: function (n)
	{
		console.log('check_ancestors:' + n);
		// Reached the top of the tree, the node wasn't found
		if (this.parent === null) return false;
		else
		{
			// The node was found, so we return true
			if (this.parent === n) return true;
			
			// At this point the node wasn't found, so we go up on the tree
			return this.parent.check_ancestors(n);
		}
	},

	move_node: function (x, y)
	{
		//console.log('moving node from ' + this.x + ' ' + this.y + ' to ' + (this.x -x) + ' ' + (this.y -y) );
		this.virtual_x -= x/g.zoom;
		this.virtual_y -= y/g.zoom;
		return this;
	},

	set_anchor: function ()
	{
		//console.log('moving node from ' + this.x + ' ' + this.y + ' to ' + (this.x -x) + ' ' + (this.y -y) );
		this.anchor_x = this.virtual_x;
		this.anchor_y = this.virtual_y;
		return this;
	},

	restore_anchor: function ()
	{
		//console.log('moving node from ' + this.x + ' ' + this.y + ' to ' + (this.x -x) + ' ' + (this.y -y) );
		this.virtual_x = this.anchor_x;
		this.virtual_y = this.anchor_y;
		return this;
	},

	set_radius: function (radius) 
	{
		this.radius = radius;
		return this;
	},
	
	reset_animation: function (value) 
	{
		this.animation = (value === 0? 1.0: 0.0);
		this.animation_direction = value;
		this.display = (value === 0);
		
		//console.log('reset_animation   direction:'+this.animation_direction+
		//	'  animation:'+this.animation+'  display:'+this.display);
		return this;
	},
	
	// Marks the node as collapsed and collapse it
	collapse: function () 
	{
		this.expanded = false;
		this.collapse_children();
	},
	
	// Collapse this node and all its children
	collapse_node: function () 
	{
		this.animation_direction = 0;
		this.collapse_children();
		return this;
	},
	
	// Collapse all the children of this node
	collapse_children: function () 
	{
		// Let's check each node for its children
		for (var j=0; j<this.children.length; j++)
		{
			var child = this.children[j];
			if (!child) return;
			child.collapse_node();
		}
	},
	
	// Marks the node as expanded and expands it
	expand: function () 
	{
		this.expanded = true;
		this.expand_children();
	},
	
	// Expand this node and all its children
	expand_node: function () 
	{
		this.animation_direction = 1;
		// Only expands the children if the node is expanded
		if (this.expanded) this.expand_children();
		return this;
	},
	
	// Expand all the children of this node
	expand_children: function () 
	{
		// Let's check each node for its children
		for (var j=0; j<this.children.length; j++)
		{
			var child = this.children[j];
			if (!child) return;
			child.expand_node();
		}	
	},
	
	update_animation: function () 
	{
		// Animation to show the nodes
		if (this.animation_direction === 1)
		{
			// We don't need to update if the parent node is still animating
			if ( (this.parent != null && this.parent.animation<1.0 ) ) return true; 
			
			// Advance the animation to the next frame
			// The node starts moving faster and gets slower as it gets
			// closer to the final position
			var step = Math.max(0.05, (1.0-this.animation)*0.2);
			this.animation += step;
			if (this.animation > 1.0) this.animation = 1.0;
			this.display = true;
		}
		else 		
		// Animation to hide the nodes
		{		
			// We don't need to update if this is a root node, or if 
			// any of the children nodes are still animating
			if (this.parent === null) return true; 
			for (var j=0; j<this.children.length; j++)
			{
				var child = this.children[j];
				if (!child || child.animation>0.1) return true;
			}
			// Advance the animation to the next frame
			// The node starts moving slow and gets faster as it gets
			// closer to the parent node
			var step = Math.max(0.05, (1.0-this.animation)*0.2);
			this.animation -= step;
			if (this.animation <= 0.0)
			{
				this.animation = 0.0;
				this.display = false;
			}
			//console.log('step:'+step);
			//console.log('anim:'+this.animation);
		}
		
		//console.log('g.corner_x:'+g.corner_x+'   g.zoom:'+g.zoom+'  this.virtual_x:'+this.virtual_x);
		// Change the display position, depending on if the node has a parent or not
		this.display_x = g.corner_x + this.virtual_x * g.zoom;
		this.display_y = g.corner_y + this.virtual_y * g.zoom;
		if (this.parent != null)
		{
			this.display_x = this.parent.display_x + ( this.display_x - this.parent.display_x ) * this.animation;
			this.display_y = this.parent.display_y + ( this.display_y - this.parent.display_y ) * this.animation;
		}
		
		//console.log('this.display_x:'+this.display_x + '   this.display_y:'+this.display_y);
		return ( (this.animation > 0.0 && this.animation_direction === 0 ) ||
				  this.animation < 1.0 && this.animation_direction === 1 );
	},	
	
	draw: function (ctx) 
	{
		// Check is this node is visible or not
		if (this.display === false) return; 

		ctx.lineWidth = this.lineWidth;

		// Default colors
		var stroke_style = "transparent";
		var fill_style = "transparent";

		// Change the aspect depending on if the node is locked
		if (this.locked) stroke_style = '#3366ff';

		switch (this.state)
		{
			case NODE_STATE_FAILURE: fill_style = '#ff0000'; break;
			case NODE_STATE_RUNNING: fill_style = '#0000ff'; break;
			case NODE_STATE_SUCCESS: fill_style = '#00ff00'; break;
			case NODE_STATE_INACTIVE: fill_style = '#999999'; break;
		}

		var radius = this.radius * g.zoom;

		// Add a second external circle is the node is selected
		if (this.highlighted || this.selected)
		{
			drawCircle( ctx, this.display_x, this.display_y, radius*1.3, '#000000', stroke_style );
		}
		
		// Draw the external circle that shows the execution state of the node
//		ctx.lineWidth = 20;
		drawCircle( ctx, this.display_x, this.display_y, radius*1.15, fill_style, stroke_style );

		// Make sure the image exists... If the node has no defined image, use a default value
		var image = this.type.getImage() ? this.type.getImage(): images.getImage('number');

	    ctx.drawImage(image, this.display_x-radius , this.display_y-radius, radius*2, radius*2);

		//this.name = "hello!!!";


		// Position and alignment for the text labels near the node
		// This position will depend if the node has children or not
		var label_xpos;
		var label_ypos;
		var label_align;

		// If the node is a leaf, we place any existing labels below the node
		if (this.children.length === 0)
		{
			label_xpos = this.display_x;
			label_ypos = this.display_y + radius + 20;
			label_align = TEXTBOX_ALIGN_CENTER;
		}
		else
		{
			// If the node is not a leaf, we place any existing labels left of the node
			label_xpos = this.display_x - radius - 10;
			label_ypos = this.display_y;
			label_align = TEXTBOX_ALIGN_LEFT;

			// In this case we also need to draw the expand/collaspe images below the node
			if (this.expanded === false)
			{
			    ctx.drawImage(images.getImage('plus'), this.display_x-(radius/2) , this.display_y+radius, radius, radius);
			}
			else
			{
			    ctx.drawImage(images.getImage('minus'), this.display_x-(radius/2) , this.display_y+radius, radius, radius);

			}
		}

		if (this.name !== '')
		{
			drawTextBox ( ctx, this.name, label_align, label_xpos, label_ypos, "#DDDDDD", '#555555' );
			label_ypos += 25;
		}

		if (this.type.value !== undefined && this.type.value !== '')
		{
			drawTextBox ( ctx, this.type.value, label_align, label_xpos, label_ypos, "#DDDDDD", '#555555' );
		}


	    ctx.fillStyle = 'transparent';


		
		/*
		// Draws the elements of this specific node type
		if (typeof(this.type.draw) === "function")
		{
   			this.type.draw(ctx, this.display_x, this.display_y, this.radius);
		}
		*/

		return this;
	},
	draw_connections: function (ctx) 
	{
		// Check if this node is visible or not
		if (!this.display) return; 

		var radius = this.radius * g.zoom;
		
		// Let's check each node for its children
		for (var j=0; j<this.children.length; j++)
		{
			var child = this.children[j];
			if (!child) return;

			// Check if the child node is visible or not
			if (!child.display) continue; 
			var color = '#AAAAAA';
			
			// Check if the connection is on the selected list
			if (this.connections_selected.indexOf(child) > -1)
			//if (child in this.connections_selected)
			{
				color = '#FF5533';
				//console.log('SELECTED!!!')
			}
			ctx.lineWidth = this.lineWidth * g.zoom;
			ctx.strokeStyle = color;


			var y_distance = (child.display_y - this.display_y - 2*radius) * 0.7;

			//var y_distance = 75;

			ctx.beginPath();
			ctx.moveTo(this.display_x, this.display_y + 2 * radius);
			ctx.bezierCurveTo( this.display_x, this.display_y+y_distance, child.display_x, 
							child.display_y-y_distance, child.display_x, child.display_y);
			ctx.stroke();

			/**/

/*			
			ctx.beginPath();
			ctx.moveTo(this.display_x, this.display_y + 2 * radius);
			ctx.lineTo(child.display_x, child.display_y);
			ctx.stroke();
/**/

		}
	},

	highlight: function () 
	{
		this.highlighted = true;
		return this;
	},
	unhighlight: function () 
	{
		this.highlighted = false;
		return this;
	},
	connection_select: function (c) 
	{
		console.log('connection_select:'+c)
		// Check if the child already exists on this list
		if (this.connections_selected.indexOf(c) > -1) return null;
		
		this.connections_selected.push(c);
		return this;
	},
	connection_toggle: function (c) 
	{
		console.log('connection_toggle:'+c)
		// Check if the child already exists on this list
		pos = this.connections_selected.indexOf(c);
		// If the child existed, remove it from the list
		if ( pos > -1) this.connections_selected.splice(pos, 1);
		// If it didn't exist, let's add it
		else this.connections_selected.push(c);
		return this;
	},
	connections_unselect: function () 
	{
		this.connections_selected = [];
		return this;
	},
	// Removes all selected connections
	connection_remove: function () 
	{
		if (this.connections_selected.length===0) return this;
		
		console.log('connection_remove:'+this.connections_selected.length)
		for (var i=0; i<this.connections_selected.length; i++)
		{
			this.remove_child(this.connections_selected[i]);
		}
		this.connections_selected = [];
		return this;
	},
	
	toggle_highlight: function () 
	{
		if (this.highlighted) {
			return this.unhighlight();
		} else {
			return this.highlight();
		}
	},
	select: function (state) 
	{
		this.selected = state;
		return this;
	},
	select_tree: function (state) 
	{
		this.selected = state;
		for (var i=0; i<this.children.length; i++)
		{
			this.children[i].select_tree(state);
		}
		return this;
	},
	unselect: function () 
	{
		this.selected = false;
		return this;
	},
	toggle_selection: function () 
	{
		if (this.selected) 
		{
			return this.unselect();
		} 
		else 
		{
			return this.select();
		}
	},
	toString: function() 
	{
		return this.uid;
	},
	
	
	//
	// export / import
	//
	
	export_node: function() 
	{
		var output = 
		{
			'name': this.name,
			'type': this.type.export_nodetype(),
			'uid': this.uid,
			'x': this.virtual_x,
			'y': this.virtual_y,
			'radius': this.radius
		};
		return output;
	},
	
	export_connection: function(c) 
	{
		//c1 = c1.toString();
		//c2 = c2.toString();

		//console.log('export_connection:'+c+'   '+this.uid+'->'+this.children[c].uid)
		console.log('export_connection:'+c+'   '+this.uid+'->');
		console.log(this);
		console.log(this.children);
		console.log(this.children[c]);
		
		return this.uid + ':' +this.children[c].uid ;
	},	
	
	// Sort the children of this node recursively, creating 
	// new sequential ID's for the arduino 
	sort_children: function (new_id) 
	{
		// Set the new arduino node id (sequencial order)
		this.id = new_id;
		
		//console.log('this is:'+new_id);
		// Sort this node's children by the X coordinate
		this.children.sort(function(node1, node2) 
			{
				return node1.virtual_x - node2.virtual_x;
			});
			
		var current_id = new_id+1;
		// Sort all the children nodes recursively
		for (var j=0; j<this.children.length; j++)
		{	
			// The of each children is the highest id of the previous
			// children branch plus 1
			current_id = this.children[j].sort_children(current_id) + 1;
		}
		return current_id-1;
	},
	
	export_arduino: function() 
	{
		return this.type.export_arduino( this );
	},
	
	// Create a string with all the children connections
	export_connections: function () 
	{
		var connections = '';
		for (var j=0; j<this.children.length; j++)
		{	
			connections += 'C'+padByte(this.id)+
				padByte(this.children[j].id) + ';';
		}
		return connections;
	},
	
	
}
