
//
// create unique identifiers
//

function UID() 
{
    this.next_device_uid = 0;
    this.next_node_uid = 0;
}

UID.prototype = 
{
    get_device_uid: function() 
    {
        var new_uid = this.next_device_uid++;
        return new_uid.toString(16);
    },
    set_device_next_uid: function(string16) 
    {
        this.next_device_uid = parseInt(string16,16);
        console.log('last device UID is:'+ this.next_node_uid);
    },
    get_node_uid: function() 
    {
        var new_uid = this.next_node_uid++;
        return new_uid.toString(16);
    },
    set_node_next_uid: function(string16) 
    {
        this.next_node_uid = parseInt(string16,16);
        console.log('last node UID is:'+ this.next_node_uid);
    }
}


function expandDiv(div, time)
{
    var height = $('#'+div).height();
    //console.log('height:'+);

    $('#'+div).stop()
              .css({height:0, display:'block'})
              .animate( {height:height}, time, function() { $('#'+div).css({height:'auto'})} );
}

function collapseDiv(div, time)
{
    var height = $('#'+div).height();
    //console.log('height:'+);

    $('#'+div).stop()
              .animate( {height:0}, time, function() { $('#'+div).css({height:height, display:'none'})} );
}



function createHtmlSelector(id, options, current)
{
    var html = "<select id='"+id+"'>";

    for (i = 0; i < options.length; i++) 
    {
        var selected = (options[i].value == current) ? " selected='true'": "";
        html += "<option value='"+options[i].value+"'"+selected+">" + options[i].name + "</option>";
    }
    html += "</select>";

    //console.log(html);
    return html;
}


function createHtmlSelectorHighLow(id, current)
{
    return createHtmlSelector(id, [{value:0, name:t[lang]['Low']}, {value:1, name:t[lang]['High']}], current);
}


function createHtmlSelectorPins(id, pin_list, current)
{
    //console.log('createSelectorPins');
    //console.log(pin_list);
    var list = [];
    for (var p=0; p<pin_list.length; p++)
    {
        list.push({value:pin_list[p], name:pin_list[p]})
    }    
    //console.log(list);

    return createHtmlSelector(id, list, current);
}

/*
function createHtmlSelectorDevices(id, type, current)
{
    var list = [];
    for (var i=0; i < devices.length; i++) 
    {
        // Only show devices that are classified as the desired type
        if (devices[i].type.class === type)
        {
            // If no device was selected for the node, we select the first compatible device
            if (this.device === null) 
            {
                this.device = devices[i];
                // We also need to initialize the data of this node with the default values for the device
                this.data = devices[i].type.init_service_data();
            }

            list.push({value:pin_list[p], name:pin_list[p]});

            // Mark the device in use by the node as selected
            var selected = '';
            if (this.device === devices[i]) selected = 'selected="true"';
            output += '<option id="select_device" value="'+i+'" '+selected+'>' + devices[i].name + '</option>';             
        }
    }



    //console.log('createSelectorPins');
    //console.log(pin_list);
    var list = [];
    for (var p=0; p<pin_list.length; p++)
    {
        list.push({value:pin_list[p], name:pin_list[p]});
    }    
    //console.log(list);

    return createHtmlSelector(id, list, current);
}
*/

// Fills a linear meter (canvas) up to a certain value
// The value must be a normalized number [0, 1]
function fillLinearMeter( id, value, color)
{
    //console.log("fillLinearMeter:"+value);
    // Get the canvas and the matching context
    var canvas = $('#'+id)[0];
    var context = canvas.getContext("2d");

    if (value > 1.0) value = 1.0;
    // Make the dimension of the canvas area equal to the css dimension
    var width = $('#'+id).width();
    var height = $('#'+id).height();
    $('#'+id).attr('width', width);
    $('#'+id).attr('height', height);

    // Clear the canvas area
    context.clearRect(0, 0, width, height);

    // If the value is positive, adjust it to the canvas dimensions
    if (value>0)
    {
        context.fillStyle = ( color? color: "#00A3D9" );
        context.fillRect(3, 3, (width-6)*value, height-6);
        context.stroke();
    }
}


// calculate the distance between two points
function distance_to_point(x1, y1, x2, y2) 
{
	var dx = x2 - x1;
	var dy = y2 - y1;
	return Math.sqrt(dx * dx + dy * dy);
}


// Square of a number
function sqr(x) { return x * x }
// Square of the distance between two points
function dist2(v, w) { return sqr(v.display_x - w.display_x) + sqr(v.display_y - w.display_y) }
// Distance between a point p and a line segment, defined by two points v and w
function distToSegment(p, v, w) 
{

    var l2 = dist2(v, w);
    if (l2 == 0) return Math.sqrt(dist2(p, v));
    var t = ((p.display_x - v.display_x) * (w.display_x - v.display_x) + (p.display_y - v.display_y) * (w.display_y - v.display_y)) / l2;
    if (t < 0) return Math.sqrt(dist2(p, v));
    if (t > 1) return Math.sqrt(dist2(p, w));
    return Math.sqrt(dist2(p, { display_x: v.display_x + t * (w.display_x - v.display_x),
                    display_y: v.display_y + t * (w.display_y - v.display_y) }));
}


//
// canvas drawing functions
//

function drawCircle(ctx, thisx, thisy, radius, fillStyle, strokeStyle) {
	ctx.fillStyle = fillStyle;
	ctx.strokeStyle = strokeStyle;
	ctx.beginPath();
	ctx.arc( thisx, thisy, radius, 0, Math.PI*2, true );
	ctx.fill();
	ctx.stroke();
	ctx.closePath();
}

function drawText( ctx, text, thisx, thisy, fillStyle ) {
    ctx.fillStyle=fillStyle;
    ctx.font="14px futura";
    ctx.textBaseline="top";
    ctx.fillText(text, thisx, thisy);
}

var TEXTBOX_ALIGN_LEFT = 1;
var TEXTBOX_ALIGN_CENTER = 2;

function drawTextBox( ctx, text, align, thisx, thisy, text_style, box_style ) 
{
    var width = ctx.measureText( text ).width + 10;

    var xpos;
    switch (align)
    {
        case TEXTBOX_ALIGN_LEFT: xpos = thisx - width; break;
        case TEXTBOX_ALIGN_CENTER: xpos = thisx - width/2; break;
    }

    ctx.textAlign="left";
    ctx.fillStyle = box_style;
    roundRect(ctx, xpos, thisy - 10, width, 20, 5);

    ctx.fillStyle=text_style;
    ctx.font="14px futura";
    ctx.textBaseline="middle";
    ctx.fillText(text, xpos+5, thisy);
}

function roundRect(ctx, x, y, width, height, radius, fill) 
{
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();
    ctx.fill();
}

function padByte(value)
{
    var x = parseInt(value);
    var str = x.toString(16);
    var pad = "00";
    return pad.substring(0, pad.length - str.length) + str;
}

function padInt(value)
{
    var x = parseInt(value);
    var str = x.toString(16);
    var pad = "0000";
    return pad.substring(0, pad.length - str.length) + str;
}

function padLong(value)
{
	var x = parseInt(value);
	var str = x.toString(16);
	//console.log(str);
	var pad = "00000000";
	return pad.substring(0, pad.length - str.length) + str;
}


function getHtmlAttr(event, name)
{
    var elem = event.target;
    // Get the device number from the HTML <tr>

    while (true)
    {
        var result = elem.getAttribute(name) ;
        if (result !== null) return result;
        elem = elem.parentNode;
    }
}


//
// clone object
//

var object_create = Object.create;
if (typeof object_create !== 'function') {
    object_create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}
function deepCopy(src, /* INTERNAL */ _visited) {
    if(src == null || typeof(src) !== 'object'){
        return src;
    }

    // Initialize the visited objects array if needed
    // This is used to detect cyclic references
    if (_visited == undefined){
        _visited = [];
    }
    // Otherwise, ensure src has not already been visited
    else {
        var i, len = _visited.length;
        for (i = 0; i < len; i++) {
            // If src was already visited, don't try to copy it, just return the reference
            if (src === _visited[i]) {
                return src;
            }
        }
    }

    // Add this object to the visited array
    _visited.push(src);

    //Honor native/custom clone methods
    if(typeof src.clone == 'function'){
        return src.clone(true);
    }

    //Special cases:
    //Date
    if (src instanceof Date){
        return new Date(src.getTime());
    }
    //RegExp
    if(src instanceof RegExp){
        return new RegExp(src);
    }
    //DOM Elements
    if(src.nodeType && typeof src.cloneNode == 'function'){
        return src.cloneNode(true);
    }

    //If we've reached here, we have a regular object, array, or function

    //make sure the returned object has the same prototype as the original
    var proto = (Object.getPrototypeOf ? Object.getPrototypeOf(src): src.__proto__);
    if (!proto) {
        proto = src.constructor.prototype; //this line would probably only be reached by very old browsers 
    }
    var ret = object_create(proto);

    for(var key in src){
        //Note: this does NOT preserve ES5 property attributes like 'writable', 'enumerable', etc.
        //For an example of how this could be modified to do so, see the singleMixin() function
        ret[key] = deepCopy(src[key], _visited);
    }
    return ret;
}
