
// Threshold to assume clicks on the connections between nodes
// The value represents how far away we can click from the line
// to still consider the click close enough to select it
var CONNECTION_CLICK_THRESHOLD = 12;


var GRAPH_MIN_ZOOM = 0.3;
var GRAPH_MAX_ZOOM = 10;


function Graph(node_class) 
{
	this.node_class = node_class;
	
	this.reset_graph();
	
	this.n = new NodeTypes();
	this.d = new DeviceTypes();

	this.uid_function = new UID();
	
	this.tempx = 0;
	this.tempy = 0;
	this.tempconnect = false;
	
	this.pan_canvas = false;
	this.pan_nodes = false;

	this.pan_pasted = false;
	this.box_select = false;


	this.dragStartX = 0;
	this.dragStarty = 0;
	
	this.boxStartX = 0;
	this.boxStarty = 0;
	
	this.delete_selection = false;

	this.corner_x = 0;
	this.corner_y = 0;

	this.zoom = 1.0;

	// Number of the node that we are displaying the properties from
	this.visible_node_properties = -1;

	// Clipboard list of node id's (used for copy-paste stuff)
	this.clipboard = [];

	// List of different versions of the tree, so that we can revert to previous versions
	this.version_list = [];
	this.version_pos = -1;

	this.block_version = false;

	this.state_updated = false;

	// @todo fix this variable (not working well since the canvas was expanded)
	this.inputBoxTyping = false;
	
	this.selectedNode = -1; // used for refreshing properties div

	// Last node that was selected
	this.lastSelectedNode = -1;
	
	// Flag used when any element is moved. Setting this flag to true
	// makes the worker thread check if any repositioning is necessary
	this.tree_moved = false;
	
	// Does the tree need to be updated because of collisions?
	this.update_collision = true;
	// Does the tree need to be updated because of animations?
	this.update_animation = true;
	
	
	this.importing = false;
}

Graph.prototype = 
{

	//
	// node handling
	//
	
	reset_graph: function() 
	{
		this.root = null;
		this.devices = [];
		this.nodes = [];
		this.num_nodes = 0;
		
		if (this.collision_worker) 
		{
			this.collision_worker.terminate();
		}
		this.collision_worker = new Worker('js/graph-upd_node_pos-worker.js');
		this.collision_worker.onmessage = this.update_node_positions;
		
		this.update_collision = true;

	},
	add_node: function (name, type, x, y, radius)
	{
		var v = new this.node_class(name, type);

		if ((x !== undefined) && (y !== undefined))
		{
			v.set_position(x,y);
		}
		if (radius !== undefined) {
			v.set_radius(radius);
		}
		
		this.nodes[this.nodes.length] = v;
		this.num_nodes++;
		return v;
	},
	remove_node: function (v) 
	{
		// Make sure it actually exists
		if (!(v in this.nodes)) return null;

		// If the node has a parent, remove the link from the parent to this node
		if (this.nodes[v].parent)
			this.nodes[v].parent.remove_child(this.nodes[v]);
		
		// If this node has children, remove the parent on all of then
		for (var i=0; i<this.nodes[v].children.length; i++)
		{
			this.nodes[v].children[i].parent = null;
		}

		// All the links have been taken care of, so we can remove the node safely
		this.nodes.splice(v, 1);		
		this.num_nodes--;
		
		// refresh properties display
		this.clear_display();

		return v;
	},
	remove_selected_nodes: function(deleteThis) 
	{
		if (!this.inputBoxTyping || deleteThis) 
		{
			for (var i = this.nodes.length-1; i >= 0; i--) 
			{
				if(this.nodes[i].selected) 
				{
					this.remove_node(i);
				}
			}
		}
		this.saveState();
	},

	remove_selected_connections: function(deleteThis) 
	{
		if (!this.inputBoxTyping || deleteThis) 
		{
			for (var i = 0; i < this.nodes.length; i++) 
			{
				this.nodes[i].connection_remove();
			}
		}
	},
	
	remove_selection: function(deleteThis) 
	{
		this.remove_selected_connections(deleteThis);
		this.remove_selected_nodes(deleteThis);
		startAnimation();

	},

	// Cut the current nodes to a temporary buffer
	cutSelected: function()
	{
		console.log('cutSelected');
		this.copySelected();
		this.remove_selected_nodes();
	},

	// Creates and returns a copy of the selected nodes (or all the nodes)
	copyNodes: function(all)
	{
		var result = [];
		for  (var i=0; i<this.nodes.length; i++)
		{
			// Let's see if we want to copy all the nodes, or just the selected ones
			if ( all || this.nodes[i].selected )
			{
				//var new_node = deepCopy(this.nodes[i]);

				var new_node = $.extend( {}, this.nodes[i]);

				//console.log("---------------------------");
				//console.log(this.nodes[i].type);
				//console.log(new_node.type);

				// Copy the current UID of the node
				new_node.uid = this.nodes[i].uid;

				// The normal nodes have pointer references, not UID references
				// So now we need to convert these pointers into static temporary UIDs, 
				// just in case the original nodes change/dissapear				
				new_node.parent =  (this.nodes[i].parent !== null? this.nodes[i].parent.uid: null);
				new_node.children = [];

				for (var j=0; j<this.nodes[i].children.length; j++)
				{
					new_node.children.push(this.nodes[i].children[j].uid);
				}
				
				// Save the current position of the copied node
				new_node.virtual_x = new_node.virtual_x;
				new_node.display_x = new_node.display_x;
				// Save the new temporary node on the result array
				result.push(new_node);
			}
		}
		return result;
	},

	// Adds all the temporary nodes stored in an array to the tree node list
	pasteNodes: function(list, maintain_ids)
	{
		console.log("pasteNodes");
		console.log("###################################################");
		// Last node of the current list of nodes (before we insert the pasted nodes)
		var first_pasted_node = this.nodes.length;

		// Create new nodes from the copied nodes and initialize them
		for (var i=0; i<list.length; i++)
		{
			var node = list[i];
			//console.log(node);

			//console.log("---------------------------");
			//console.log(node.type);

			//console.log(new_node.type);


			// Add a new node based on a list node
			g.add_node('', deepCopy(node.type), node.virtual_x, node.virtual_y, 20);


			//var new_node = $.extend( {}, this.nodes[i]);

			//add_node: function (name, type, x, y, radius)


			// Initialize the variables that are not initialized automatically
			var new_node = g.nodes[g.nodes.length-1];
			if (maintain_ids) new_node.uid = node.uid;
			new_node.display_x = node.display_x;
			new_node.display_y = node.display_y;

			new_node.virtual_x = node.virtual_x;
			new_node.virtual_y = node.virtual_y;
			new_node.virtual_x = node.virtual_x;
			new_node.virtual_y = node.virtual_y;
		}

		// Now we will copy all the connections between the list nodes to the new nodes
		for (var i=0; i<list.length; i++)
		{
			var clip_node = list[i];
			//console.log("processing connections:"+i+'  children:'+clip_node.children.length);

			// Go through all the children of the node
			for (var j=0; j<clip_node.children.length; j++)
			{
				// Let's check if this specific child was also on the list(a node can 
				// have children that were not copied, so we can't re-create those connections)
				var pos = this.find_clipboard_uid(list, clip_node.children[j])
				//console.log('pos:'+pos);
				// The child node does exist, so we can copy the connection to the new nodes
				if (pos !== -1)
				{
					// Since the new nodes are in the same order as the pasted nodes, we can
					// add the connection based on their relative positions on the arrays
					this.nodes[first_pasted_node+i].add_child(this.nodes[first_pasted_node+pos]); 
				} 
			}
		}		
	},

	// Copy the currently selected nodes to a temporary buffer (clipboard)
	copySelected: function()
	{
		console.log("copySelected");
		//var d = new Date();
		//var start = d.getTime();
		this.clipboard = this.copyNodes(false);
		//d = new Date(); var end = d.getTime(); console.log("time:"+(end-start));
	},

	// Returns the position of a node with a specific UID on the clipboard nodes
	find_clipboard_uid: function(list, uid)
	{
		//console.log('find_clipboard_uid:'+uid);
		for (var i=0; i<list.length; i++)
		{
			if (list[i].uid === uid) return i;
		}
		return -1;
	},

	// Paste the nodes from temporary buffer to the current screen
	pasteSelected: function()
	{
		console.log("pasteSelected");

		// We start by un-selecting every currently selected node
		for  (var i=0; i<this.nodes.length; i++)
		{
			this.nodes[i].selected = false;
		}

		var first_pasted_node = this.nodes.length;

		// Adds the nodes currently on the clipboard to the tree
		this.pasteNodes(this.clipboard);

		// Now we select all the new nodes that were just added and calculate the 
		// central position between all the nodes
		// Let's start the bounding box with the coordinates of the first point
		var min_x = this.nodes[first_pasted_node].display_x; 
		var max_x = min_x;
		var min_y = this.nodes[first_pasted_node].display_y; 
		var max_y = min_y;

		console.log('------------------------------------------');
		console.log('min_x:'+min_x+'  max_x:'+max_x+'  min_y:'+min_y+'  max_y:'+max_y);

		for  (var i=first_pasted_node; i<this.nodes.length; i++)
		{
			// Mark the node as selected
			this.nodes[i].selected = true;
			console.log('this.nodes[i].display_x:'+this.nodes[i].display_x+'  this.nodes[i].display_y:'+this.nodes[i].display_y);
			console.log('this.nodes[i].virtual_x:'+this.nodes[i].virtual_x+'  this.nodes[i].virtual_y:'+this.nodes[i].virtual_y);
			console.log(this.nodes[i]);
			// Inscrese the bounding box for all the nodes if necessary
			if (this.nodes[i].display_x < min_x) min_x = this.nodes[i].display_x;
			if (this.nodes[i].display_x > max_x) max_x = this.nodes[i].display_x;
			if (this.nodes[i].display_y < min_y) min_y = this.nodes[i].display_y;
			if (this.nodes[i].display_y > max_y) max_y = this.nodes[i].display_y;
		}

		console.log('min_x:'+min_x+'  max_x:'+max_x+'  min_y:'+min_y+'  max_y:'+max_y);

		// Middle point of the pasted content
		var paste_center_x = min_x + (max_x - min_x) / 2;
		var paste_center_y = min_y + (max_y - min_y) / 2;

		// Now we pan the new nodes so that their center will appear
		// centered with the mouse last known position
		g.pan_selected_nodes(paste_center_x - last_mouse_x  , paste_center_y - last_mouse_y );

		// Start panning the new nodes
		this.pan_pasted = true;

		startAnimation();
	},

	// Undo the last action executed
	undo: function()
	{
		// Can't do anything if the version is blocked (another undo/redo already executing)
		if (!this.block_version)
		{
			// Block the version untill we finish the undo (so re can't undo/redo untill this ends)
			this.block_version = true;
			//console.log("ERROR! UNDO! UNDO! UNDO!");

			//console.log('undo 1: this.version_list.length:'+this.version_list.length+'   this.version_pos:'+this.version_pos );

			if (this.version_pos>0)  
			{
				this.version_pos--;
				this.reset_graph();
				// Adds the nodes from the desired version to the tree
				this.pasteNodes(this.version_list[this.version_pos], true);
			}
			startAnimation();

			// Unlock the version so do can do more undo/redos
			this.block_version = false;
		}
		//console.log('undo 2: this.version_list.length:'+this.version_list.length+'   this.version_pos:'+this.version_pos );
	},

	// Redo the last action that was undone
	redo: function()
	{
		// Can't do anything if the version is blocked (another undo/redo already executing)
		if (!this.block_version)
		{
			if (this.block_version) console.log("-------------------fail!!!!------------------");

			// Block the version untill we finish the undo (so re can't undo/redo untill this ends)
			this.block_version = true;
			//console.log("OOOOPS REDO! REDO! REDO!");

			if (this.version_pos<this.version_list.length-1)  
			{
				this.version_pos++;
				this.reset_graph();
				// Adds the nodes from the desired version to the tree
				this.pasteNodes(this.version_list[this.version_pos], true);

			}
			startAnimation();
			// Unlock the version so do can do more undo/redos
			this.block_version = false;
		}

		//console.log('redo 2: this.version_list.length:'+this.version_list.length+'   this.version_pos:'+this.version_pos );
	},

	// Saves the current state of the tree in the version list
	saveState: function()
	{
		//console.log('saveState1: this.version_list.length:'+this.version_list.length+'   this.version_pos:'+this.version_pos );
		// Remove all the configurations after the current state
		this.version_list.splice(this.version_pos+1, this.version_list.length-(this.version_pos+1));
		// Add the current state to the end of the version list
		this.version_list.push(this.copyNodes(true));
		// The current version of the tree is now the last one on the version list
		this.version_pos = this.version_list.length-1;
		//console.log('saveState2: this.version_list.length:'+this.version_list.length+'   this.version_pos:'+this.version_pos );

	},

	// Sets a new state for a specific node
	set_node_state: function (node, state)
	{
		//console.log('set_node_state:'+node+','+state);
		//console.log(this.nodes[node]);
		this.nodes[node].set_state(state);
	},

	// Set the anchor positions for all the nodes
	set_anchors: function ()
	{
		console.log('set_anchors');
		for (var i=0; i<this.nodes.length; i++)
		{
			this.nodes[i].set_anchor();
		}
	},

	// Restore the position that was set as the  the anchor positions for all the nodes
	restore_anchors: function ()
	{
		console.log('restore_anchors');
		for (var i=0; i<this.nodes.length; i++)
		{
			this.nodes[i].restore_anchor();
		}
	},

	//
	// update node positions
	//
	// need to use "g." instead of "this." because it's a listener function 
	update_node_positions: function(e) 
	{	
		//console.log('e.data.updated:'+e.data.updated);
		g.update_collision = e.data.updated;
		
		if (mousedown) 
		{
			return;
		}
		else 
		{
			for (var id in e.data.nodes) 
			{
				var thisnode = e.data.nodes[id];
				for (var i = 0; i < g.nodes.length; i++) 
				{
					if(g.nodes[i].uid == thisnode['uid']) 
					{
						g.nodes[i].set_position(thisnode['x'], thisnode['y']);
						break;
					}
				}
			}
		}
	},

	zoomTree: function (zoom_offset, position)
	{

		// Calculate the current position of the cursor relative to the virtual screen
		var virtual_x = g.corner_x + position.x * g.zoom;
		var virtual_y = g.corner_y + position.y * g.zoom;

		
		// Constrain the zoom to the minimum and maximum values
		g.zoom += zoom_offset;
		if (zoom_offset < 0 && g.zoom <= GRAPH_MIN_ZOOM) g.zoom = GRAPH_MIN_ZOOM;
		if (zoom_offset > 0 && g.zoom >= GRAPH_MAX_ZOOM) g.zoom = GRAPH_MAX_ZOOM;

		// Calculate the current position of the cursor relative to the virtual screen
		var new_virtual_x = g.corner_x + position.x * g.zoom;
		var new_virtual_y = g.corner_y + position.y * g.zoom;

		g.corner_x += virtual_x - new_virtual_x;
		g.corner_y += virtual_y - new_virtual_y;

		//console.log('g.corner_x:'+g.corner_x +'    g.corner_y:'+g.corner_y);
		startAnimation();    
	},

	
	//
	// find root
	//
	
	// Finds the root of the tree
	// @todo update this function
	find_root: function() 
	{
		/*
		for (var i=0; i<this.nodes.length; i++) 
		{
			if (this.nodes[i]['type']['name'] == 'Root') 
			{
				return i;
			}
		}
		return -1;
		*/

		return this.nodes[0];
	},
	
	//
	// drawing
	//
	
	draw: function (ctx)
	{
		//console.log('Yoink! '+this.update_collision+'  '+this.update_animation);
		// update nodes positions with a webworker
		if (this.update_collision ) 
		{
			var mynodes = [];
			for (var j = 0; j < this.nodes.length; j++) 
			{
				mynodes[j] = [];
				mynodes[j]['uid'] = this.nodes[j]['uid'];
				mynodes[j]['x'] = this.nodes[j]['virtual_x'];
				mynodes[j]['y'] = this.nodes[j]['virtual_y'];
			}
			this.collision_worker.postMessage({'nodes': mynodes});
		}
		
		// clear canvas background
		ctx.clearRect(0,0,cw,ch);
		ctx.setLineDash([]);

		// Draw the source nodes on the left side
	    ctx.fillStyle='#555555';

	    var aux_x = this.n.horizontal_margin*2 + this.n.node_width;

	    var aux_y =  this.n.top_margin + this.n.nodes.length * ( this.n.node_height + this.n.vertical_margin );

		var CORNER_RADIUS = 20;

	    ctx.beginPath();
	    ctx.moveTo(0, aux_y);
	    ctx.lineTo(aux_x - CORNER_RADIUS, aux_y);
	    ctx.quadraticCurveTo(aux_x, aux_y, aux_x,  aux_y - CORNER_RADIUS);
	    ctx.lineTo(aux_x, 0);
	    ctx.lineTo(0, 0);
	    ctx.closePath();
	    ctx.fill();

/*
	this.top_margin = 30;
	this.vertical_margin = 20;
	this.horizontal_margin = 20;
	this.vertical_offset = 50;
	this.node_width = 50;
	this.node_height = 50;
*/

		var ypos = this.n.top_margin;

		for (n = 0; n < this.n.nodes.length; n++) 
		{
			var image = this.n.nodes[n].image;

			if (!this.n.nodes[n].enabled) ctx.globalAlpha = 0.2;
			else ctx.globalAlpha = 1.0;
			//console.log( image );
	    	if (image) ctx.drawImage(image, this.n.horizontal_margin, ypos, this.n.node_width, this.n.node_height);
			
			ypos += this.n.node_height + this.n.vertical_margin;
		}



		// Animate the nodes (expanding/collapsing the tree)
		this.update_animation = false;
		for (var i = 0; i < this.nodes.length; i++) 
		{
			if (this.nodes[i].update_animation()) this.update_animation = true;
		}
		//console.log('this.update_animation:'+this.update_animation)
		
		// If no nodes are moving we don't need to keet redrawing everything
		if (!this.update_collision && !this.update_animation) 
		{
			//console.log('-------------------------------------le cancel!!!');
			cancelAnimationFrame(animation_frame);
		}
		
		
		// draw connections between nodes
		for (var i = 0; i < this.nodes.length; i++) 
		{
			this.nodes[i].draw_connections(ctx);
		}
		
		//draw nodes
		for (var i = this.nodes.length-1; i>=0; i--) 
		{
			//console.log(this.nodes[i]);
			this.nodes[i].draw(ctx);
		}
		
		// Draw the temporary node, if it exists
		if (this.n.source_selected != -1) this.draw_temp();

		//console.log('g.boxStartX:'+g.boxStartX+'  last_mouse_x-g.boxStartX:'+(last_mouse_x-g.boxStartX));
		// Draw the select bounding box
		if (g.box_select)
		{			
			ctx.lineWidth = 3;
			ctx.strokeStyle = '#4466FF';
			ctx.setLineDash([6, 2]);
			
			ctx.rect(g.boxStartX, g.boxStartY, last_mouse_x-g.boxStartX, last_mouse_y-g.boxStartY);
			ctx.stroke();
		}
	},
	// Unselect all nodes and connections
	unselect_all: function()
	{
		for (var i = 0; i < this.nodes.length; i++) 
		{	
			this.nodes[i].unselect();
			this.nodes[i].connections_unselect();				
		}		
	},

	//
	// mouse handling
	//

	// Returns the number of the node that is closest to the x,y position, 
	// assuming that position is inside the node
	node_clicked: function(x, y)
	{
		var min_distance = null;
		var closest_node = -1;
		for (var i = 0; i < this.nodes.length; i++)
		{
			// Don't bother checking nodes that are not visible
			if (this.nodes[i].display === false) continue;
			// If the node is visible, calculate the distace to the point
			var distance = distance_to_point(x,y,this.nodes[i].display_x, this.nodes[i].display_y);
			// If the distance is shorter than the node radius, the click touched the node
			if ( distance < this.nodes[i].radius * g.zoom)
			{
				// Now we check if the click was closer to this node than all the other nodes
				if (min_distance === null || min_distance>distance)
				{
					distance = min_distance;
					closest_node = i;
				}
			}
		}
		return closest_node;
	},



	// Returns the number of the node collapse button that is closest to the x,y position, 
	// assuming that position is inside the collapse button
	node_expand_clicked: function(x, y)
	{
		var min_distance = null;
		var closest_button = -1;
		for (var i = 0; i < this.nodes.length; i++)
		{
			// Don't bother checking nodes that are not visible
			if (this.nodes[i].display === false) continue;

			var radius = this.nodes[i].radius * g.zoom;
			// If the node is visible, calculate the distace to the point
			var distance = distance_to_point(x, y, this.nodes[i].display_x,
				 this.nodes[i].display_y + radius * 1.5);
			// If the distance is shorter than the expand button radius, the click touched the button
			if ( distance < radius/2)
			{
				// Now we check if the click was closer to this node than all the other nodes
				if (min_distance === null || min_distance>distance)
				{
					distance = min_distance;
					closest_button = i;
				}
			}
		}
		return closest_button;
	},

	// Returns the number of the parent and child nodes that for the connection closest to 
	// the x,y position, assuming the connection line is close enough to the click
	connection_clicked: function(x, y)
	{
		var hit_connection_parent = -1;				
		var hit_connection_child = -1;
		var hit_connection_distance = CONNECTION_CLICK_THRESHOLD;
		// Go through all the nodes...
		for (var i = 0; i < this.nodes.length; i++)
		{
			// For each node, check all the child connections
			for (var j = 0; j < this.nodes[i].children.length; j++)
			{
				//console.log('checking connection:'+i+','+j );
				// Calculate the distance between the click and the connectione between two nodes 
				// Don't bother checking connections that are not visible
				if (this.nodes[i].display === false || this.nodes[i].children[j] === false) continue;

				var distance = distToSegment({display_x:x, display_y:y}, this.nodes[i], this.nodes[i].children[j]);
				// Check if that distance is smaller the the shorter distance found so far
				if (distance < hit_connection_distance)
				{
					hit_connection_distance = distance;
					hit_connection_parent = i;
					hit_connection_child = j;
				}
			}
		}
		// If a connection was fount, return an object with the parent and child numbers
		if (hit_connection_parent !== -1)
		{
			return {parent:hit_connection_parent, child: hit_connection_child};
		}
		// Otherwise return a null object
		return null;
	},

	mouseDownLeft: function (x,y) 
	{
		if (this.pan_pasted)
		{
			this.pan_pasted = false;
			this.saveState();
			this.set_anchors();
		}
		this.inputBoxTyping = false;
		this.delete_selection = false;
		this.state_updated = false;
		this.pan_nodes = false;

		console.log('mouseDownLeft: x:'+x+'  y:'+y+'  ctrlDown:'+ctrlDown+'  shiftDown:'+shiftDown);

/*
	this.top_margin = 30;
	this.vertical_margin = 20;
	this.horizontal_margin = 20;
	this.vertical_offset = 50;
	this.node_width = 50;
	this.node_height = 50;
*/	

		var thisx = this.n.horizontal_margin + this.n.node_width/2;

		//console.log( 'thisx:' + thisx);


		// Check if the click is hitting any node
		var hit_node = this.node_clicked(x, y);		
		// A node was clicked, let's do stuff!!!
		if (hit_node != -1)
		{

			console.log(this.nodes[hit_node]);

			// Save the currently selected node
			this.selectedNode = hit_node;
			this.lastSelectedNode = hit_node;
			this.display_node_properties(hit_node);					
			if (this.nodes[hit_node].selected)
			{
				// Clicking on a selected node with the shift down toggles the whole sub-tree
				if (shiftDown) this.nodes[hit_node].select_tree(false);
			}
			else
			{
				// When the shift is not pressed, we remove the previous selection
				if (!shiftDown) this.unselect_all();
				// Clicking on a unselected node adds the whole sub-tree to the selection
				this.nodes[hit_node].select_tree(true);
			}
			startAnimation();
			return;
		}


		// Check if the click is hitting any node expand button
		var hit_node_button = this.node_expand_clicked(x, y);
		// A node button was clicked, let's do stuff!!!
		if (hit_node_button != -1)
		{
			console.log("ZOMG hit_node_button:"+hit_node_button);
			// Check if the node is currently expanded or collapsed and toggles it
			if (this.nodes[hit_node_button].expanded)	this.nodes[hit_node_button].collapse();
			else this.nodes[hit_node_button].expand();
			startAnimation();
			return;
		}

		// Check if the click is hitting any connection between nodes
		var hit_connection = this.connection_clicked(x, y);
		// A connection was clicked, let's do stuff!!!
		if (hit_connection !== null)
		{
			console.log("ZOMG hit_connection:"+hit_connection.parent+'  '+hit_connection.child);

			// When the shift is not pressed, we remove the previous selection
			if (!shiftDown) this.unselect_all();

			this.nodes[hit_connection.parent].connection_toggle(this.nodes[hit_connection.parent].children[hit_connection.child]);

			startAnimation();
			return;
		}

		// Let's check if the click hitted any source node selectors
		for (var n = 0; n < this.n.nodes.length; n++) 
		{
			var thisy = this.n.top_margin+this.n.node_height/2 +  n*(this.n.node_height+this.n.vertical_margin);

			//console.log( 'thisy:' + thisy);
			// We hit a new node selector, let's create a temp node
			if (distance_to_point(x,y,thisx,thisy) < this.n.sources_radius) 
			{
				// Make sure this type of node can be instantiated
				if (this.n.nodes[n].enabled === false)
				{
					// A duplicate unique node was found, won't create this one
					openPopUp(t[lang]['error_cannot_instantiate_node']);
					break;
				}

				this.n.source_selected = n;

				// Unselect all other nodes and connections
				this.unselect_all();

				this.update_temp(x,y);
				startAnimation();
				return;
			}
		}				



		// At this point, nothing was clicked... Let's start drawing a bounding box
		this.box_select = true;
		this.boxStartX = x;
		this.boxStartY = y;

		// Delete the previouslly selected elements if shift is not pressed
		if (!shiftDown) this.unselect_all();

		// Show the default toolbox, since no element is currently selected
		this.clear_display();
	},

	mouseDownRight: function (x,y) 
	{
		this.pan_pasted = false;
		this.inputBoxTyping = false;
		this.delete_selection = false;
		this.state_updated = false;
		this.pan_nodes = false;

		// Check if the click is hitting any node
		var hit_node = this.node_clicked(x, y);
		// A node was clicked, let's do stuff!!!
		if (hit_node != -1)
		{
			// Save the currently selected node
			this.selectedNode = hit_node;
			this.lastSelectedNode = hit_node;
			this.display_node_properties(hit_node);					
			if (this.nodes[hit_node].selected)
			{
				// CLicking on a selected node with the shift down toggles the node selection
				if (shiftDown) this.nodes[hit_node].select(false);
			}
			else
			{
				// When the shift is not pressed, we remove the previous selection
				if (!shiftDown) this.unselect_all();
				// Clicking on a unselected adds the node to the selection
				this.nodes[hit_node].select(true);
			}
			startAnimation();
			return;
		}

		// At this point, nothing was clicked... Let's start panning the canvas
		this.dragStartX = x;
		this.dragStartY = y;
		this.pan_canvas = true;
		startAnimation();
	},

	mouseUp: function (x,y) 
	{
		startAnimation();

		// Are we supposed to unselect the current selection?
		if (this.delete_selection) this.unselect_all();
		g.update_collision = true;

		//console.log('mouseUp: source_selected:'+this.n.source_selected+'  selectedNode:'+this.selectedNode);
		var moved_node = null;
		// Check if we were dragging a new node
		if (this.n.source_selected != -1) 
		{
			console.log('New node!');
			var srctype = this.n.nodes[this.n.source_selected];
			
			// Let's check if the new node is valid
			var valid = true;

			// Check if any unique nodes are being duplicated
			if (srctype.unique == true) 
			{
				for (var i = 0; i < this.nodes.length; i++) 
				{
					if ( this.nodes[i].type.name == srctype.name)
					{
						// A duplicate unique node was found, won't create this one
						openPopUp(t[lang]['error_duplicate_unique_node']);
						valid = false;
						break;
					}
				}
			}

			// Otherwise all is good, let's create the new node
			if (valid) 
			{
				moved_node = g.add_node('',	deepCopy(srctype).init(), x, y, 20);
				if (moved_node.type.initPointers) moved_node.type.initPointers();

				console.log('#######################################################');				
				console.log(srctype);
				console.log(moved_node);


				moved_node.selected = true;
				this.saveState();
			}
		}
		// Check if we were dragging an existing node with no parent
		else if (this.selectedNode != -1)
		{
			moved_node = this.nodes[this.selectedNode];
		}
		
		//console.log('moved_node:');	console.log(moved_node);

		// If we were dragging a node (either a new or an existing one) with no parent, 
		// let's see if it landed on top of another existing node
		if (moved_node != null && moved_node.parent === null)
		{
			console.log("BING: x:"+x+"  y:"+y);
			
			for (var i = 0; i < this.nodes.length; i++)
			{		
				// Compare the node position with all the other nodes
				// Won't compare with itself, or with nodes that are not visible
				if (this.nodes[i] !==  moved_node && this.nodes[i].display &&
					distance_to_point(x,y,this.nodes[i].display_x,this.nodes[i].display_y) < this.nodes[i].radius*2) 
				{
					console.log("LE DROP!!!");

					// Now let's check in the node can be a valid parent node
					var parent = this.nodes[i];

					// don't allow to add more nodes to leaf nodes 
					if (parent.type.leaf === true) 
					{
						openPopUp(t[lang]['error_leaf']);
						break;
					}
					
					// don't allow a node to be a child of a descendant
					// (this would create a closed infinite loop)
					if (parent.check_ancestors(moved_node)) 
					{
						openPopUp(t[lang]['error_loop']);
						break;
					}
					
					// @todo mudar estas coordenadas para a cena do virtual screen
					//moved_node.virtual_y += 60;

					// Adds all the selected nodes that have no parent as children of the base node
					for (var n=0; n<g.nodes.length; n++ )
					{
						//console.log("node:"+n+"  selected:"+g.nodes[n].selected+"  parent:"+g.nodes[n].parent);

						if (g.nodes[n].selected && g.nodes[n].parent === null)
						{
							// Make sure not to add more than one node to a parent node that can only have one child
							if (parent.children.length > 0 && parent.type.single_child)
							{
								openPopUp(t[lang]['error_single_child']);
								break;
							}
							parent.add_child(g.nodes[n]);
						}

					} 

					//g.nodes[i].add_child(g.nodes[moved_node]);

					//console.log("----------------");
					//console.log(g.nodes[i]);
					//console.log(g.nodes[moved_node]);


					this.restore_anchors();
				
					//console.log(g.n);

					g.n.unselect_source();

					// No need to check any other nodes
					break;
				}
			}

		}
		
		else 
		{
			// check if it's releasing a selected node
			for (var i = 0; i < this.nodes.length; i++) 
			{	
				if ( ( distance_to_point(x,y,this.nodes[i].display_x,this.nodes[i].display_y) < this.nodes[i].radius) &&
					 ( this.nodes[i].selected ) )
				{
					//console.log('display this nodes info');
					this.selectedNode = i;
					this.lastSelectedNode = i;
					// @todo originally was here... should it be?
					//this.display_node_properties(i);
				}
			}	
		}
		
		// If we're selecting nodes within a bounding box, let's mark them all as selected
		if (this.box_select)
		{
			if (!shiftDown) this.unselect_all();
			var x_start = Math.min(x, this.boxStartX);
			var x_end = Math.max(x, this.boxStartX);
			var y_start = Math.min(y, this.boxStartY);
			var y_end = Math.max(y, this.boxStartY);
			for (var i = 0; i < this.nodes.length; i++)
			{
				if ( this.nodes[i].display_x > x_start &&  this.nodes[i].display_x < x_end &&
					 this.nodes[i].display_y > y_start &&  this.nodes[i].display_y < y_end)
				{
					this.nodes[i].selected = true;
				}
			}
		}

		this.n.source_selected = -1;
		this.selectedNode = -1;

		this.pan_canvas = false;
		this.box_select = false;
		this.tree_moved = true;
		startAnimation();



		if (this.pan_nodes)
		{
			this.set_anchors();
		}

		if (this.state_updated)	this.saveState();


	},
	mouseMove: function (x,y) 
	{
		//console.log('mouseMove: x:'+x+'  y:'+y+ '  this.pan_pasted:'+this.pan_pasted);

		// Get the offset of the draw since the last update
		var offset_x = this.dragStartX - x;
		var offset_y = this.dragStartY - y;
		// Set the current position as the last update		
		this.dragStartX = x;
		this.dragStartY = y;

		if (this.pan_pasted) g.pan_selected_nodes(offset_x, offset_y);
		startAnimation();
	},
	mouseDrag: function (x,y) 
	{
		//console.log('mouseDrag: x:'+x+'  y:'+y);

		// Get the offset of the draw since the last update
		var offset_x = this.dragStartX - x;
		var offset_y = this.dragStartY - y;
		// Set the current position as the last update		
		this.dragStartX = x;
		this.dragStartY = y;

		// Let's see if we're dragging a new node...
		var s = this.n.source_selected;
		if (s != -1) 
		{
			// dragging from a source, draw a temporary node
			this.update_temp(x,y);
		}
		// The node isn't new, so we're moving existing selected nodes
		else if (!this.box_select && !this.pan_canvas) 
		{
			g.pan_selected_nodes(offset_x, offset_y);
			this.pan_nodes = true;
		}
		// If we're panning the canvas we don't want to unselect all the nodes 
		if (this.pan_canvas) this.delete_selection = false;
		// If we're panning the canvas, we just need to move the corners of the view window
		if (this.pan_canvas) 
		{
			g.corner_x -= offset_x;
			g.corner_y -= offset_y;
		}

		//console.log('g.corner_x:'+g.corner_x +'    g.corner_y:'+g.corner_y);
			
		startAnimation();
	},
	
	// Pan all the selected nodes in the tree by a specific offset
	pan_selected_nodes: function(x, y)
	{				
		for (var i = 0; i < this.nodes.length; i++) 
		{
			if(this.nodes[i].selected) 
			{								
				this.nodes[i].move_node(x, y);
			}
		}
		this.state_updated = true;

	},


	//
	// temporary node
	//
	
	update_temp: function (x,y) 
	{
		this.tempx = x;
		this.tempy = y;
	},
	
	draw_temp: function () 
	{

		// Make sure the image exists... If the node has no defined image, use a default value
		var image = this.n.nodes[this.n.source_selected].image;

	    ctx.drawImage(image, this.tempx-this.n.node_width/2, this.tempy-this.n.node_height/2, 
	    					this.n.node_width, this.n.node_height);
	},
	
	//
	// properties display box
	//
	
	display_node_properties: function(i) 
	{
		console.log("display_node_properties:"+i);
		this.visible_node_properties = i;
		$('#propertiesdiv').css('display', 'block');

		var dom = document.getElementById('propertiesdisplaybox');
		if (dom) {

			var output = '';
			
			// properties table
			output += '<table>';
			
			// add uid, unchangable
			output += '<tr><td>'+t[lang]['UID']+'</td><td>'+this.nodes[i].uid+'</td></tr>';
			
			// add name, editable textbox
			output += '<tr><td>'+t[lang]['Name']+'</td><td><input id="text_node_name" value="'+
				this.nodes[i].name+'"/></td></tr>';
			
			output += '</table>';

			// node-specific properties
			output += this.nodes[i].type.list_properties();
			
			dom.innerHTML = output;
			
			// add listener for auto-update of name
			var nameInputBox = document.getElementById('text_node_name');
			nameInputBox.onkeyup = function() 
			{
				g.inputBoxTyping = true;
				g.nodes[i].name = nameInputBox.value;
			}
			this.nodes[i].type.add_list_properties_listeners();

		}
	},
	
	show_com_ports: function() 
	{
		var selector = $('#select_com_port');
		//console.log(selected_com_port);
		//console.log(selector.length);

		// Make sure the selector exists
		if (selector.length !== 1) return; 
		var html = '';
		for (var i=0; i<com_ports_list.length; i++)
		{
			html += "<option value='"+i+"'' class='com_option'>"+com_ports_list[i]+"</option>";
		}
		if (com_ports_list.length === 0)  html += "<option value='-1' class='com_option'>No ports available</option>";
		$(selector).html(html);
	},

	show_connection_state: function()
	{
		var file = connected_com_port === '' ? 'disconnected' : 'connected';

		$('#button_com_connect').attr('src', 'images/header/'+file+'.svg');

	},

	handle_connection: function()
	{
		if (connected_com_port === '') { sendData('#CONNECT,'+selected_com_port+',;'); }
		else { sendData('#DISCONNECT,;'); }
	},

	clear_display: function() 
	{
		$('#propertiesdiv').css('display', 'none');

		var dom = document.getElementById('propertiesdisplaybox');
		if (dom) 
		{

			$('#propertiesdiv').css('display', 'none');			
		}
		
	},
	target_platform_changed: function() 
	{
		// get root targets reference
		for (var i=0; i<this.nodes.length; i++) {
			output.push( this.nodes[i].export_node() );
		}
		
		// if a node is of a type listed in root targets,
		// and is using that template, update it
		for (var i=0; i<this.nodes.length; i++) {
			// if has template
			// and if has same type as listed in one of the root targets
			// and the name is the same
			
			//todo: change the type.targeted
			
			//todo: finish this
		}
	},

	//
	// import / export
	//
	
	// Re-order all nodes so the order matches the visual appearance
	sort_nodes: function()
	{
		var root = this.find_root();
		root.sort_children(0);
		this.nodes.sort(function(node1, node2) 
			{
				return node1.id - node2.id;
			});
		
	},
	
	export_nodes: function() 
	{
		var output = [];
		for (var i=0; i<this.nodes.length; i++) 
		{
			output.push( this.nodes[i].export_node() );
		}
		return output;
	},
	
	export_connections: function() 
	{
		var output = [];
		for (var i=0; i<this.nodes.length; i++) 
		{
			console.log('node:'+i);
			console.log(this.nodes[i]);
			
			for (var j=0; j<this.nodes[i].children.length; j++)
			{		
				console.log('exporting:'+i+','+j);
				output.push( this.nodes[i].export_connection(j) );
			}
		}
		return output;				
	},
	
	export_json: function() 
	{
		this.sort_nodes();
		this.exportjson = {
			"nodes": this.export_nodes(),
			"connections": this.export_connections()
		};
		
		this.jsonstring = JSON.stringify(this.exportjson);
		
		return this.jsonstring;
	},
	export_arduino: function() 
	{
		this.sort_nodes();
		
		// header

		var output = 'T'+padByte(g.devices.length)+padByte(this.nodes.length)+';';


		// list all devices
		for (var d=0; d<g.devices.length; d++)
		{
			output += g.devices[d].export_arduino(d);

			//console.log(g.devices[d]);
			//console.log(g.devices[d].export_arduino(d));
		}

//		D000103;D010105;D020106;D030308;';

//		D000105;D010106;D020107;D04030801;D05030901;D06030a01;N000002;N0103100001;N0203100001;C0001;C0002;F;		
		
		//L;N00010201;N010202;N020202;N030004;N0403100001;N05030000000088;N0603100000;N07030000000099;N080004;N0903100101;N0A0300000002FF;N0B03100100;N0C0300000003A6;C0001;C0002;C0103;C0304;C0305;C0306;C0307;C0208;C0809;C080A;C080B;C080C;F;
				
		// list all nodes
		for (var i=0; i<this.nodes.length; i++) 
		{
			output += this.nodes[i].export_arduino();
		}
		for (var i=0; i<this.nodes.length; i++) 
		{
			output += this.nodes[i].export_connections();
		}

		output += 'F;';
		
		return output;
	},
	
	// Debug function to reset the node grow animation
	reset_animation: function(value)
	{
		for (var i = 0; i < this.nodes.length; i++) 
		{
			this.nodes[i].reset_animation(value);
		}
		
	},
	

	// Imports a tree from a json string
	// if a parent is specified, the first node of the imported tree
	// (which should be its root) is added as a child of the parent node
	import_json: function(import_root, jsonstring, parent) 
	{
		//console.log('import_json:'+jsonstring);
		//console.log('parent:');
		//console.log(parent);
		var temp = JSON.parse(jsonstring);

		// Initialize all the devices
		for (var i=0; i<temp['devices'].length; i++) 
		{
			g.addDevice(temp['devices'][i]);
		}

		//console.log(g.devices);

		// Offset to apply to all of the loaded nodes (when a parent exists)
		// The offset is calculated so that the loaded tree stays below the parent node
		var x_offset = 0;
		var y_offset = 0;
		
		// variable with associations between the old and new ids for the new nodes
		// (they have specific id's in the json string, but will get new ones now)
		var node_convert = {};
		
		var root_node = true;
		// Import all the nodes. The root should be the node 0
		// If we don't want to import the root, start on node 1
		var first_node = import_root ? 0 : 1;
		for (var i=first_node; i<temp['nodes'].length; i++) 
		{
			//console.log('ingesting nodes: ' + temp['nodes'][i]['uid']);		

			var nodeinfoname = temp['nodes'][i]['type']['name'];
			
			// go through all existing nodetypes and select as mytype
			var mytype = null;
			for (var n = 0; n < this.n.nodes.length; n++) 
			{
				var thisname = this.n.nodes[n].toString();

				if ( nodeinfoname === thisname) 
				{
					mytype = this.n.nodes[n];
				}
			}
			// Assuming the node type is valid, adds it to the list 
			if (mytype != null) 
			{
				// instantialize this node
				var thisnode = this.add_node(
								temp['nodes'][i]['name'],
								deepCopy(mytype).init(),
								temp['nodes'][i]['x'],
								temp['nodes'][i]['y'],
								temp['nodes'][i]['radius']
							);

				// Create the association between the old and the new ID
				node_convert[temp['nodes'][i]['uid']] = thisnode.uid;
				
				// set the nodetype properties
				for( tY in temp['nodes'][i]['type'] ) 
				{
					thisnode['type'][tY] = temp['nodes'][i]['type'][tY];
				}
				
				//console.log( thisnode['type'] );

				// If a parent node was specified, the new tree is a branch
				// of the main tree, so there is extra stuff to do here
				if (parent)
				{			
					//Check if we're processing the root node from the new tree
					if (root_node)
					{
						// Let's make it a child of the specified parent node
						parent.add_child(thisnode);

						// Now we calculate the position offset, so that the new
						// tree will appear below the specified parent node
						x_offset = parent.virtual_x-thisnode.virtual_x;
						y_offset = parent.virtual_y-thisnode.virtual_y + 80;
						
						console.log('x_offset:'+x_offset+'  y_offset:'+y_offset);
						root_node = false;						
					}
					
					// Move the new nodes to the desired position below the parent
					thisnode.virtual_x = thisnode.virtual_x + x_offset;
					thisnode.virtual_y = thisnode.virtual_y + y_offset;

					// Lock all the new nodes
					thisnode.locked = true;					
				}
				
			} else 
			{
				openPopUp(t[lang]['unknown_type']);
			}
		}
				
		// import connections
		for (var i=0; i<temp['connections'].length; i++) 
		{
			var cname = temp['connections'][i];
			var res = cname.split(':');
			var node1 = null;
			var node2 = null;
			
			// Look for the nodes on this specific connection
			for (var j=0; j<this.nodes.length; j++) 
			{
				// node_convert is used to convert the old ids into the new ones
				if (node_convert[res[0]] == this.nodes[j].toString()) node1 = this.nodes[j];
				if (node_convert[res[1]] == this.nodes[j].toString()) node2 = this.nodes[j];
				// If both nodes were found, don't need to look any more
				if (node1 !== null && node2 !== null) break;
			}
			
			//console.log('outofloop: ' + node1.toString() + ' ' + node2.toString());
			if ((node1 != null) && (node2 != null)) 
			{
				node1.add_child(node2);
			}
		}

		// Replace any node references by the matching pointers, if necessary
		for (var n=0; n<this.nodes.length; n++) 
		{
			if (this.nodes[n].type.initPointers) this.nodes[n].type.initPointers();
		}
		this.jsonstring = '';
		this.saveState();

		g.n.refresh_devices();
	},

	export_to_json: function() 
	{
		var textToWrite = g.export_json();
		console.log(textToWrite);
		
		var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
		var fileNameToSaveAs = 'export.json';

		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.innerHTML = "Download File";
		if (window.webkitURL != null)
		{
			// Chrome allows the link to be clicked
			// without actually adding it to the DOM.
			downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
		}
		else
		{
			// Firefox requires the link to be added to the DOM
			// before it can be clicked.
			downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
			downloadLink.onclick = function(event) {
					document.body.removeChild(event.target);
				};
			downloadLink.style.display = "none";
			document.body.appendChild(downloadLink);
		}

		downloadLink.click();
	},

	upload_to_arduino: function(save) 
	{
		console.log("upload_to_arduino");
		var textToWrite = (save? 'E;': 'L;') + g.export_arduino();
		sendData(textToWrite, true);
	},

	// Returns the number of a device from its ID value
	getDeviceById: function(id)
	{
		for (var i=0; i<g.devices.length; i++)
		{
			if (g.devices[i].id === id ) return i;
		}
		return -1;
	},

	// Adds a new device to the device list based on the device data
	addDevice: function(data)
	{
		//console.log(data);

		// Let's see what device type we want to create
		switch (data.type)
		{
			case "DigitalOutput":
				g.devices.push(new Device(data.id, data.name, new DeviceTypeDigitalOutput( data.pin, data.initial_state) ));
			break;
			case "DigitalInput":
				g.devices.push(new Device(data.id, data.name, new DeviceTypeDigitalInput( data.pin, data.active_type) ));
			break;
			case "DifferentialMotor":
				g.devices.push(new Device(data.id, data.name, new DeviceTypeDifferentialMotor( data.pin_A1, data.pin_A2, data.pin_B1, data.pin_B2, 
																						data.initial_speed_A, data.initial_speed_B) ));
			break;
		}

	},

	// Instantiates a new tree from data read from a file
	create_tree: function(data)
	{
		g.reset_graph();
		g.import_json(true, data);
		startAnimation();
	},

	// Opens a new tree from a file
	open_tree: function()
	{
		var x;
	  	var fileToLoad = document.getElementById("button_load_json").files[0];

	  	console.log(fileToLoad);

		var fileReader = new FileReader();
		fileReader.onload = function(fileLoadedEvent) 
		{
			g.create_tree(fileLoadedEvent.target.result);
		};
		fileReader.readAsText(fileToLoad, "UTF-8");
	},
	
	// Loads a new tree from a file
	load_tree: function(file_url)
	{
		// ajax call to load the game JSON file
		$.ajax({
		    url: file_url,
		    dataType: 'text',
		    success: g.create_tree,
		    error: function( data ) { openPopUp( "ERROR LOADING FILE: " + file_url ); }
		});

	},
	
};
