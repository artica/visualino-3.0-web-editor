// Base node type identifiers (for arduino)
var NODE_SEQUENCE = 0x00;
var NODE_PARALLEL =	0x01;
var NODE_OVERRIDE = 0x02;
var NODE_SERVICE = 0x03;

// Service type codes (for arduino)
var SERVICE_DELAY = 0x00;
var SERVICE_OUTPUT_DIGITAL_PIN 			= 0x10;
var SERVICE_OUTPUT_ANALOG_PIN 			= 0x11;
var SERVICE_OUTPUT_DIFFERENTIAL_MOTOR 	= 0x12;
var SERVICE_INPUT_DIGITAL_PIN 			= 0x20;
var SERVICE_INPUT_ANALOG_PIN 			= 0x21;

// Node state 
var NODE_STATE_FAILURE = 0;
var NODE_STATE_RUNNING = 1;
var NODE_STATE_SUCCESS = 2;
var NODE_STATE_INACTIVE = 3;	


function NodeTypes() 
{
	this.nodes = 
		[
			new NodeTypeRoot(),
			new NodeTypeJsonImport(),
			new NodeTypeParallel(),
			new NodeTypeSequence(),
			new NodeTypeStatusOverride(),
			new NodeTypeInput(),
			new NodeTypeOutput(),
			new NodeTypeDelayTime(),
		]
		
	this.source_selected = -1; // source index being dragged

	this.sources_radius = 20;
	
	// screen positioning
	this.top_margin = 20;
	this.vertical_margin = 10;
	this.horizontal_margin = 10;
	this.node_width = this.sources_radius*2;
	this.node_height = this.sources_radius*2;

}

NodeTypes.prototype = 
{
	unselect_source: function() 
	{
		this.source_selected = -1;
	},

	// Refresh the default devices used in the Input and Output nodes
	refresh_devices: function() 
	{
		// Look for the input and output nodes, and set a default device for each one
		for (var i=0; i<this.nodes.length; i++)
		{
			// By default, all the selector nodes ate enabled
			this.nodes[i].enabled = true;
			//console.log(this.nodes[i]);

			// If this node is a Output type, initialize it with a default output device
			if ( this.nodes[i] instanceof NodeTypeOutput)
			{
				// Disable the node selector untill we find an output device
				this.nodes[i].enabled = false;
				// Go throught the device list and look for an output device
				for (var d=0; d<g.devices.length; d++)
				{
					// An output was found, let's initialize 
					if (g.devices[d].type.class === DEVICE_CLASS_OUTPUT)
					{
						this.nodes[i].device_id = g.devices[d].id;
						this.nodes[i].device = g.devices[d];
						// We have at least a device, so let's enable the output node selector
						this.nodes[i].enabled = true;
						break;
					}
				}
			}

			// If this node is a Input type, initialize it with a default input device
			if ( this.nodes[i] instanceof NodeTypeInput)
			{
				// Disable the node selector untill we find an input device
				this.nodes[i].enabled = false;
				// Go throught the device list and look for an input device
				for (var d=0; d<g.devices.length; d++)
				{
					// An input was found, let's initialize 
					if (g.devices[d].type.class === DEVICE_CLASS_INPUT)
					{
						this.nodes[i].device_id = g.devices[d].id;
						this.nodes[i].device = g.devices[d];
						// We have at least a device, so let's enable the input node selector
						this.nodes[i].enabled = true;
						break;
					}
				}
			}
		}
	}


}

