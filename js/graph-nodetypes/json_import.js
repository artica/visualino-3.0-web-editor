
//
// Json Import
// 

function NodeTypeJsonImport() 
{
	this.name = 'JSON Import';
	this.init();

	return this.name;
}

NodeTypeJsonImport.prototype = 
{
	init: function() {
		this.unique = false;
		this.leaf = false;
		this.single_child = true;
		this.jsonstring = '';
		this.detailshidden = true;
		
		//todo: parse json and load as properties any binded parameters
		this.image  = images.getImage('import');		
		
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		
		var output = '';
		
		//todo: if hidden, place button to show detail
		if (this.detailshidden) {
			//todo: place button to show detail
		}
		
		// JSON File To Load
		output += '<table><tr><td>'+t[lang]['JSON String']+'</td><td><input id="jsonstringFileToLoad" type="file" /></td></tr></table>';
	
		// JSON String
		//output += '<tr><td>'+t[lang]['JSON String']+'</td><td><textarea id="jsonstringInput" name="jsonstringInput">' + this.jsonstring + '</textarea></td></tr>';
		
		//todo: list jsonfile or jsonstring
		//todo: list any binded parameters
		return output;
	},
	add_list_properties_listeners: function() 
	{
	
		var thisnode = this;
		
		// JSON File To Load
		var jsonstringFileToLoad = document.getElementById('jsonstringFileToLoad');
		jsonstringFileToLoad.onchange = function() 
		{
			console.log('load json file...');
			console.log(thisnode);
			
			var fileReader = new FileReader();
			fileReader.onload = function(fileLoadedEvent)
			{
				console.log(thisnode);
				var loaded_data = fileLoadedEvent.target.result;
//				thisnode.jsonstring = loaded_data;
				
							
				// @todo isto nao pode sacar o no assim...
				console.log('g.lastSelectedNode:'+g.lastSelectedNode)
				g.import_json(false, loaded_data, g.nodes[g.lastSelectedNode]);
				
			};			
			fileReader.readAsText(jsonstringFileToLoad.files[0], "UTF-8");
			
		}		
		return this;
	},

	export_nodetype: function() 
	{
		var output = 
		{
			'name': this.name
		};
		return output;
	},
	export_arduino: function(node) 
	{
		// As fas as the arduino knows these nodes are sequence nodes	
		return 'N' + padByte(node.id) + padByte(NODE_SEQUENCE) + 
			padByte(node.children.length)+';';	
	}
}
