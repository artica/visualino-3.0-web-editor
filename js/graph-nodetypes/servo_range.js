
//
// Servo Range
//

function NodeTypeServoRange() {
	this.name = 'Servo Range';
	this.init();
	return this.name;
}

NodeTypeServoRange.prototype = {
	init: function() {
		this.unique = false;
		this.leaf = true;
		this.pintypes = ['Digital','PWM'];
		this.pintype = 'Digital';
		this.pinvalue = 9;
		this.min = 20;
		this.max = 160;
		this.inc = 1;
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '';
		
		// pin type		
		output += '<tr><td>'+t[lang]['Pin Type']+'</td><td>';
		output += '<select id="pintypeInput">';
		for (var i=0; i < this.pintypes.length; i++) {
			var selected = '';
			if (this.pintype == this.pintypes[i]) selected = 'selected="true"';
			output += '<option value="'+this.pintypes[i]+'" '+selected+'>' + t[lang][this.pintypes[i]] + '</option>';
		}
		output += '</td></tr>';

		// pin value
		output += '<tr><td>'+t[lang]['Pin']+'</td><td><input id="pinInput" name="pinInput" value="'+this.pinvalue+'"></td></tr>';
		
		// min
		output += '<tr><td>'+t[lang]['min']+'</td><td><input id="minInput" name="minInput" value="'+this.min+'"></td></tr>';
		
		// max
		output += '<tr><td>'+t[lang]['max']+'</td><td><input id="maxInput" name="maxInput" value="'+this.max+'"></td></tr>';
		
		// inc
		output += '<tr><td>'+t[lang]['inc']+'</td><td><input id="incInput" name="incInput" value="'+this.inc+'"></td></tr>';
		
		return output;
	},
	add_list_properties_listeners: function() {
	
		// pin type
		var pintypeInput = document.getElementById('pintypeInput');
		if (pintypeInput) {
			var thisnode = this;
			pintypeInput.onchange = function() {
				var index = pintypeInput.selectedIndex;
				var options = pintypeInput.options;
		
				for (var j = 0; j < thisnode.pintypes.length; j++) {
					if (options[index].value == thisnode.pintypes[j]) {
						thisnode.pintype = thisnode.pintypes[j];
					}
				}
			}
		}
		
		// pin value
		var pinInputBox = document.getElementById('pinInput');
		if (pinInputBox) {
			var thisnode = this;
			pinInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.pinvalue = pinInputBox.value;
			}
			pinInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			pinInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// min
		var minInputBox = document.getElementById('minInput');
		if (minInputBox) {
			var thisnode = this;
			minInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.value = minInputBox.value;
			}
			minInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			minInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// max
		var maxInputBox = document.getElementById('maxInput');
		if (maxInputBox) {
			var thisnode = this;
			maxInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.value = maxInputBox.value;
			}
			maxInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			maxInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}

		// inc
		var incInputBox = document.getElementById('incInput');
		if (incInputBox) {
			var thisnode = this;
			incInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.value = incInputBox.value;
			}
			incInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			incInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'pintype': this.pintype,
			'pinvalue': this.pinvalue,
			'min': this.min,
			'max': this.max,
			'inc': this.inc
		};
		return output;
	},
	export_arduino: function(nodeID) {
		return 'SL::Behaviors::ServoRange* ' + nodeID + ' = new SL::Behaviors::ServoRange('+this.min+','+this.max+','+this.inc+','+this.pinvalue+');';
	}
}
