
//
// Parallel
//

function NodeTypeParallel() {
	this.name = 'Parallel';
	this.init();
	return this.name;
}

NodeTypeParallel.prototype = {
	init: function() {
		this.unique = false;
		this.leaf = false;
		this.single_child = false;
		this.policies = {'Require One': 0, 'Require All': 1};
		this.completion_policy = this.policies['Require All'];		
		this.failure_policy = this.policies['Require One'];
		this.image  = images.getImage('parallel');		
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '<table>';
		
		// completion policy	
		output += '<tr><td>'+t[lang]['Completion Policy']+'</td><td>';
		output += '<select id="cpInput">';
		for (i in this.policies) {
			var selected = '';
			//console.log(this.failure_override + ' ' + this.overrides[i]);
			if (this.completion_policy == this.policies[i]) selected = 'selected="true"';
			output += '<option value="'+this.policies[i]+'" '+selected+'>' + t[lang][i] + '</option>';
		}
		output += '</td></tr>';
		
		// failure policy	
		output += '<tr><td>'+t[lang]['Failure Policy']+'</td><td>';
		output += '<select id="fpInput">';
		for (i in this.policies) {
			var selected = '';
			//console.log(this.failure_override + ' ' + this.overrides[i]);
			if (this.failure_policy == this.policies[i]) selected = 'selected="true"';
			output += '<option value="'+this.policies[i]+'" '+selected+'>' + t[lang][i] + '</option>';
		}
		output += '</td></tr></table>';

		return output;
	},
	add_list_properties_listeners: function() {
		// completion policy
		var cpInput = document.getElementById('cpInput');
		if (cpInput) {
			var thisnode = this;
			cpInput.onchange = function() {
				var index = cpInput.selectedIndex;
				var options = cpInput.options;
				thisnode.completion_policy = parseInt(options[index].value, 10);
			}
		}
		
		// failure policy
		var fpInput = document.getElementById('fpInput');
		if (fpInput) 
		{
			var thisnode = this;
			fpInput.onchange = function() 
			{
				var index = fpInput.selectedIndex;
				var options = fpInput.options;
				thisnode.failure_policy = parseInt(options[index].value, 10);
			}
		}
		return;
	},
	export_nodetype: function() 
	{
		var output = {
			'name': this.name,
			'completion_policy': this.completion_policy,
			'failure_policy': this.failure_policy
		};
		return output;
	},
	export_arduino: function(node) 
	{		
		var policy = 2*this.failure_policy + this.completion_policy;
		
		return 'N' + padByte(node.id) + padByte(NODE_PARALLEL) + 
			padByte(node.children.length)+padByte(policy)+';';		
	},
	draw: function(ctx, x, y, radius) {
		ctx.shadowOffsetX = 2;
		ctx.shadowOffsetY = 2;
		ctx.shadowBlur = 5;
		ctx.shadowColor = '#000000';
		
		var thischar = '1';
		if (this.completion_policy === 1) thischar = '*';
		drawText ( ctx, thischar, x-radius, y-radius, "#11ff11" );
		
		thischar = '1';
		if (this.failure_policy === 1) thischar = '*';
		drawText ( ctx, thischar, x-radius, y+radius*0.5, "#ff1111" );
		
		ctx.shadowOffsetX = 0;
		ctx.shadowOffsetY = 0;
		ctx.shadowBlur = 0;
	}
}

