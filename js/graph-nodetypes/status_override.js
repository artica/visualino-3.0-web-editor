
//
// Status Override
//
var NODE_OVERRIDE_POLICY_FAILURE_OFFSET = 0;
var NODE_OVERRIDE_POLICY_RUNNING_OFFSET = 2;
var NODE_OVERRIDE_POLICY_SUCCESS_OFFSET = 4;

function NodeTypeStatusOverride() {
	this.name = 'Status Override';
	this.init();
	return this.name;
}

NodeTypeStatusOverride.prototype = 
{
	init: function() 
	{
		this.unique = false;
		this.leaf = false;
		this.single_child = true;
		this.overrides = {'Fail': 0, 'Run': 1, 'Success': 2};
		this.failure_override = this.overrides['Fail'];
		this.running_override = this.overrides['Run'];
		this.success_override = this.overrides['Success'];
		this.image  = images.getImage('override');
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() 
	{
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '<table>';
	
		// failure override		
		output += '<tr><td>'+t[lang]['Failure Override']+'</td><td>';
		output += '<select id="foInput">';
		for (i in this.overrides) {
			var selected = '';
			//console.log(this.failure_override + ' ' + this.overrides[i]);
			if (this.failure_override == this.overrides[i]) selected = 'selected="true"';
			output += '<option value="'+this.overrides[i]+'" '+selected+'>' + t[lang][i] + '</option>';
		}
		output += '</td></tr>';

		// running override		
		output += '<tr><td>'+t[lang]['Running Override']+'</td><td>';
		output += '<select id="roInput">';
		for (i in this.overrides) {
			var selected = '';
			if (this.running_override == this.overrides[i]) selected = 'selected="true"';
			output += '<option value="'+this.overrides[i]+'" '+selected+'>' + t[lang][i] + '</option>';
		}
		output += '</td></tr>';
		//console.log('loading: ' + this.success_override);
		// success override		
		output += '<tr><td>'+t[lang]['Success Override']+'</td><td>';
		output += '<select id="soInput">';
		for (i in this.overrides) {
			var selected = '';
			if (this.success_override == this.overrides[i]) selected = 'selected="true"';
			output += '<option value="'+this.overrides[i]+'" '+selected+'>' + t[lang][i] + '</option>';
		}
		output += '</td></tr></table>';
		
		return output;
	},
	add_list_properties_listeners: function() {
	
		// failure override
		var foInput = document.getElementById('foInput');
		if (foInput) {
			var thisnode = this;
			foInput.onchange = function() {
				var index = foInput.selectedIndex;
				var options = foInput.options;
				thisnode.failure_override = parseInt(options[index].value, 10);
			}
		}

		// running override
		var roInput = document.getElementById('roInput');
		if (roInput) {
			var thisnode = this;
			roInput.onchange = function() {
				var index = roInput.selectedIndex;
				var options = roInput.options;
				thisnode.running_override = parseInt(options[index].value, 10);
			}
		}
		
		// success override
		var soInput = document.getElementById('soInput');
		if (soInput) {
			var thisnode = this;
			soInput.onchange = function() {
				var index = soInput.selectedIndex;
				var options = soInput.options;
				thisnode.success_override = parseInt(options[index].value, 10);
			}
		}
	
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'failure_override': this.failure_override,
			'running_override': this.running_override,
			'success_override': this.success_override
		};
		return output;
	},
	export_arduino: function(node) 
	{
		console.log('this.failure_override:'+this.failure_override);
		console.log('this.running_override:'+this.running_override);
		console.log('this.success_override:'+this.success_override);
		
		var policy = 
			(this.failure_override << NODE_OVERRIDE_POLICY_FAILURE_OFFSET) |
			(this.running_override << NODE_OVERRIDE_POLICY_RUNNING_OFFSET) |
			(this.success_override << NODE_OVERRIDE_POLICY_SUCCESS_OFFSET);

		console.log('policy:'+policy);
			
		return 'N' + padByte(node.id) +
			padByte(NODE_OVERRIDE) + padByte(policy)+';';

		//return 'BehaviorTree::StatusOverride* ' + nodeID + ' = new BehaviorTree::StatusOverride('+this.success_override+','+this.failure_override+','+this.running_override+');';
	},
	draw: function(ctx, x, y, radius) 
	{
		//ctx.shadowOffsetX = 2;
		//ctx.shadowOffsetY = 2;
		//ctx.shadowBlur = 5;
		//ctx.shadowColor = '#000000';
		
		var mycolors = ['#ee1111','#eeee11','#11ee11'];

		for (var i=0; i<=2; i++) {
			var mycolor = '#444444';
			if (i === this.failure_override) mycolor = mycolors[this.failure_override];
			drawCircle( ctx,
					x-radius+radius*(.33*i),
					y-radius,
					radius*0.12,
					mycolor,
					"transparent"
				);
		}
		
		for (i=0; i<=2; i++) {
			var mycolor = '#444444';
			if (i === this.running_override) mycolor = mycolors[this.running_override];
			drawCircle( ctx,
					x-radius+radius*(.33*i),
					y-radius*0.75,
					radius*0.12,
					mycolor,
					"transparent"
				);
		}
		
		for (i=0; i<=2; i++) {
			var mycolor = '#444444';
			if (i === this.success_override) mycolor = mycolors[this.success_override];
			drawCircle( ctx,
					x-radius+radius*(.33*i),
					y-radius*0.5,
					radius*0.12,
					mycolor,
					"transparent"
				);
		}

	}
}
