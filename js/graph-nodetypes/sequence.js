
//
// Sequence
//

function NodeTypeSequence() 
{
	this.name = 'Sequence';
	this.init();
	return this.name;
}

NodeTypeSequence.prototype = 
{
	init: function() 
	{
		this.unique = false;
		this.leaf = false;
		this.single_child = false;
		this.image  = images.getImage('sequence');
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() 
	{
		return this.name;
	},
	set_color: function(color) 
	{
		this.color = color;
	},
	list_properties: function() 
	{
		return '';
	},
	add_list_properties_listeners: function() 
	{
		return;
	},
	export_nodetype: function() 
	{
		var output = 
		{
			'name': this.name
		};
		return output;
	},
	export_arduino: function(node) 
	{
		return 'N' + padByte(node.id) + padByte(NODE_SEQUENCE) + 
			padByte(node.children.length)+';';
	}
}

