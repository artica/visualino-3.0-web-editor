
//
// Sensor
//

function NodeTypeSensor() {
	this.name = 'Sensor';
	this.init();
	return this.name;
}

NodeTypeSensor.prototype = {
	init: function() {
		this.targeted = 0;		
		this.assigned = -1;
		
		this.unique = false;
		this.leaf = true;

		this.pintypes = {'Analogue': 0, 'Digital': 1};
		this.pintype = this.pintypes['Analogue'];
		this.pintype_export = ['PIN_ANALOGUE','PIN_DIGITAL'];
		this.pinvalue = 0;
		this.threshold = 640;
		this.passtype = true;
		this.image  = images['input'];		
		return this;
	},
	toString: function() {
		var output = this.name;
		if (this.assigned != -1) {
			var nt = g.nodes[g.find_root()].type;
			output = nt.targets[nt.selectedtarget]['assigns'][this.assigned]['Name'];
		}
		return output;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '';
		
		// assigned sensor		
		output += '<tr><td>'+t[lang]['Assigned']+'</td><td>';
		output += '<select id="assignedInput">';
		
		var selected = '';
		if (this.assigned == -1) selected = 'selected="true"';
		output += '<option value="-1" '+selected+'></option>';
		
		var root_ref = g.find_root();
		
		if (root_ref != -1) {
			var nt = g.nodes[root_ref].type;
			var st = nt.selectedtarget;
			
		}
		output += '</select></td></tr>';
		
		// pin type		
		output += '<tr><td>'+t[lang]['Pin Type']+'</td><td>';
		output += '<select id="pintypeInput">';
		
		//for (var i=0; i < this.pintypes.length; i++) {
		for (i in this.pintypes) {
			var selected = '';
			if (this.pintype == this.pintypes[i]) selected = 'selected="true"';
			output += '<option value="'+this.pintypes[i]+'" '+selected+'>' + t[lang][i] + '</option>';
		}
		output += '</td></tr>';

		// pin value
		output += '<tr><td>'+t[lang]['Pin']+'</td><td><input id="pinInput" name="pinInput" value="'+this.pinvalue+'"></td></tr>';
		
		// threshold
		//console.log('test ' + this.pintype);
		if (this.pintype === 0) { 
			output += '<tr><td>'+t[lang]['Threshold']+' [0..1023]</td><td><input id="thresholdInput" name="thresholdInput" value="'+this.threshold+'"></td></tr>';
		}
		
		// passtype
		var checked = '';
		if (this.passtype) checked = 'checked';
		output += '<tr><td>'+t[lang]['Pass Type']+'</td><td><input type="checkbox" id="passType" name="passType" value="passType" '+checked+'>'+t[lang]['Pass High']+'</td></tr>';
		
		return output;
	},
	add_list_properties_listeners: function() {
	
		// assigned
		var assignedInput = document.getElementById('assignedInput');
		if (assignedInput) {
			var thisnode = this;
			assignedInput.onchange = function() {
				console.log('changing');
				var index = assignedInput.selectedIndex;
				var options = assignedInput.options;
				
				// update assigned index
				thisnode.assigned = parseInt(options[index].value,10);
				
				// update pin values
				if (thisnode.assigned != -1) {
					var nt = g.nodes[g.find_root()].type;
					var assigns = nt.targets[nt.selectedtarget]['assigns'][thisnode.assigned];
					
					thisnode.pintype = thisnode.pintypes[assigns['Pin Type']];
					thisnode.pinvalue = assigns['Pin'];
					
					if (g.selectedNode != -1) g.display_node_properties(g.selectedNode);
				}
			}
		}
		
		// pin type
		var pintypeInput = document.getElementById('pintypeInput');
		if (pintypeInput) {
			var thisnode = this;
			pintypeInput.onchange = function() {
				var index = pintypeInput.selectedIndex;
				var options = pintypeInput.options;
		
				thisnode.pintype = parseInt(options[index].value, 10);
				if (g.selectedNode != -1) g.display_node_properties(g.selectedNode);
			}
		}
		
		// pin value
		var pinInputBox = document.getElementById('pinInput');
		if (pinInputBox) {
			var thisnode = this;
			pinInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.pinvalue = pinInputBox.value;
			}
			pinInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			pinInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// threshold
		var thresholdInputBox = document.getElementById('thresholdInput');
		if (thresholdInputBox) {
			var thisnode = this;
			thresholdInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.threshold = thresholdInputBox.value;
			}
			thresholdInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			thresholdInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// pass type
		var passInputBox = document.getElementById('passType');
		if (passInputBox) {
			var thisnode = this;
			passInputBox.onclick = function() {
				g.inputBoxTyping = true;
				thisnode.passtype = passInputBox.checked;
			}
		}
	
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'pintype': this.pintype,
			'pinvalue': this.pinvalue,
			'threshold': this.threshold
		};
		return output;
	},
	export_arduino: function(node) {
		return 'N' + padByte(node.id) + 
			padByte(NODE_SERVICE) + padByte(SERVICE_INPUT_DIGITAL_PIN)+
			padByte(this.pinvalue)+';';



		//var mode = this.pintype_export[this.pintype];
		//return 'SL::Behaviors::Sensor* ' + nodeID + ' = new SL::Behaviors::Sensor('+mode+','+this.pinvalue+','+this.threshold+');';
	}
}

