
//
// Sensor
//

function NodeTypeInput() 
{
	this.name = 'Input';
	this.init();
	return this.name;
}

NodeTypeInput.prototype = 
{
	init: function() 
	{
		this.unique = false;
		this.leaf = true;
		this.device = -1;
		this.data = {};
		this.image  = images.getImage('input');
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() 
	{
		return this.name;
	},

	// Convert the integer references to device pointers
	initPointers: function()
	{
		console.log('initPointers:'+this.device_id);
		console.log(this);
		console.log(this.device);
		console.log('-------------------');
		//var devices = g.find_root().type.devices;

		this.device = g.devices[g.getDeviceById(this.device_id)];
	},

	// List the properties of the input node, with all the specific device configuration
	list_properties: function() 
	{
		// Get the list of devices in the system
		var devices = g.devices;
		var output = '';
		
		output += '<table><tr><td>'+t[lang]['device']+'</td><td>'

		// Create a selector to choose which device to use
		output += '<select id="select_device">';
		for (var i=0; i < devices.length; i++) 
		{
			// Only show devices that are classified as outputs
			if (devices[i].type.class === DEVICE_CLASS_INPUT)
			{
				// If no device was selected for the node, we select the first compatible device
				if (this.device === null) 
				{
					this.device = devices[i];
					// We also need to initialize the data of this node with the default values for the device
					this.data = devices[i].type.init_service_data();
				}
				// Mark the device in use by the node as selected
				var selected = '';
				if (this.device === devices[i]) selected = 'selected="true"';
				output += '<option id="select_device" value="'+i+'" '+selected+'>' + devices[i].name + '</option>';				
			}
		}
		output += '</select></td></tr></table>';

		console.log(this.device);

		// Now we show the properties for the selected device type, assuming there is one
		if (this.device !== null)
		{ 
			output += this.device.type.show_service_configuration(this.data);
		}
		return output;
	},

	// Add the listeners for changes in the configuration
	add_list_properties_listeners: function()
	{
		// Variable necessary to acess the data from a callback function
		var thisnode = this;
		// callback function for when the selected device changes
		$('#select_device').on('change', function() 
			{
				console.log("CATANO!!!!  "+ parseInt($('#select_device').val()));

				console.log(thisnode.device);
				thisnode.device = g.devices[ parseInt($('#select_device').val()) ];
				console.log(thisnode.device);
				// We need to initialize the default data for the new device
				thisnode.data = thisnode.device.type.init_service_data();
				// Now we refresh the properties window with the new data
				g.display_node_properties(g.visible_node_properties);
			});

		// We also need to add the listeners for the configuration elements specific for the device
		if (this.device !== null)
		{ 
			this.device.type.add_service_configuration_listeners(thisnode);
		}		
		return this;
	},

	// Export the data for the node 
	export_nodetype: function() 
	{
		var output = {
			'name': this.name,
			'device': this.device,
			'data': this.data
		};
		return output;
	},

	// Export the data for the node (so sent to the Arduino)
	export_arduino: function(node) 
	{
		console.log('##### export_input:'+node);
		console.log(node);
		return 'N' + padByte(node.id) + padByte(NODE_SERVICE) +	this.device.type.export_arduino_service(this);
	}
}

