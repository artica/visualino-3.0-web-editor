
//
// Root
//

function NodeTypeRoot() {
	this.name = 'Root';
	this.init();
	return this.name;
}

NodeTypeRoot.prototype = 
{
	init: function() 
	{
		this.unique = true;
		this.leaf = false;
		this.single_child = true;

		this.new_device_type = 0;

		this.devices = [];
		this.image  = images.getImage('root');		

		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() 
	{
		return this.name;
	},
	set_color: function(color) 
	{
		this.color = color;
	},

	list_properties: function() 
	{
		console.log("ROOT list_properties");
		console.log(g.devices);
		var output = '<table><tr><th class="td_device_image">Image</th><th class="td_device_name">Name</th>'+
			'<th class="td_device_type">Type</th><th class="td_device_configuration">Configuration</th></tr></table>';

		// List the hardware device configuration
		for (var i=0; i<g.devices.length; i++) 
		{
			var device = g.devices[i];
			//console.log(device);

			//output += '<tr><td><hr></td><td><hr></td><td><hr></td><td><hr></td></tr>';

			// Create the table with the basic device information (always visible)
			output += '<table><tr class="tr_device_select" device_number="'+i+'">'+
				'<td class="td_device_image"><image class="image_list" src="'+device.getImage().src+'"></image></td>'+
				'<td style="td_device_name">'+device.toString()+'</td>'+
				'<td style="td_device_type">'+device.type.toStringType()+'</td>'+
				'<td style="td_device_configuration">'+device.type.toStringConfiguration()+'</td></tr></table>';

			var image = device.getImage().src;
			var img_id = device.getImage().img_id;

			output += "<div id='div_device_properties_"+i+"' style='display:"+
				( device.visible? 'block': 'none') + "'><table><tr>"+
				"<td><img class='button_update_device' device_number='"+i+"' src='images/header/update.svg'></img></td>"+
				"<td><img class='button_delete_device' device_number='"+i+"' src='images/header/delete.svg'></img></td>"+
				"<td><img class='button_collapse_device' device_number='"+i+"' src='images/header/collapse.svg'></img></td>"+
				"</tr></table><table>"+
				"<tr><td>"+t[lang]['type']+"</td><td>"+
				device.type.toStringType();

			output += "</td></tr>"+

				"<tr><td>"+t[lang]['Name']+"</td><td><input type='text' id='name_device_"+i+"'"+
				" value='"+device.name+"'></input></td></tr>"+
				"<tr><td>"+t[lang]['Image']+"</td><td><img id='image_device_"+i+"' class='image_device' "+
				"img_id='"+img_id+"' device_number='"+i+"' src='"+image+"'/></td></tr>"+
				"</table>"+
				"<div id='images_selector_"+i+"'></div>";


			// Now we add the configuration properties for the selected device type
			output += device.type.list_properties(i) +
				"</div>";
					
		}
		output += '<tr><td colspan="4"><hr></td></tr></table>';

		// Adds a line with an option to add a new device
		output += "<table><tr>"+
					"<td>"+t[lang]['Add']+"</td>"+		
					"<td><select id='select_new_device'>";
		for (d = 0; d < g.d.devices.length; d++) 
		{
			output += "<option id='select_device'"+(this.new_device_type===d? " selected='true'": "")+
				" value='"+d+"'>" + g.d.devices[d].toStringType() + "</option>";
			//console.log(g.d.devices[d].toStringType());
		}

		output += "</select></td>"+
				"<td><img id='img_new_device' src='images/header/add.svg'></img></td>"+
				"</tr></table>";

		return output;
	},
	add_list_properties_listeners: function() 
	{
		var thisnode = this;
		// Selector to change the type of the new device to be created
		$('#select_new_device').on('change', function() 
			{
				thisnode.new_device_type = parseInt($('#select_new_device').val());
				// Now we refresh the properties window with the new data
				//

				//g.display_node_properties(g.visible_node_properties);

				//console.log("---------------------------");
				//	console.log(thisnode.new_device_type);
				// $('#div_new_device').html( g.d.devices[thisnode.new_device_type].list_properties(999) );
			});

		// Button to add a new device to the device list with default values
		$('#img_new_device').on('click', function() 
			{
				//console.log("Adding...");
				//console.log(g.d.devices[thisnode.new_device_type]);

				// Create a new device based to the 'select_new_device' selector value
				var new_device = g.d.devices[thisnode.new_device_type].create_default_device();
				g.devices.push(new_device);

				// Now we refresh the properties window with the new data
				g.display_node_properties(g.visible_node_properties);

				// We also need to update the active nodetypes
				// If a new input our output is created, the respective nodec can be instantiated
				g.n.refresh_devices();

			});

		// Button to update the settings of an existing device
		$('.button_update_device').on('click', function() 
			{
				// Try to update the device settings
				console.log("Updating...");
				// Get the device number and new device name from the forms
		        var device_number = parseInt(getHtmlAttr(event, 'device_number'));
				var name = $('#name_device_'+device_number).val();

				// Get the a pointer to the old device to be updated
				var device = g.devices[device_number];

				console.log(device);

				// Create a new device type object of the same type as the current one
				var new_device_data = device.type.create_custom_device_data(device_number);

				console.log(new_device_data);

				console.log('-------> #image_device_'+device_number);



				device.name = name;
				device.type = new_device_data;
				device.setImage( $('#image_device_'+device_number).attr('img_id') );

				//console.log( 'zomg:' + $('#img_selected').attr('img_id') );

				// Now we refresh the properties window with the new data
				g.display_node_properties(g.visible_node_properties);
				
			});

		// Button to delete an existing device
		$('.button_delete_device').on('click', function() 
			{
				// Try to update the device settings
				console.log("Deleting...");
		        var device_number = parseInt(getHtmlAttr(event, 'device_number'));

				g.devices.splice(device_number, 1);
				// Now we refresh the properties window with the new device list
				g.display_node_properties(g.visible_node_properties);

				// We also need to update the active nodetypes
				// If all the inputs our outputs are deleted, the respective nodes cannot be instantiated
				g.n.refresh_devices();
			});


		$('.button_collapse_device').on('click', function(event) 
			{
		        var device_number = parseInt(getHtmlAttr(event, 'device_number'));
				collapseDiv('div_device_properties_'+device_number, 300);				
				g.devices[device_number].visible = false;
			});

		$('.tr_device_select').click( function(event) 
		{
	        var device_number = parseInt(getHtmlAttr(event, 'device_number'));

			g.devices[device_number].visible = !g.devices[device_number].visible;
			// Now we refresh the properties window with the new data
			//g.display_node_properties(g.visible_node_properties);

			if (g.devices[device_number].visible)
				expandDiv('div_device_properties_'+device_number, 300);
			else 
				collapseDiv('div_device_properties_'+device_number, 300);				

		});

		$('.image_device').click( function(event) 
			{ 
				console.log("MEH!");
		        var device_number = parseInt(getHtmlAttr(event, 'device_number'));

		        // @todo 'output' num pode ser carago... mudar isto

				images.createImageSelector('images_selector_'+device_number, 'image_device_'+device_number, 'output'); 
			} );

		return this;
	},


	export_nodetype: function() 
	{
		var output = 
		{
			'name': this.name
		};
		return output;
	},

	export_arduino: function(node) 
	{
		return 'N' + padByte(node.id) + padByte(NODE_SEQUENCE) + 
			padByte(node.children.length)+';';
	}
}
