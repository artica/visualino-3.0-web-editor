
//
// Servo Incrementor
//

function NodeTypeServoIncrementor() {
	this.name = 'Servo Incrementor';
	this.init();
	return this.name;
}

NodeTypeServoIncrementor.prototype = {
	init: function() {
		this.unique = false;
		this.leaf = true;
		this.pintypes = ['Digital','PWM'];
		this.pintype = 'Digital';
		this.pinvalue = 9;
		this.value = 1;
		return this;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() {
		var output = '';
		
		// pin type		
		output += '<tr><td>'+t[lang]['Pin Type']+'</td><td>';
		output += '<select id="pintypeInput">';
		for (var i=0; i < this.pintypes.length; i++) {
			var selected = '';
			if (this.pintype == this.pintypes[i]) selected = 'selected="true"';
			output += '<option value="'+this.pintypes[i]+'" '+selected+'>' + t[lang][this.pintypes[i]] + '</option>';
		}
		output += '</td></tr>';

		// pin value
		output += '<tr><td>'+t[lang]['Pin']+'</td><td><input id="pinInput" name="pinInput" value="'+this.pinvalue+'"></td></tr>';
		
		// incremental value
		output += '<tr><td>'+t[lang]['Value']+'</td><td><input id="valueInput" name="valueInput" value="'+this.value+'"></td></tr>';
		
		return output;
	},
	add_list_properties_listeners: function() {
	
		// pin type
		var pintypeInput = document.getElementById('pintypeInput');
		if (pintypeInput) {
			var thisnode = this;
			pintypeInput.onchange = function() {
				var index = pintypeInput.selectedIndex;
				var options = pintypeInput.options;
		
				for (var j = 0; j < thisnode.pintypes.length; j++) {
					if (options[index].value == thisnode.pintypes[j]) {
						thisnode.pintype = thisnode.pintypes[j];
					}
				}
			}
		}
		
		// pin value
		var pinInputBox = document.getElementById('pinInput');
		if (pinInputBox) {
			var thisnode = this;
			pinInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.pinvalue = pinInputBox.value;
			}
			pinInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			pinInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		
		// value
		var valueInputBox = document.getElementById('valueInput');
		if (valueInputBox) {
			var thisnode = this;
			valueInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.value = valueInputBox.value;
			}
			valueInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			valueInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}

		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'pintype': this.pintype,
			'pinvalue': this.pinvalue,
			'value': this.value
		};
		return output;
	},
	export_arduino: function(nodeID) {
		return 'SL::Behaviors::ServoIncrementor* ' + nodeID + ' = new SL::Behaviors::ServoIncrementor('+this.value+','+this.pinvalue+');';
	}
}
