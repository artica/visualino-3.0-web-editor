
//
// LED
//

function NodeTypeLED() 
{
	this.name = 'LED';
	this.init();
	return this.name;
}

NodeTypeLED.prototype = 
{
	init: function() {
		this.unique = false;
		this.leaf = true;
		this.device = 0;
		this.value = 0;
		this.image  = images['output'];		
		return this;
	},
	toString: function() 
	{
		return this.name;
	},
	set_color: function(color) 
	{
		this.color = color;
	},
	list_properties: function() 
	{
		var output = '';
		var devices = g.nodes[g.find_root()].type.devices;
		//console.log(g.nodes[g.find_root()]);
		console.log(this.device);
		// Create a selector to choose which device to use	
		output += '<tr><td>'+t[lang]['Device']+'</td><td><select id="select_device">';
		for (var i=0; i < devices.length; i++) 
		{
			var selected = '';
			if (this.device == i) selected = 'selected="true"';
			output += '<option id="select_device" value="'+i+'" '+selected+'>' + devices[i].name + '</option>';
		}
		output += '</select></td></tr>';
		
		// value
		output += '<tr><td>'+t[lang]['Value']+' [0..255]</td><td><input id="text_output_value" value="'+this.value+'"></td></tr>';
		
		return output;
	},
	add_list_properties_listeners: function() 
	{
		var thisnode = this;
		$('#text_output_value').on('change', function() 
			{
				console.log("BAM!");
				console.log(thisnode);
				thisnode.value = parseInt($('#text_output_value').val());
			});

		$('#select_device').on('change', function() 
			{
				console.log(thisnode);
				thisnode.device = parseInt($('#select_device').val());
				console.log("DEVICE:"+thisnode.device);

			});
		
		return this;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'pintype': this.pintype,
			'device': this.device,
			'value': this.value
		};
		return output;
	},
	export_arduino: function(node) 
	{
		return 'N' + padByte(node.id) + 
			padByte(NODE_SERVICE) + padByte(SERVICE_OUTPUT_DIGITAL_PIN)+
			padByte(this.device)+padByte(this.value)+';';
	}
}
