
//
// Delay Time
//

function NodeTypeDelayTime() 
{
	this.name = 'Delay Time';
	this.init();
	this.value = 1000;
	return this.name;
}

NodeTypeDelayTime.prototype = 
{
	init: function() {
		this.unique = false;
		this.leaf = true;
		this.image  = images.getImage('delay');		
		return this;
	},
	getImage: function()
	{
		return this.image;
	},
	toString: function() {
		return this.name;
	},
	set_color: function(color) {
		this.color = color;
	},
	list_properties: function() 
	{
		console.log("le delay time:"+this.value);	

		return '<table>'+
				'<tr>'+
					'<td>'+t[lang]['Value']+'</td>'+
					'<td><input id="valueInput" type="number" value="'+this.value+'" min="0" max="4294967295"></td>'+
				'</tr>'+
			'</table>';
	},
	add_list_properties_listeners: function() {
	
		// value
		var valueInputBox = document.getElementById('valueInput');
		if (valueInputBox) {
			var thisnode = this;
			valueInputBox.onkeyup = function() {
				g.inputBoxTyping = true;
				thisnode.value = valueInputBox.value;
			}
			valueInputBox.onclick = function() {
				g.inputBoxTyping = true;
			}
			valueInputBox.onfocus = function() {
				g.inputBoxTyping = true;
			}
		}
		return;
	},
	export_nodetype: function() {
		var output = {
			'name': this.name,
			'value': this.value
		};
		return output;
	},
	export_arduino: function(node) 
	{
		return 'N' + padByte(node.id) + 
			padByte(NODE_SERVICE) + padByte(SERVICE_DELAY)+
			padLong(this.value)+';';
	},
	draw: function(ctx, x, y, radius) 
	{
		drawText ( ctx, this.value, x-radius, y+radius, "#eeeeee" );
	}
}
