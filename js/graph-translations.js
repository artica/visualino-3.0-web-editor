

var lang = 'pt';
var t = [];



//
// English
//

t['en'] = [];

// index.html
t['en']['bad_canvas'] = 'Couldn\'t access mycanvas';
t['en']['no_canvas'] = 'Your browser has no canvas support! That\'s bad.';


// graph-devicetypes.js
t['en']['device'] = 'Device';

t['en']['digital_input'] = 'Digital Input';
t['en']['digital_output'] = 'Digital Output';
t['en']['differential_motor'] = 'Differential Motor';
t['en']['new_device'] = 'New Device';

t['en']['type'] = 'Type';


// graph-nodetypes.js
t['en']['Root'] = 'Root';
t['en']['Root Info'] = 'This is the Root node! Where all the magic starts';
t['en']['Target'] = 'Target Platform';
t['en']['JSON Import'] = 'JSON Import';
t['en']['JSON Import Info'] = 'This is the JSON Import node! It allows you to import composite behavior trees from an encoded json string.';
t['en']['JSON String'] = 'JSON String';
t['en']['JSON Child'] = 'Can\'t import JSON if the node already has children.';
t['en']['Parallel'] = 'Parallel';
t['en']['Parallel Info'] = 'This is the Parallel node! It allows you to start a sub-tree whose children all run in parallel.';
t['en']['Require One'] = 'Require One';
t['en']['Require All'] = 'Require All';
t['en']['Completion Policy'] = 'Completion Policy';
t['en']['Failure Policy'] = 'Failure Policy';
t['en']['Sequence'] = 'Sequence';
t['en']['Sequence Info'] = 'This is the Sequence node! It allows you to start a sub-tree whose children all run in sequence.';
t['en']['Priority Selector'] = 'Priority Selector';
t['en']['Status Override'] = 'Status Override';
t['en']['Fail'] = 'Fail';
t['en']['Run'] = 'Run';
t['en']['Success'] = 'Success';
t['en']['Failure Override'] = 'Failure Override';
t['en']['Running Override'] = 'Running Override';
t['en']['Success Override'] = 'Success Override';
t['en']['Motor'] = 'Motor';
t['en']['Motor Info'] = 'This is the Motor node! It allows you to control a motor.';
t['en']['Right Motor'] = 'Right Motor';
t['en']['Left Motor'] = 'Left Motor';
t['en']['Digital'] = 'Digital';
t['en']['Analogue'] = 'Analogue';
t['en']['PWM'] = 'PWM';
t['en']['Pin Type'] = 'Pin Type';
t['en']['Speed'] = 'Speed';
t['en']['Servo'] = 'Servo';
t['en']['Servo Info'] = 'This is the Servo node! It allows you to control a servo.';
t['en']['Servo Range'] = 'Servo Range';
t['en']['Servo Range Info'] = 'This is the Servo Range node! It allows you to control a servo.';
t['en']['Pin'] = 'Pin';
t['en']['Initial State'] = 'Initial State';
t['en']['Servo Incrementor'] = 'Servo Incrementor';
t['en']['Servo Incrementor Info'] = 'This is the Servo Incrementor node! It allows you to control a servo with incremental values.';
t['en']['Servo Threshold'] = 'Servo Threshold';
t['en']['Servo Threshold Info'] = 'This is the Servo Threshold node! It allows you to check values from the servo.';
t['en']['Threshold'] = 'Threshold';
t['en']['Pass Type'] = 'Type';
t['en']['Pass High'] = 'High Pass';
t['en']['Pass Low'] = 'Low Pass';
t['en']['Low'] = 'Low';
t['en']['High'] = 'High';
t['en']['LED'] = 'LED';
t['en']['LED Info'] = 'This is the LED node! It allows you to change the value of a LED.';
t['en']['Value'] = 'Value';
t['en']['Add'] = 'Add';
t['en']['State'] = 'State';
t['en']['Active'] = 'Active';
t['en']['Sensor'] = 'Sensor';
t['en']['Sensor Info'] = 'This is the Sensor node! It allows you to read value from a sensor.';
t['en']['Video'] = 'Video';
t['en']['Video Info'] = 'This is the Video node! It allows you to play a video.';
t['en']['Image'] = 'Image';
t['en']['Image Info'] = 'This is the Image node! It allows you to display an image.';
t['en']['Delay Time'] = 'Delay Time';
t['en']['Delay Time Info'] = 'This is the Delay Time node! It allows you to delay the following action for a certain number of miliseconds.';
t['en']['Assigned'] = 'Assigned';
t['en']['Sensor IR'] = 'Sensor IR';
t['en']['Left Bumper'] = 'Left Bumper';
t['en']['Right Bumper'] = 'Right Bumper';
t['en']['min'] = 'Minimum';
t['en']['max'] = 'Maximum';
t['en']['inc'] = 'Increment';

// graph-graph.js
t['en']['UID'] = 'UID';
t['en']['Name'] = 'Name';
t['en']['Node Type'] = 'Node Type';
t['en']['unknown_type'] = 'Trying to import a node of type unknown to current version of visualino.';
t['en']['connections_error'] = 'Trying to import a connection to non existing nodes.';
t['en']['del'] = 'To delete a node just press the delete key when the node is selected.';
t['en']['delete_node'] = 'Delete node';
t['en']['Export .json'] = 'Export .json';
t['en']['0root'] = 'Warning: You should have atleast 1 root node to export correctly.';
t['en']['manyroot'] = 'Warning: You should have only 1 root node to export correctly.';
t['en']['error_duplicate_unique_node'] = 'It is not allowed to add duplicates of unique nodes.';
t['en']['error_cannot_instantiate_node'] = 'It is not possible to create that type of node.';
t['en']['error_leaf'] = 'It is not allowed to add more nodes to leaf nodes.';
t['en']['error_loop'] = 'A node cannot be a child of one of its descendants.';
t['en']['error_single_child'] = 'This node can only have one child.';
t['en']['confirm_load'] = 'Are you sure you want to import a JSON file? Unsaved changes to the current tree will be lost.';
t['en']['COM Port'] = "COM Port:";
t['en']['Connect'] = "Connect";
t['en']['Disconnect'] = "Disconnect";
t['en']['No Connection'] = "The interface needs to be connected to a port before sending data!";
t['en']['Communication Problem'] = "Problems in data transmission to the Arduino!";
t['en']['Communication Fail'] = "Problem starting communication... Is the Arduino connected?";
t['en']['No Websocket'] = "The websocket is not connected to the COM gateway server!";
t['en']['error_tree_upload'] = "Error uploading the tree to the Arduino!";


//
// Portuguese
//

t['pt'] = [];

// index.html
t['pt']['bad_canvas'] = 'mycanvas não está acessível';
t['pt']['no_canvas'] = 'O seu browser não suporta o elemento canvas! É mau.';


// graph-devicetypes.js
t['pt']['device'] = 'Dispositivo';

t['pt']['digital_input'] = 'Input Digital';
t['pt']['digital_output'] = 'Output Digital';
t['pt']['differential_motor'] = 'Motor Diferencial';
t['pt']['new_device'] = 'Novo Dispositivo';

t['pt']['type'] = 'Tipo';

// graph-nodetypes.js
t['pt']['Root'] = 'Raíz';
t['pt']['Root Info'] = 'Este é o nó Raíz! Onde toda a magia começa.';
t['pt']['Target'] = 'Plataforma alvo';
t['pt']['JSON Import'] = 'JSON Import';
t['pt']['JSON Import Info'] = 'Este é o nó que permite importar JSON, ou seja, podes ter árvores dentro de nós e assim aumentar a complexidade dos teus programas!';
t['pt']['JSON String'] = 'JSON String';
t['pt']['JSON Child'] = 'Não é possivel importar JSON por o nó já ter filhos.';
t['pt']['Parallel'] = 'Paralelo';
t['pt']['Parallel Info'] = 'Este é o nó Paralelo! Permite começar um ramo onde todas as folhas correm em paralelo, ou seja, em simultâneo.';
t['pt']['Require One'] = 'Necessário Um';
t['pt']['Require All'] = 'Necessários Todos';
t['pt']['Completion Policy'] = 'Regra de Sucesso';
t['pt']['Failure Policy'] = 'Regra de Falha';
t['pt']['Sequence'] = 'Sequência';
t['pt']['Sequence Info'] = 'Este é o nó Sequência! Permite começar um ramo onde todas as folhas correm em sequência, ou seja, uma folha a seguir à outra.';
t['pt']['Priority Selector'] = 'Selector de Prioridade';
t['pt']['Status Override'] = 'Status Override';
t['pt']['Fail'] = 'Falha';
t['pt']['Run'] = 'Correr';
t['pt']['Success'] = 'Successo';
t['pt']['Failure Override'] = 'Falha devolvida';
t['pt']['Running Override'] = 'Corrida devolvida';
t['pt']['Success Override'] = 'Successo devolvido';
t['pt']['Motor'] = 'Motor';
t['pt']['Motor Info'] = 'Este é o nó Motor, vai permitir controlar um único motor.';
t['pt']['Right Motor'] = 'Motor Direito';
t['pt']['Left Motor'] = 'Motor Esquerdo';
t['pt']['Digital'] = 'Digital';
t['pt']['Analogue'] = 'Analógico';
t['pt']['PWM'] = 'PWM';
t['pt']['Pin Type'] = 'Tipo de pin';
t['pt']['Speed'] = 'Velocidade';
t['pt']['Servo'] = 'Servo';
t['pt']['Servo Info'] = 'Este é o nó Servo, vai permitir controlar um motor servo.';
t['pt']['Servo Range'] = 'Servo Range';
t['pt']['Servo Range Info'] = 'Este é o nó Servo Range, vai permitir controlar um motor servo.';
t['pt']['Pin'] = 'Pino';
t['pt']['Initial State'] = 'Estado Inicial';
t['pt']['Servo Incrementor'] = 'Incrementador de Servo';
t['pt']['Servo Incrementor Info'] = 'Este é o nó que incrementa o Servo, permite controlar o Servo com um valor que incrementa ou decrementa.';
t['pt']['Servo Threshold'] = 'Decisor do Servo';
t['pt']['Servo Threshold Info'] = 'Este é o nó Decisor do Servo, permite estabelecer valores para a utilização do Servo.';
t['pt']['Threshold'] = 'Limitador';
t['pt']['Pass Type'] = 'Tipo';
t['pt']['Pass High'] = 'Passa Alto';
t['pt']['Low'] = 'Baixo';
t['pt']['High'] = 'Alto';
t['pt']['LED'] = 'LED';
t['pt']['LED Info'] = 'Este é o nó do LED, permite mudar o estado do LED.';
t['pt']['Value'] = 'Valor';
t['pt']['Add'] = 'Adicionar';
t['pt']['State'] = 'Estado';
t['pt']['Active'] = 'Activo';
t['pt']['Sensor'] = 'Sensor';
t['pt']['Sensor Info'] = 'Este é o nó Sensor, permite fazer uma leitura ao Sensor.';
t['pt']['Video'] = 'Video';
t['pt']['Video Info'] = 'Este é o nó Video, permite ler e reproduzir um video.';
t['pt']['Image'] = 'Imagem';
t['pt']['Image Info'] = 'Este é o nó Imagem, permite ler e reproduzir uma imagem.';
t['pt']['Delay Time'] = 'Esperar';
t['pt']['Delay Time Info'] = 'Este é o nó Esperar, permite esperar um determinado periodo de tempo em milisegundos.';
t['pt']['Assigned'] = 'Assignado';
t['pt']['Sensor IR'] = 'Sensor IR';
t['pt']['Left Bumper'] = 'Bumper Esquerdo';
t['pt']['Right Bumper'] = 'Bumper Direito';
t['pt']['min'] = 'Minimo';
t['pt']['max'] = 'Máximo';
t['pt']['inc'] = 'Incremento';

// graph-graph.js
t['pt']['UID'] = 'UID';
t['pt']['Name'] = 'Nome';
t['pt']['Node Type'] = 'Tipo de nó';
t['pt']['unknown_type'] = 'Nó de tipo desconhecido';
t['pt']['connections_error'] = 'Tentativa de importação de uma ligação de nós não existentes.';
t['pt']['del'] = 'Para apagar um nó basta premir a tecla delete quando este se encontra seleccionado.';
t['pt']['delete_node'] = 'Remover nó';
t['pt']['Export .json'] = 'Exportar .json';
t['pt']['0root'] = 'Aviso: Deve ter 1 nó raíz para exportar correctamente.';
t['pt']['manyroot'] = 'Aviso: Deve ter somente 1 nó raíz para exportar correctamente.';
t['pt']['error_duplicate_unique_node'] = 'Não é permitido adicionar duplicados de nós unicos.';
t['pt']['error_cannot_instantiate_node'] = 'Não é possível criar esse tipo de nó.';
t['pt']['error_leaf'] = 'Não é permitido adicionar mais nós a um nó terminal.';
t['pt']['error_loop'] = 'Um nó não pode ser filho de um dos seus descendentes.';
t['pt']['error_single_child'] = 'Este nó apenas pode ter um filho.';
t['pt']['confirm_load'] = 'Tem a certeza que deseja importar um ficheiro JSON? Alterações à àrvore não guardadas até ao momento serão perdidas.';
t['pt']['COM Port'] = "Porta COM:";
t['pt']['Connect'] = "Ligar:";
t['pt']['Disconnect'] = "Desligar:";
t['pt']['No Connection'] = "A interface precisa de estar ligada a uma porta para poder enviar dados!";
t['pt']['Communication Problem'] = "Problemas na comunicação com o Arduino!";
t['pt']['Communication Fail'] = "Problema ao iniciar a comunicação... O Arduino está ligado?";
t['pt']['No Websocket'] = "O websocket não está ligado ao COM gateway server!";
t['pt']['error_tree_upload'] = "Erro ao fazer upload da árvore para o Arduino!";


