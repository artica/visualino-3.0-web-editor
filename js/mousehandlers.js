		
var mousedown = false;

var last_mouse_x;
var last_mouse_y;

function getMousePosition(event)
{
    var x = new Number();
    var y = new Number();
    if (event.x != undefined && event.y != undefined)
	  {
        x = event.x;
        y = event.y;
	  }
  	else // Firefox method to get the position
  	{
    	  x = event.clientX + document.body.scrollLeft +
    		  document.documentElement.scrollLeft;
    	  y = event.clientY + document.body.scrollTop +
    		  document.documentElement.scrollTop;
  	}

  	x -= c.offsetLeft;
  	y -= c.offsetTop;

    last_mouse_x = x;
    last_mouse_y = y;
  	return {x:x, y:y};
}


function mouseDown(event)
{
    var button = event.which || event.button;
    //console.log('button:'+button);
    var pos = getMousePosition(event);  			  	
    mousedown = true;
    if (button === 1) g.mouseDownLeft(pos.x, pos.y);
    else if (button === 3) g.mouseDownRight(pos.x, pos.y);
} 

function mouseUp(event)
{
	var pos = getMousePosition(event);  			  	  	
  	mousedown = false;
  	g.mouseUp(pos.x, pos.y);
} 

function mouseMove(event)
{
	var pos = getMousePosition(event);  			  	  	
  	if (mousedown) 
  	{
  		g.mouseDrag(pos.x, pos.y);
  	} else 
  	{
  		g.mouseMove(pos.x, pos.y);
	}
} 

function mouseScroll(event)
{
    event.preventDefault();
	var pos = getMousePosition(event);	
    if(event.originalEvent.wheelDelta /120 > 0) 
    {
        console.log('scrolling up !');
        g.zoomTree(0.05, pos);
    }
    else
    {
        console.log('scrolling down !');
        g.zoomTree(-0.05, pos);
    }

    console.log('zoom:'+g.zoom);
}

