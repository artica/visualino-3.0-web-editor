
- ver a configuração do Gyro
	como fazer a separação/categorização dos I/Os. Várias opções:

	- ter categorias nos devices que já existem (Ex: Motor com opção normal e Gyro)

	- ter devices completamente diferentes (Motor e Motor Gyro)

	- ter um Device type que seria Gyro Component com subtipos (motor, servo, gyroscope-x, gyroscope-y, etx)


Componentes fisicos:
	Motor Esquerdo
	Motor Direito

	Buzzer
	Microfone

	Servo ?


	Encoder Esquerdo
	Encoder Direito

	Acelerometro X
	Acelerometro y
	Acelerometro z
	Giroscopio X
	Giroscopio y
	Giroscopio z
	Magnetometro X
	Magnetometro y
	Magnetometro z


	Seguidor linha esquerda
	Seguidor linha direita
	Sharp baixo central

	Sharp frente esquerda
	Sharp frente central
	Sharp frente direita


Valores calculados (caterogia separada ou variáveis internas do gyro?):
	Heading x
	Heading y
	Heading z


Behaviours (categoria separada):
	Andar x mm
	Rodar x graus



- queremos ter a opção de mudar o tido dos devices na configuração

- como lidar com os vários arduinos (uno/mega)
	- tem pinos diferentes
	- o arduino deve identificar-se?
	- os sketches devem validar se podem correr no arduino que está a ser usado



- alternativa simplificada ao status override na maioria dos casos (podemos manter o override actual na mesma)
   - testes podem ter opção de run untill success
   - sequencias podem ter opção de run 1 time, x times or forever



- teste de ascending/descending



- ver bem a questão do scope das variáveis
# - normalizar todas variáveis 0 a 1 ?
	pode causar problemas de mapeamento... ver isto bem
